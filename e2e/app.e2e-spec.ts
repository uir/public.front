import { Uir2Page } from './app.po';

describe('uir2 App', () => {
  let page: Uir2Page;

  beforeEach(() => {
    page = new Uir2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
