import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {Overview} from "../models/overview";
import {OverviewsService} from "../tabs/overviews-tab/overviews.service";
import {Observable} from "rxjs";

@Injectable()
export class PlaceOverviewsResolver implements Resolve<Overview[]> {

  constructor(private overviewsService: OverviewsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Overview[]> {
    return this.overviewsService.list(route.paramMap.get('placeId'))
  }

}
