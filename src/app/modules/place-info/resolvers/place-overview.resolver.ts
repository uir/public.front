import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {OverviewsService} from "../tabs/overviews-tab/overviews.service";
import {Overview} from "../models/overview";
import {Observable} from "rxjs";

@Injectable()
export class PlaceOverviewResolver implements Resolve<Overview> {

  constructor(private overviewsService: OverviewsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Overview> {
    return this.overviewsService.get(route.parent.parent.paramMap.get('placeId'),route.paramMap.get('overviewId'))
  }
}
