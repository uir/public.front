import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';
import {MatDialog} from "@angular/material";
import {WindowService} from "../../../services/window.service";
import {AuthorizeService} from "../../auth/services/authorize.service";

@Injectable()
export class OverviewsCreateGuard implements CanActivate {

  constructor(private dialog: MatDialog, private authService: AuthorizeService, private windowService: WindowService) {
  }

  canActivate(): boolean {
    let isAuthorize = this.authService.isAuthorizeSnapshot();
    if (!isAuthorize)
      this.windowService.showAuth();
    return isAuthorize;
  }
}
