import {Injectable} from '@angular/core';
import {
  CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot
} from '@angular/router';
import {OverviewsService} from "../tabs/overviews-tab/overviews.service";
import {MatDialog} from "@angular/material";
import {UserService} from "../../auth/services/user.service";
import {Observable} from "rxjs";
import {switchMap} from "rxjs/operators";

@Injectable()
export class OverviewsEditGuard implements CanActivate {
  public overviewId: string = null;

  constructor(private dialog: MatDialog,
              private userService: UserService,
              private overviewsService: OverviewsService) {
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> {
    let mass = state.url.split('/');
    this.overviewId = mass[mass.length - 2];
    return this.userService
      .getUser()
      .pipe(
        switchMap(x => this.overviewsService
          .checkPermission(this.overviewId, x.id))
      )
  }
}
