export class PlaceMeta {
  name: string;
  hasSales: boolean;
  hasImages: boolean;
  hasVideos: boolean;
  hasOverviews: boolean;
}
