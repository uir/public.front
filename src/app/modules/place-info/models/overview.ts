export class Overview {
  id: number;
  name: string;
  createDate?: number;
  description: string;
  userName?: string;
  placeId: string;
  userId: number;
  plus: string;
  minus: string;
}
