import {Tag} from "../../../models/tag";

export class PlaceInfo {
  id: number;
  name:string;
  description?: string;
  phone: string;
  vicinity: string;
  website: string;
  address: string;
  tags?: any[];
  priceLevel?: PriceLevel;
  workPeriods?: WorkPeriod[];
  categories: string[];
  lat: number;
  lng: number;
  isBookmark: boolean;
}

export enum PriceLevel {
  Free = 1,
  Inexpensive = 2,
  Moderate = 3,
  Expensive = 4,
  VeryExpensive = 5,
}

export enum PeriodType {
  Open = 1,
  Close = 2
}

export class WorkPeriod {
  periodType: PeriodType;
  day: number;
  time: string;
}

export class PlaceSearch {
  id: number;
  name: string;
  tags: Tag[];
}
