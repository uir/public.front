import {Routes} from "@angular/router";
import {PartnerInfoComponent} from "./partner-info.component";
import {SalesTabComponent} from "./tabs/sales-tab/sales-tab.component";
import {InfoTabComponent} from "./tabs/info-tab/info-tab.component";
import {ImagesTabComponent} from "./tabs/images-tab/images-tab.component";
import {VideoTabComponent} from "./tabs/video-tab/video-tab.component";
import {OverviewComponent} from "./tabs/overviews-tab/overviews-tab.component";
import {OverviewCreateComponent} from "./tabs/overviews-tab/create/overview-create.component";
import {PlaceOverviewResolver} from "./resolvers/place-overview.resolver";
import {OverviewsCreateGuard} from "./guard/overviews.create.guard";
import {OverviewViewComponent} from "./tabs/overviews-tab/view/overview-view.component";
import {OverviewEditComponent} from "./tabs/overviews-tab/edit/overview-edit.component";
import {OverviewsEditGuard} from "./guard/overviews.edit.guard";

export const PlaceInfoRoutes: Routes = [
  {
    path: 'places/:placeId',
    component: PartnerInfoComponent,
    data: {
      useClickLog: true,
      handler: "PartnerInfoComponent"
    },
    children: [
      {
        path: '',
        redirectTo: 'info',
        pathMatch: 'full'
      },
      {
        path: 'info',
        component: InfoTabComponent
      },
      {
        path: 'images',
        component: ImagesTabComponent
      },
      {
        path: 'videos',
        component: VideoTabComponent
      },
      {
        path: 'sales',
        component: SalesTabComponent
      },
      {
        path: 'overviews',
        children: [
          {
            path: '',
            redirectTo: 'all',
            pathMatch: 'full'
          },
          {
            path: 'all',
            component: OverviewComponent
          },
          {
            path: 'create',
            component: OverviewCreateComponent,
            canActivate: [OverviewsCreateGuard]
          },
          {
            path: ':overviewId',
            component: OverviewViewComponent,
            resolve: {
              data: PlaceOverviewResolver
            }
          },
          {
            path: ':overviewId/edit',
            component: OverviewEditComponent,
            canActivate: [OverviewsEditGuard],
            resolve: {
              data: PlaceOverviewResolver
            },
          },
        ]
      },
    ]
  }
];
