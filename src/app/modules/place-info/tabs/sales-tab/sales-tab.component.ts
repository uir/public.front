import {Component, OnDestroy, OnInit} from '@angular/core';
import {Sale} from "../../../../models/sale";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {PlaceService} from "../../place.service";
import {map, switchMap} from "rxjs/operators";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'sales-tab',
  templateUrl: './sales-tab.component.html',
  styleUrls: ['./sales-tab.component.css', '../../../../../assets/styles/color.css']
})
export class SalesTabComponent implements OnInit, OnDestroy {
  sales: Sale[];
  private _subscribe;

  constructor(public media: ObservableMedia,
              private route: ActivatedRoute,
              private placeService: PlaceService) {

  }

  take(sale: Sale) {
  }

  ngOnInit() {
    this._subscribe = this._subscribe = (this.route.parent.routeConfig.path == 'result' ? this.route : this.route.parent)
      .paramMap
      .pipe(
        map((value: ParamMap) => value.get("placeId")),
        switchMap(value => this.placeService.getSales(value))
      )
      .subscribe((value: Sale[]) => {
        this.sales = value;
      });
  }

  getTime(date: Date): number {
    return new Date(date).getTime();
  }

  ngOnDestroy(): void {
    this._subscribe.unsubscribe();
  }
}
