import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Sale} from "../../../../models/sale";
import {PlaceInfo} from "../../models/place.model";
import {ActivatedRoute} from "@angular/router";
import {PlaceService} from "../../place.service";
import {map, switchMap} from "rxjs/operators";
import {forkJoin} from "rxjs";
import {SelectMarkerParams} from "../../../common/angular-google-maps/models/select-marker.model";
import {Observable, Subject} from "rxjs/index";
import {MarkerOptions} from "../../../common/angular-google-maps/services/google-maps-types";
import {googleMapStyles} from "../../../map/components/google-map.styles";
import {AuthorizeService} from "../../../auth/services/authorize.service";
import {WindowService} from "../../../../services/window.service";
import {ObservableMedia} from "@angular/flex-layout";
import {of} from "rxjs/internal/observable/of";
import {BookmarkService} from "../../../../services/bookmark.service";

@Component({
  selector: 'app-info-tab',
  templateUrl: './info-tab.component.html',
  styleUrls: ['./info-tab.component.css', '../../../../../assets/styles/color.css']
})
export class InfoTabComponent implements OnInit, OnDestroy {

  public styles = googleMapStyles;

  info: PlaceInfo;
  private _subscribe;
  public markers: Observable<MarkerOptions[]>;
  private _idOptionsMap: Map<number, MarkerOptions> = new Map();

  constructor(private route: ActivatedRoute,
              private placeService: PlaceService,
              private authService: AuthorizeService,
              private windowService: WindowService,
              public bookmarkService: BookmarkService,
              private cd: ChangeDetectorRef,
              public media: ObservableMedia) {
  }

  ngOnInit() {
    this._subscribe = (this.route.parent.routeConfig.path == 'result' ? this.route : this.route.parent)
      .paramMap
      .pipe(
        map((value) => {
          return value.get("placeId")
        }),
        switchMap(value => this.placeService.getInfo(value))
      )
      .subscribe((value: PlaceInfo) => {
        this.info = value;

        const mapValue = {
          clickable: true,
          position: {lat: this.info.lat, lng: this.info.lng},
          optimized: true,
          icon: 'assets/icons/location.png',
          selectIcon: 'assets/icons/location_select.png',
          draggable: false
        };
        this._idOptionsMap.set(this.info.id, mapValue);

        this.markers = of([mapValue]);

        this.bookmarkService.isBookmark(this.info.id).subscribe(x => {
          this.info.isBookmark = x;
        });
      });
  }


  ngOnDestroy(): void {
    this._subscribe.unsubscribe();
  }


  controlBookmark(p: PlaceInfo) {
    if (!p.isBookmark) {
      if (!this.authService.isAuthorizeSnapshot())
        this.windowService.showAuth();
      else
        this.bookmarkService
          .addBookmark(p.id)
          .subscribe(value => {
            p.isBookmark = true;
            this.cd.detectChanges();
          });
    } else
      this.bookmarkService
        .deleteBookmark(p.id)
        .subscribe(value => {
          p.isBookmark = false;
          this.cd.detectChanges();
        });
  }
}
