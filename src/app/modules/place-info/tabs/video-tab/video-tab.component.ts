import {Component, OnDestroy, OnInit} from '@angular/core';
import {Video} from "../../../../models/video.model";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {PlaceService} from "../../place.service";
import {map, switchMap} from "rxjs/operators";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-video-tab',
  templateUrl: './video-tab.component.html',
  styleUrls: ['./video-tab.component.css']
})
export class VideoTabComponent implements OnInit, OnDestroy {

  videos: Video[];
  private _subscribe;

  constructor(public media: ObservableMedia,private route: ActivatedRoute, private placeService: PlaceService) {
  }

  ngOnInit() {
    this._subscribe = this._subscribe = (this.route.parent.routeConfig.path == 'result' ? this.route : this.route.parent)
      .paramMap
      .pipe(
        map((value: ParamMap) => value.get("placeId")),
        switchMap(value => this.placeService.getVideos(value))
      )
      .subscribe((value: Video[]) => {
        this.videos = value;
      });
  }

  ngOnDestroy(): void {
    this._subscribe.unsubscribe();
  }

}
