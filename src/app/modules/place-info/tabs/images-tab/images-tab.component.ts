import {Component, OnDestroy, OnInit} from '@angular/core';
import {ObservableMedia} from "@angular/flex-layout";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {IImage} from "../../../common/image-gallery/model/image.model";
import {PlaceService} from "../../place.service";
import {map, switchMap} from "rxjs/operators";
import {Image} from "../../../../models/image";

@Component({
  selector: 'images-tab',
  templateUrl: './images-tab.component.html',
  styleUrls: ['./images-tab.component.css']
})
export class ImagesTabComponent implements OnInit, OnDestroy {
  images: IImage[] = [];
  private _subscribe;

  constructor(public media: ObservableMedia, private route: ActivatedRoute, private placeService: PlaceService) {
  }

  ngOnInit(): void {
    this._subscribe = (this.route.parent.routeConfig.path == 'result' ? this.route : this.route.parent)
      .paramMap
      .pipe<string, Image[]>(
        map((value: ParamMap) => value.get("placeId")),
        switchMap(value => this.placeService.getImages(value))
      )
      .subscribe(value => {
        this.images = value;
      });
  }

  ngOnDestroy(): void {
    this._subscribe.unsubscribe();
  }
}
