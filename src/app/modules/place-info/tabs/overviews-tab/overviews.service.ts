import {Injectable} from '@angular/core';
import {Overview} from "../../models/overview";
import {environment} from "../../../../../environments/environment";
import {HttpService} from "../../../../services/http.service";
import {Observable} from "rxjs";

@Injectable()
export class OverviewsService {


  constructor(private http: HttpService) {
  }

  private _placeOverviews = environment.api + 'api/places/$id/overviews';

  checkPermission(overviewId, userId): Observable<boolean> {
    let url = environment.api + 'api/overviews/$id/permission/$userId';
    url = url.replace("$id", overviewId)
      .replace("$userId", userId);
    return this.http.GET<boolean>(url);
  }

  list(placeId): Observable<Overview[]> {
    let url = this._placeOverviews.replace("$id", placeId);
    return this.http.GET<Overview[]>(url);
  };

  create(placeId, overview): Observable<Overview> {
    let url = this._placeOverviews.replace("$id", placeId);
    return this.http.POST<Overview>(url, overview);
  }

  get(placeId, overviewId): Observable<Overview> {
    let url = this._placeOverviews.replace("$id", placeId) + '/' + overviewId;
    return this.http.GET<Overview>(url);
  };

  edit(overview): Observable<Overview> {
    let url = this._placeOverviews.replace("$id", overview.placeId) + '/' + overview.id;
    return this.http.PUT<Overview>(url, overview);
  };
}

