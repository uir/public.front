import {Component, Input} from '@angular/core';
import {Overview} from "../../../models/overview";
import {UserService} from "../../../../auth/services/user.service";
import {filter} from "rxjs/operators";

@Component({
  selector: 'overview-list-item',
  templateUrl: './overview-list-item.component.html',
  styleUrls: ['./overview-list-item.component.css']
})
export class OverviewListItemComponent {

  @Input() overview: Overview = null;
  public userId: number;

  constructor(userService: UserService) {
    userService
      .getUser()
      .pipe(filter(value => value != null))
      .subscribe(x => this.userId = x.id);
  }
}



