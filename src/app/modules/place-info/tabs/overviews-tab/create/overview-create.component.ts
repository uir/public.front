import {Component, OnInit} from '@angular/core';
import {OverviewForm} from "../overview.form";
import {OverviewsService} from "../overviews.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {SnackBarService} from "../../../../../services/snack-bar.service";
import {UserService} from "../../../../auth/services/user.service";
import {map} from "rxjs/operators";

@Component({
  selector: 'overview-create',
  templateUrl: '../base/overview.component.html',
  styleUrls: ['../base/overview.component.css']
})
export class OverviewCreateComponent implements OnInit {

  public userId: number;
  public placeId: string;
  public routerClose: string;

  constructor(private router: Router,
              private userService: UserService,
              private overviewService: OverviewsService,
              public overviewForm: OverviewForm,
              private snackService: SnackBarService,
              private route: ActivatedRoute) {

    userService.getUser().subscribe(value =>{
      this.userId = value.id})
    }


  ngOnInit() {
    this.route
      .parent
      .parent
      .paramMap
      .pipe(
        map((value: ParamMap) => value.get("placeId"))
      )
      .subscribe(value => {
        this.placeId = value;
        this.routerClose = "../../info";
      });
    this.overviewForm.reset();
    this.overviewForm.buildForm();
  }

  bind() {
    let overview = this.overviewForm.getValue();
    overview.userId = this.userId;
    overview.placeId = this.placeId;
    this.overviewService
      .create(this.placeId, overview)
      .subscribe(value => {
        this.overviewForm.clear();
        this.snackService.show('Обзор отправен на модерацию.');
        this.router.navigate([this.router.url.replace("/overviews/create", "")]);
      });
  }
}
