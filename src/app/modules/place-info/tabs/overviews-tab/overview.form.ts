import {FormBuilder, Validators} from '@angular/forms';
import {BaseForm} from '../../../../forms/base.form';
import {FormGroup} from '@angular/forms/forms';
import {Injectable} from '@angular/core';
import {Overview} from "../../models/overview";

@Injectable()
export class OverviewForm extends BaseForm<Overview> {

  public description: '';
  public plus: any[];
  public minus: any[];

  protected getForm(): FormGroup {
    return this.fb.group({
      name: [
        this.data.name,
        [
          Validators.required
        ]
      ],
      plus: [
        this.data.plus
      ],
      minus: [
        this.data.minus
      ],
      description: [
        this.data.description
      ]
    });
  }

  public editorOptions = {
    placeholder: "Составте свой обзор",
    modules: {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],
        ['blockquote'],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        [{'indent': '-1'}, {'indent': '+1'}],
        [{'size': ['small', false, 'large', 'huge']}],
        [{'color': []}, {'background': []}],
        [{'font': []}],
        [{'align': []}],
        ['image']
      ]
    }
  };

  public getValue = (): Overview => {
    let plus = this.plus.map(x => x.value ? x.value : x).join(';');
    let minus = this.minus.map(x => x.value ? x.value : x).join(';');
    return {...this.form.value, description: this.description, plus: plus, minus: minus}
  };

  onContentChanged({quill, html, text}) {
    this.description = html;
  }

  public setFormData(data) {
    this.description = data.description;
    this.plus = data.plus ? data.plus.split(';') : [];
    this.minus = data.plus ? data.plus.split(';') : [];
    super.setFormData(data)
  }

  public clear() {
    super.clear();
    this.setFormData({...this.getValue(), description: '', plus: '', minus: ''})
  }

  public reset() {
    this.description = '';
    this.plus = [];
    this.minus = [];
    super.reset();
  }

  public isValide() {
    return this.isValid() &&
      this.description != null &&
      this.description != '' &&
      this.plus.length > 0 &&
      this.minus.length > 0
  }

  protected getValidationMessages() {
    return {
      name: {
        required: this.required
      },
    };
  }

  constructor(private fb: FormBuilder) {
    super({
      placeId: null,
      id: null,
      name: '',
      description: '',
      userId: null,
      plus: '',
      minus: ''
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }
}
