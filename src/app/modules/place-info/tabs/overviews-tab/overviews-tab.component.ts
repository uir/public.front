import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Overview} from "../../models/overview";
import {OverviewsService} from "./overviews.service";
import {map, switchMap} from "rxjs/operators";

@Component({
  selector: 'app-overview',
  templateUrl: './overviews-tab.component.html',
  styleUrls: ['./overviews-tab.component.css']
})
export class OverviewComponent implements OnInit, OnDestroy {

  public overviews: Overview[] = [];
  private _subscribe;

  constructor(private route: ActivatedRoute, private overviewService: OverviewsService) {

  }

  ngOnInit(): void {
    this._subscribe = this.route
      .parent
      .parent
      .paramMap
      .pipe(
        map((value: ParamMap) => value.get("placeId")),
        switchMap(value => {
          return this.overviewService.list(value)
        })
      )
      .subscribe(value => {
        this.overviews = value;
      });
  }

  ngOnDestroy(): void {
    this._subscribe.unsubscribe();
  }
}

