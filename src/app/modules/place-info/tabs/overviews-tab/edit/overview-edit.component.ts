import {Component, OnInit} from '@angular/core';
import {OverviewForm} from "../overview.form";
import {OverviewsService} from "../overviews.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SnackBarService} from "../../../../../services/snack-bar.service";
import {Overview} from "../../../models/overview";

@Component({
  selector: 'overview-edit',
  templateUrl: '../base/overview.component.html',
  styleUrls: ['../base/overview.component.css']
})
export class OverviewEditComponent implements OnInit {

  public overview: Overview;
  public routerClose: string = '..';

  constructor(public overviewService: OverviewsService,
              private route: ActivatedRoute,
              private router: Router,
              private snackService: SnackBarService,
              public overviewForm: OverviewForm) {
  }

  ngOnInit() {
    this.route.data
      .subscribe(value => {
        this.overview = value.data[0];
        this.overviewForm.setFormData(this.overview);
        this.overviewForm.buildForm();
      });
  }

  bind() {
    let overview = {...this.overview,...this.overviewForm.getValue()};
    this.overviewService.edit(overview).subscribe(value => {
      this.overviewForm.clear();
      this.snackService.show('Обзор отправен на модерацию.');
      this.router.navigate([this.router.url.replace("/edit", "")]);
    });
  }
}


