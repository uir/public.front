import {Component} from '@angular/core';
import {Overview} from "../../../models/overview";
import {ActivatedRoute} from "@angular/router";
import {UserService} from "../../../../auth/services/user.service";

@Component({
  selector: 'overview-view',
  templateUrl: './overview-view.component.html',
  styleUrls: ['./overview-view.component.css','../overview-list-item/overview-list-item.component.css']
})
export class OverviewViewComponent{

  public overview: Overview = null;
  public userId: number;

  constructor(private route: ActivatedRoute, userService: UserService) {
    userService.getUser().subscribe(x => this.userId = x.id);
    this.route.data.subscribe(value => {
      this.overview = value.data[0];
    });
  }
}



