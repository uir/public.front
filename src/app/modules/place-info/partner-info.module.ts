import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ImagesTabComponent} from "./tabs/images-tab/images-tab.component";
import {PartnerInfoComponent} from "./partner-info.component";
import {SalesTabComponent} from "./tabs/sales-tab/sales-tab.component";
import {VideoTabComponent} from "./tabs/video-tab/video-tab.component";
import {InfoTabComponent} from "./tabs/info-tab/info-tab.component";
import {
  MatButtonModule,
  MatCardModule,
  MatChipsModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatTabsModule
} from "@angular/material";
import {VgBufferingModule} from "videogular2/buffering";
import {VgOverlayPlayModule} from "videogular2/overlay-play";
import {VgControlsModule} from "videogular2/controls";
import {VgCoreModule} from "videogular2/core";
import {FlexLayoutModule} from "@angular/flex-layout";
import {PlaceService} from "./place.service";
import {RouterModule} from "@angular/router";
import {PlaceInfoRoutes} from "./place-info.routing";
import {OverviewComponent} from "./tabs/overviews-tab/overviews-tab.component";
import {PlaceOverviewsResolver} from "./resolvers/place-overviews.resolver";
import {OverviewsService} from "./tabs/overviews-tab/overviews.service";
import {OverviewCreateComponent} from "./tabs/overviews-tab/create/overview-create.component";
import {OverviewForm} from "./tabs/overviews-tab/overview.form";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {OverviewListItemComponent} from "./tabs/overviews-tab/overview-list-item/overview-list-item.component";
import {PlaceOverviewResolver} from "./resolvers/place-overview.resolver";
import {OverviewsCreateGuard} from "./guard/overviews.create.guard";
import {OverviewViewComponent} from "./tabs/overviews-tab/view/overview-view.component";
import {OverviewEditComponent} from "./tabs/overviews-tab/edit/overview-edit.component";
import {SnackBarService} from "../../services/snack-bar.service";
import {OverviewsEditGuard} from "./guard/overviews.edit.guard";
import {TagInputModule} from "ngx-chips";
import {CloseButtonModule} from "../common/close-buttom/close-button.module";
import {ImageGalleryModule} from "../common/image-gallery/image-gallery.module";
import {TagListModule} from "../common/tags-list/tag-list.module";
import {LoaderModule} from "../common/loader/loader.module";
import {QuillEditorModule} from "../common/quill-editor/quillEditor.module";
import {HttpClientModule} from "@angular/common/http";
import {AgmCoreModule} from "../common/angular-google-maps/core.module";
import {BookmarkControlModule} from "../common/bookmark-control/bookmark-control.module";
import {BemModule} from "angular-bem";
import {CountdownModule} from "ngx-countdown";

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forChild(PlaceInfoRoutes),
    MatCardModule,
    ReactiveFormsModule,
    MatChipsModule,
    MatTabsModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    CloseButtonModule,
    ImageGalleryModule,
    VgCoreModule,
    TagInputModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    TagListModule,
    FlexLayoutModule,
    LoaderModule,
    QuillEditorModule,
    MatInputModule,
    AgmCoreModule,
    BookmarkControlModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyAhdRieYCmfU9EU3nqEYYNH74noY5E_Oac"
    }),
    BemModule,
    CountdownModule
  ],
  declarations: [
    PartnerInfoComponent,
    ImagesTabComponent,
    SalesTabComponent,
    VideoTabComponent,
    InfoTabComponent,
    OverviewComponent,
    OverviewCreateComponent,
    OverviewListItemComponent,
    OverviewViewComponent,
    OverviewEditComponent
  ],
  providers:[
    PlaceService,
    OverviewsService,
    OverviewForm,
    PlaceOverviewsResolver,
    PlaceOverviewResolver,
    OverviewsCreateGuard,
    SnackBarService,
    OverviewsEditGuard
  ]
})
export class PlaceInfoModule {
}
