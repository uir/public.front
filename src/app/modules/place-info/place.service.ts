import {Injectable} from '@angular/core';
import {Sale} from '../../models/sale';
import {Video} from '../../models/video.model';
import {environment} from '../../../environments/environment';
import {Image} from "../../models/image";
import {HttpService} from "../../services/http.service";
import {PlaceInfo} from "./models/place.model";
import {PlaceMeta} from "./models/place-meta.model";
import {Observable} from "rxjs";

@Injectable()
export class PlaceService extends HttpService {
  private _placeMeta = environment.api + 'api/places/$id/meta';
  private _placeInfo = environment.api + 'api/places/$id/info';
  private _placeImages = environment.api + 'api/places/$id/images';
  private _placeSales = environment.api + 'api/places/$id/sales';
  private _placeVideos = environment.api + 'api/places/$id/videos';

  getMeta(placeId): Observable<PlaceMeta>{
    let url = this._placeMeta.replace("$id", placeId);
    return this.GET<PlaceMeta>(url);
  }

  getInfo(placeId): Observable<PlaceInfo> {
    let url = this._placeInfo.replace("$id", placeId);
    return this.GET<PlaceInfo>(url);
  }

  getImages(placeId): Observable<Image[]> {
    let url = this._placeImages.replace("$id", placeId);
    return this.GET<Image[]>(url);
  }

  getSales(placeId): Observable<Sale[]> {
    let url = this._placeSales.replace("$id", placeId);
    return this.GET<Sale[]>(url);
  }

  getVideos(placeId): Observable<Video[]> {
    let url = this._placeVideos.replace("$id", placeId);
    return this.GET<Video[]>(url);
  }
}
