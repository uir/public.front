import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {transition, trigger, useAnimation} from "@angular/animations";
import {PlaceService} from "./place.service";
import {PlaceMeta} from "./models/place-meta.model";
import {map, pairwise, switchMap} from "rxjs/operators";
import {fadeIn} from "ng-animate";
import {FormControl} from "@angular/forms";
import {ObservableMedia} from "@angular/flex-layout";
import {NavigationService} from "../../services/navigation.service";

@Component({
  selector: 'app-partner-info',
  templateUrl: './partner-info.component.html',
  styleUrls: ['./partner-info.component.css'],
  animations: [
    trigger('fade',
      [
        transition('void => *', useAnimation(fadeIn, {
          params: {timing: 0.1}
        }))
      ])
  ],
})
export class PartnerInfoComponent implements OnInit {

  public placeId: number;
  public meta: PlaceMeta;
  selected = new FormControl(0);

  constructor(
    private route: ActivatedRoute,
    public media: ObservableMedia,
    private router: Router,
    private placeService: PlaceService,
    public navigationService: NavigationService) {
  }

  back() {
    this.router.navigate([this.navigationService.getUrl()])
  }

  ngOnInit(): void {
    this.route
      .params
      .subscribe(x => this.placeId = +x.placeId);
    this.route
      .paramMap
      .pipe(
        map(value => value.get("placeId")),
        switchMap(value => this.placeService.getMeta(value))
      )
      .subscribe(value => {
        this.meta = value;
      });

    this.route.queryParams.subscribe(x => {
      if (x.tab) {
        switch (x.tab) {
          case 'images' : {
            this.selected.setValue(1);
            break;
          }
          case 'videos' : {
            this.selected.setValue(2);
            break;
          }
          case 'sales' : {
            this.selected.setValue(3);
            break;
          }
          case 'overviews' : {
            this.selected.setValue(4);
            break;
          }
        }
      }
    })
  }

  goMap() {
    this.router.navigate(['/']);
  }

}


