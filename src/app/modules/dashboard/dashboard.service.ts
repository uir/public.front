import {Injectable} from '@angular/core';
import {HttpService} from "../../services/http.service";
import {Observable} from "rxjs";
import {DsPlace} from "../../models/dashboard/ds.place.model";
import {SaleItem} from "../../models/dashboard/ds.sale.model";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends HttpService {

  private bookmarksApi = environment.api + "api/dashboard/places/bookmarks";
  private newPlacesApi = environment.api + "api/dashboard/places";
  private recentSalesApi = environment.api + "api/dashboard/sales/recent";
  private newSalesApi = environment.api + "api/dashboard/sales";

  public getBookmarks(): Observable<DsPlace[]> {
    return this.GET(this.bookmarksApi);
  }

  public getNewSales(): Observable<SaleItem[]> {
    return this.GET(this.newSalesApi);
  }

  public getNewPlaces(): Observable<DsPlace[]> {
    return this.GET(this.newPlacesApi);
  }

  public getRecentSales(): Observable<SaleItem[]> {
    return this.GET(this.recentSalesApi);
  }
}
