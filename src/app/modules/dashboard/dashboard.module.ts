import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {UserLkModule} from "../user-lk/user-lk.module";
import {DsUserReferralChartComponent} from './components/ds-user-referal-chart/ds-user-referral-chart.component';
import {MatCardModule, MatChipsModule, MatIconModule, MatListModule, MatProgressBarModule} from "@angular/material";
import {BemModule} from "angular-bem";
import {FlexLayoutModule} from "@angular/flex-layout";
import {DsPlacesListComponent} from './components/ds-list/place/ds-places-list.component';
import {DsSalesListComponent} from './components/ds-list/sale/ds-sales-list.component';
import {RouterModule} from "@angular/router";
import {AppRoutes} from "../../app.routing";
import {LoaderModule} from "../common/loader/loader.module";
import {DsAuthrizedContentComponent} from './components/ds-unauthrized/ds-authrized-content.component';
import {SaleItemModule} from "../common/sale-item/sale-item.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {XsCardsComponent} from "./components/xs-list/xs-cards.component";
import {XsPlacesItemComponent} from "./components/xs-list/places/xs-places-item.component";
import {XsSaleItemComponent} from './components/xs-list/sale/xs-sale-item.component';
import {ImgPipeModule} from "../common/img-pipe/img-pipe.module";
import {ScrollHelper} from "./components/xs-list/helpers/scroll-directive";
import {PlaceService} from "../place-info/place.service";
import {BookmarkControlModule} from "../common/bookmark-control/bookmark-control.module";

@NgModule({
  imports: [
    MatIconModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes),
    CommonModule,
    UserLkModule,
    MatChipsModule,
    MatCardModule,
    BemModule,
    FlexLayoutModule,
    MatListModule,
    LoaderModule,
    SaleItemModule,
    ImgPipeModule,
    BookmarkControlModule,
  ],
  declarations: [
    ScrollHelper,
    DashboardComponent,
    DsUserReferralChartComponent,
    DsPlacesListComponent,
    XsCardsComponent,
    XsPlacesItemComponent,
    DsSalesListComponent,
    DsAuthrizedContentComponent,
    XsSaleItemComponent,
  ],
  providers: [
    PlaceService
  ]
})
export class DashboardModule {
}
