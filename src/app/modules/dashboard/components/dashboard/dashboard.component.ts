import {Component, OnInit} from '@angular/core';
import {DashboardService} from "../../dashboard.service";
import {DsPlace} from "../../../../models/dashboard/ds.place.model";
import {SaleItem} from "../../../../models/dashboard/ds.sale.model";
import {Observable} from "rxjs";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public bookmarks$: Observable<DsPlace[]>;
  public newPlaces$: Observable<DsPlace[]>;
  public newSales$: Observable<SaleItem[]>;
  public recentSales$: Observable<SaleItem[]>;

  constructor(private dsService: DashboardService,public media: ObservableMedia) {
  }

  ngOnInit() {
    this.newPlaces$ = this.dsService.getNewPlaces();
    this.bookmarks$ = this.dsService.getBookmarks();
    this.newSales$ = this.dsService.getNewSales();
    this.recentSales$ = this.dsService.getRecentSales();
  }

}
