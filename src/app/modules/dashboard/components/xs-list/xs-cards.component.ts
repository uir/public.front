import {Component, Input, OnInit} from "@angular/core";
import {Observable} from "rxjs";
import {DsPlace} from "../../../../models/dashboard/ds.place.model";
import {SaleItem} from "../../../../models/dashboard/ds.sale.model";
import {ObservableMedia} from "@angular/flex-layout";


@Component({
  selector: 'app-xs-cards',
  templateUrl: './xs-cards.component.html',
  styleUrls: ['./xs-cards.component.scss'],

})
export class XsCardsComponent implements OnInit {

  @Input() authorizedOnly: boolean = false;

  @Input() places$: Observable<DsPlace[]>;
  @Input() sales$: Observable<SaleItem[]>;


  place: DsPlace;
  sale: SaleItem;

  constructor(public media: ObservableMedia,) {
  }

  ngOnInit() {
  }

}
