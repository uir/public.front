import {Directive, ElementRef, Input} from "@angular/core";
import {fromEvent, interval, Observable, timer} from "rxjs";
import {audit, debounce, debounceTime, reduce, switchMap, take, tap} from "rxjs/operators";

@Directive({
  selector: '[scrollHelper]'
})
export class ScrollHelper {

  @Input()
  countItems: number;

  _scrollValues: number[];
  _currentScrollValue: number = 0;
  _previousScrollValue: number = 0;

  constructor(private el: ElementRef) {
    this.scrollObserve();
  }

  scrollObserve = () =>
    fromEvent(this.el.nativeElement, 'scroll')
      .pipe(
        debounceTime(100),
        tap(() => this.changeCurrentScroll(this.leftScrollInCss)),
        audit(() => this.scrollHandler()),
        tap(() => this.equateCurrentAndPreviousValues()),
      ).subscribe();


  scroll = (scrollValue: number): Observable<any> =>
    interval(10)
      .pipe(
        take(10),
        tap(() => this.scrollBy(scrollValue / 10)),
        reduce(_ => _),
        switchMap(() => timer(200)));


  findCloseScrollValue() {
    let index = 0;

    while (this.intermediatePointPredicate(index)) index++;

    let rigthScrollValue = this.scrollValues[index + 1] - this._currentScrollValue;
    let leftScrollValue = this.scrollValues[index] - this._currentScrollValue;


    return this.changeCardNeeded
      ? this.isRightScroll ? rigthScrollValue : leftScrollValue
      : this.isRightScroll ? leftScrollValue : rigthScrollValue;

  }


  scrollHandler = (): Observable<any> => this.scroll(this.findCloseScrollValue());
  scrollBy = (px: number) => this.el.nativeElement.scrollBy(px, 0);
  equateCurrentAndPreviousValues = () => this._previousScrollValue = this._currentScrollValue;
  intermediatePointPredicate = (index): boolean => (this._currentScrollValue - this.scrollValues[index]) * (this._currentScrollValue - this.scrollValues[index + 1]) > 0;
  changeCurrentScroll = (_value) => this._currentScrollValue = _value;


  get scrollValues() {
    if (this._scrollValues)
      return this._scrollValues;

    this._scrollValues = [];
    for (var i = 0; i < this.countItems; i++) {
      this._scrollValues.push(this.scrollStep * i);
    }
    return this._scrollValues;
  }

  get scrollStep() {
    return this.el.nativeElement.scrollWidth / this.countItems;
  }

  get isRightScroll() {
    return this._currentScrollValue > this._previousScrollValue;
  }

  get changeCardNeeded() {
    return this._currentScrollValue > this._previousScrollValue + this.scrollStep / 5 || this._previousScrollValue > this._currentScrollValue + this.scrollStep / 5;
  }

  get leftScrollInCss() {
    return this.el.nativeElement.scrollLeft;
  }


}
