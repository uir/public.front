import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Observable} from "rxjs";
import {DsPlace} from "../../../../../models/dashboard/ds.place.model";
import {Place} from "../../../../../models/place.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-xs-places-item',
  templateUrl: './xs-places-item.component.html',
  styleUrls: ['./xs-places-item.component.scss']
})
export class XsPlacesItemComponent implements OnInit {

  @Input() places$: Observable<DsPlace[]>;

  @ViewChild("cardsContainer")
  cards: ElementRef;

  getUrl(place: Place, tab: string) {
    this.router.navigateByUrl(`result/places/${place.id}/info?tab=${tab}`);
  }

  constructor(private router:Router) {

  }

  ngOnInit() {
  }
}
