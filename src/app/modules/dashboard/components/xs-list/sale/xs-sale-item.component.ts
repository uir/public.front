import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {SaleItem} from "../../../../../models/dashboard/ds.sale.model";

@Component({
  selector: 'app-xs-sale-item',
  templateUrl: './xs-sale-item.component.html',
  styleUrls: ['./xs-sale-item.component.scss']
})
export class XsSaleItemComponent implements OnInit {

  @Input() sales$: Observable<SaleItem[]>;

  constructor() {
  }

  ngOnInit() {
  }

}
