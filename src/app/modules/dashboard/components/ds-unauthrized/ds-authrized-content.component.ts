import { Component, OnInit } from '@angular/core';
import {AuthorizeService} from "../../../auth/services/authorize.service";

@Component({
  selector: 'app-authrized-content',
  templateUrl: './ds-authrized-content.component.html',
  styleUrls: ['./ds-authrized-content.component.css']
})
export class DsAuthrizedContentComponent implements OnInit {

  constructor(public authService: AuthorizeService) { }

  ngOnInit() {
  }

}
