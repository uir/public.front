import {Component, Input, OnInit} from '@angular/core';
import {DsPlace} from "../../../../../models/dashboard/ds.place.model";
import {Observable} from "rxjs";
import {AuthorizeService} from "../../../../auth/services/authorize.service";
import {filter, switchMap} from "rxjs/operators";

@Component({
  selector: 'app-ds-places-list',
  templateUrl: './ds-places-list.component.html',
  styleUrls: ['./ds-places-list.component.css']
})
export class DsPlacesListComponent implements OnInit {
  @Input() authorizedOnly: boolean = false;
  @Input() title: string;
  @Input() items$: Observable<DsPlace[]>;
  @Input() class: string;

  public items: DsPlace[];

  constructor(private authService: AuthorizeService) {
  }

  ngOnInit(): void {
    let req = this.items$;
    if (this.authorizedOnly)
      req = this.authService
        .isAuthorize()
        .pipe(
          filter(x => x),
          switchMap(x => this.items$)
        );
    req.subscribe(x => this.items = x);
  }
}
