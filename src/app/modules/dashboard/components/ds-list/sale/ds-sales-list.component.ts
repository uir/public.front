import {Component, Input} from '@angular/core';
import {SaleItem} from "../../../../../models/dashboard/ds.sale.model";
import {AuthorizeService} from "../../../../auth/services/authorize.service";
import {filter, switchMap} from "rxjs/operators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-ds-sales-list',
  templateUrl: './ds-sales-list.component.html',
  styleUrls: ['./ds-sales-list.component.css'],
})
export class DsSalesListComponent {
  @Input() authorizedOnly: boolean = false;
  @Input() title: string;
  @Input() items$: Observable<SaleItem[]>;
  @Input() class: string;

  public items: SaleItem[];

  constructor(private authService: AuthorizeService) {
  }

  ngOnInit(): void {
    let req = this.items$;
    if (this.authorizedOnly)
      req = this.authService
        .isAuthorize()
        .pipe(
          filter(x => x),
          switchMap(x => this.items$)
        );
    req.subscribe(x => this.items = x);
  }
}
