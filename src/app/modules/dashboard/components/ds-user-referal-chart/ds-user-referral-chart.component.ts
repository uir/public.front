import {Component, OnInit} from '@angular/core';
import {AuthorizeService} from "../../../auth/services/authorize.service";

@Component({
  selector: 'app-ds-user-referal-chart',
  templateUrl: './ds-user-referral-chart.component.html',
  styleUrls: ['./ds-user-referral-chart.component.css']
})
export class DsUserReferralChartComponent implements OnInit {

  constructor(public authService: AuthorizeService) {
  }

  ngOnInit() {
  }

}
