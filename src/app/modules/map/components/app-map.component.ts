import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MapService} from "../map.service";
import {Router} from "@angular/router";
import {MarkerOptions} from "../../common/angular-google-maps/services/google-maps-types";
import {map} from "rxjs/operators";
import {Observable, Subject} from "rxjs";
import {MapMouseEvent, SelectMarkerParams} from "../../common/angular-google-maps/models/select-marker.model";
import {Marker} from "../../../models/marker.model";
import {Place} from "../../../models/place.model";
import {PlacesService} from "../../side-nav/services/places.service";
import {googleMapStyles} from "./google-map.styles";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-map',
  templateUrl: 'app-map.component.html',
  styleUrls: ['app-map.component.css', 'app-map.component_mobile.css']
})
export class AppMapComponent implements OnInit {

  @ViewChild('popover') popover;

  public markers: Observable<MarkerOptions[]>;
  public _markers: Marker[];
  public selectedMarker$: Subject<SelectMarkerParams> = new Subject();
  public latDef = 55.8091019;
  public lngDef = 49.0339922;
  public zoom = 11;

  private _idOptionsMap: Map<number, MarkerOptions> = new Map();
  public popoverPlace: Place;
  public styles = googleMapStyles;

  constructor(
    private mapService: MapService,
    private placeService: PlacesService,
    private router: Router,
    private cd: ChangeDetectorRef,
    public media: ObservableMedia
  ) {
    this.mapService.getMarkers()
      .subscribe(value => this._markers = value);
  }

  ngOnInit(): void {

    if (!this.media.isActive('xs'))
      this.markers = this.mapService
        .getMarkers()
        .pipe(
          map(value =>
            value.map(x => {
              const mapValue = {
                clickable: true,
                position: {lat: x.latitude, lng: x.longitude},
                optimized: true,
                icon: 'assets/icons/location.png',
                selectIcon: 'assets/icons/location_select.png',
                draggable: false
              };
              this._idOptionsMap.set(x.id, mapValue);
              return mapValue;
            }))
        );
    this.mapService
      .getFocusedPlace()
      .subscribe(id => {
        if (id) {
          let option = this._idOptionsMap.get(id);
          this.selectedMarker$.next({options: option, zoom: 14, changeCenter: true});
        } else
          this.selectedMarker$.next(null);
        this.cd.detectChanges();
      })
  }

  onMarkerClick(e: MapMouseEvent) {
    this.router.navigate([this.router.url, "places", this.getIdByMapEvent(e)]);
  }

  onMarkerOut(e: MapMouseEvent) {
    this.selectedMarker$.next({});
    this.popoverPlace = null;
  }

  onMarkerOver(e: MapMouseEvent) {
    const id = this.getIdByMapEvent(e);
    const option = this._idOptionsMap.get(id);
    this.popoverPlace = this.placeService.places
      .getValue()
      .find(y => y.id == id);
    this.selectedMarker$.next({options: option});
  }

  private getIdByMapEvent(e: MapMouseEvent): number {
    const m = this._markers.find(value => value.latitude == e.latLng.lat && value.longitude == e.latLng.lng);
    return m.id;
  }
}
