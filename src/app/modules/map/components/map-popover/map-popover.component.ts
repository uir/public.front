import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {Place} from "../../../../models/place.model";

@Component({
  selector: 'app-map-popover',
  templateUrl: './map-popover.component.html',
  styleUrls: ['./map-popover.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapPopoverComponent {

  @Input() place: Place;

}
