import {Injectable} from "@angular/core";
import {AuthorizeService} from "../auth/services/authorize.service";
import {BehaviorSubject, Observable} from "rxjs";
import {HttpService} from "../../services/http.service";
import {HttpClient} from "@angular/common/http";
import {Marker} from "../../models/marker.model";

@Injectable({providedIn: "root"})
export class MapService extends HttpService {

  private markers: BehaviorSubject<Marker[]>;
  private _zoomPlace: BehaviorSubject<number> = new BehaviorSubject<number>(null);

  constructor(private authServise: AuthorizeService, http: HttpClient) {
    super(http);
    this.markers = new BehaviorSubject<Marker[]>([]);
  }

  public getMarkers(): Observable<Marker[]> {
    return this.markers;
  }

  public clear() {
    this.markers.next([]);
    this.resetFocusPlace();
  }

  public resetFocusPlace() {
    this._zoomPlace.next(null);
  }

  public focusPlace(placeId: number) {
    this._zoomPlace.next(placeId);
  }

  public getFocusedPlace(): Observable<number> {
    return this._zoomPlace;
  }

  public setMarkers(markers: Marker[]) {
    this.clear();
    this.markers.next(markers);
  }
}
