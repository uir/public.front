import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {MatIconModule, MatListModule} from "@angular/material";
import {AppRoutes} from "../../app.routing";
import {AgmCoreModule} from "../common/angular-google-maps/core.module";
import { MapPopoverComponent } from './components/map-popover/map-popover.component';
import {AppMapComponent} from "./components/app-map.component";
import {BemModule} from "angular-bem";

@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    RouterModule.forRoot(AppRoutes),
    MatIconModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyAhdRieYCmfU9EU3nqEYYNH74noY5E_Oac"
    }),
    BemModule
  ],
  declarations: [
    AppMapComponent,
    MapPopoverComponent
  ],
  exports: [
    AppMapComponent
  ]
})
export class AppMapModule {
}
