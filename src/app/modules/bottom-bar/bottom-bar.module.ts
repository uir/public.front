import {BottomBarComponent} from "./bottom-bar.component";
import {AppMapModule} from "../map/app-map.module";
import {HeaderModule} from "../header/header.module";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatSidenavModule,
  MatSnackBarModule,
  MatToolbarModule
} from "@angular/material";
import {ReferralButtonModule} from "../referral/referral-button.module";
import {AuthModule} from "../auth/auth.module";
import {UserLkModule} from "../user-lk/user-lk.module";
import {CategoriesMenuModule} from "../side-nav/side-nav.module";
import {ClickOutsideModule} from "ng-click-outside";


@NgModule({
  declarations: [
    BottomBarComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    AppMapModule,
    UserLkModule,
    CategoriesMenuModule,
    MatDialogModule,
    MatButtonModule,
    MatSnackBarModule,
    AuthModule,
    ReferralButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    ClickOutsideModule
  ],
  exports: [BottomBarComponent]
})
export class BottomBarModule {
}
