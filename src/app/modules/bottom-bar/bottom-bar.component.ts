import {Component} from '@angular/core';
import {MatDialog} from "@angular/material";
import {AuthWindowComponent} from "../auth/components/auth-window/authwindow.component";
import {AuthorizeService} from "../auth/services/authorize.service";
import {BottomBarService, BottomBarType} from "../../services/bottom.bar.service";
import {User} from "../../models/user.model";
import {UserService} from "../auth/services/user.service";

@Component({
  selector: 'bottom-bar',
  templateUrl: `./bottom-bar.component.html`,
  styleUrls: ['bottom-bar.component.css']
})
export class BottomBarComponent{

  public isLogin: boolean;
  public type: BottomBarType;
  public open: boolean;
  public user: User;

  constructor(private authService: AuthorizeService,
              private dialog: MatDialog,
              private userService: UserService,
              private bottombarService: BottomBarService) {
    authService.isAuthorize()
      .subscribe(isLogin => {
        this.isLogin = isLogin;
      });
    this.bottombarService.getType().subscribe(x => {
      this.type = x
    });
    this.bottombarService.open.subscribe(x => {
      this.open = x
    })
    userService
      .getUser()
      .subscribe(value => {
        this.user = value;
      })
  }

  close(drawer) {
    drawer.close();
  }

  openLk(drawer: any) {
    if (!this.isLogin)
      this.dialog.open(AuthWindowComponent, {
        data: {
          activeTab: 'login'
        },
        panelClass: "user-lk-window"
      });
    else
      drawer.toggle()
  }

  openlending(){
    window.open('http://about.uir.one')
  }
}
