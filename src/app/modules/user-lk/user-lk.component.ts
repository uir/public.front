import {Component, OnInit} from '@angular/core';
import {ObservableMedia} from "@angular/flex-layout";
import {FormControl} from "@angular/forms";
import {Router} from "@angular/router";
import {NavigationService} from "../../services/navigation.service";

@Component({
  selector: 'app-user-lk',
  templateUrl: './user-lk.component.html',
  styleUrls: ['./user-lk.component.css', '../../../assets/styles/menu.css', 'user-lk.component.mobile.css']
})
export class UserLkComponent implements OnInit {

  public title = "Партнерская программа";
  selected = new FormControl(0);

  public currentPage: string;
  public page = {
    personal: "personal",
    referal: "referal",
    help: "help",
    tariffs: "tariffs"
  };

  constructor(public media: ObservableMedia,
              private navigationService: NavigationService,
              private router: Router) {
    this.currentPage = this.page.referal;
  }

  ngOnInit() {
  }

  setTitle(title){
    if(title == 0){
      this.title = 'Партнерская программа'
    }
    if(title == 1){
      this.title = 'Тарифные планы'
    }
    if(title == 2){
      this.title = 'Личные данные'
    }
  }

  close(){
    this.router.navigate([this.navigationService.getUrl()])
  }

  isPageShow(page){
    return this.currentPage == page;
  }
}
