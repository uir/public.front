import {FormBuilder, Validators} from '@angular/forms';
import {FormGroup} from '@angular/forms/forms';
import {Injectable} from '@angular/core';
import {UserInfo} from "../model/info";
import {BaseForm} from "../../../forms/base.form";

@Injectable()
export class UserInfoForm extends BaseForm<UserInfo> {



  protected getForm(): FormGroup {
    return this.fb.group({
      name: [],
      birthday: [],
      residenceAddress: [],
      datePassport: [],
      phoneNumber: [],
      email: [],
      passportDepartment: [],
      surname: [],
      middleName: [],
      numberPassport: [
        this.data.numberPassport,
        [
          Validators.pattern("^[0-9]{6}$")
        ]
      ],
      seriesPassport: [
        this.data.seriesPassport,
        [
          Validators.pattern("^[0-9]{4}$")
        ]
      ],
      payPalEmail: [
        this.data.payPalEmail,
        [
          Validators.required,
          Validators.pattern(this.emailPattern)
        ]
      ]
    });
  }

  constructor(private fb: FormBuilder) {
    super({
      id: null,
      name: '',
      tariffId: null,
      endDay: null,
      tariffName: '',
      numberPassport: '',
      seriesPassport: '',
      birthday: '',
      residenceAddress: '',
      datePassport: '',
      passportDepartment: '',
      middleName: '',
      surname: '',
      phoneNumber: '',
      email: '',
      payPalEmail: ''
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }

  protected getValidationMessages() {
    return {
      numberPassport: {
        pattern: "Поле должно состоять из 6 цифр"
      },
      seriesPassport: {
        pattern: "Поле должно состоять из 4 цифр"
    },
      payPalEmail: {
        required: this.required,
        pattern: this.emailPatternError
      }
    };
  }
}
