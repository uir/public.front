import { Component, OnInit } from '@angular/core';
import {UserInfo} from "../model/info";
import {UserInfoForm} from "./user-personal-data.component.form";
import {SnackBarService} from "../../../services/snack-bar.service";
import {UserLkService} from "../services/user-lk.service";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-user-personal-data',
  templateUrl: './user-personal-data.component.html',
  styleUrls: ['./user-personal-data.component.css']
})
export class UserPersonalDataComponent implements OnInit {
  public userInfo: UserInfo;

  constructor(private _userLkService: UserLkService,
              private snackService: SnackBarService,
              public  infoForm: UserInfoForm,
              public media: ObservableMedia) {
    _userLkService.getInfo().subscribe(x => {
      this.userInfo = x;
      this.infoForm.setFormData(this.userInfo)
      this.infoForm.buildForm()
    })
  }

  submit(){
    this._userLkService.edit({...this.infoForm.getValue(),id: this.userInfo.id}).subscribe(x => {
      this.snackService.show('Информация сохранена.');
    })
  }

  ngOnInit() {
  }
}
