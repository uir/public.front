import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {User} from "../../../../models/user.model";
import {UserService} from "../../../auth/services/user.service";
import {DocumentWindowComponent} from "../../../windows/document-window/document-window.component";
import {MatDialog} from "@angular/material";
import {ContactsWindowComponent} from "../../../windows/contacts-window/contacts-window.component";
import {SignOutService} from "../../../auth/services/sign-out.service";

@Component({
  selector: 'lk-dialog',
  templateUrl: './lk-mobile-dialog.html',
  styleUrls: ['./lk-mobile-dialog.css']
})
export class LkMobileDialog {
  public user: User;
  public init: boolean;
  @Input() drawer: any;

  constructor(private signOutService: SignOutService,
              private userService: UserService,
              private dialog: MatDialog,
              private router: Router){
    userService
      .getUser()
      .subscribe(value => {
        this.user = value;
      })
  }

  openInfo(){
    this.dialog.open(DocumentWindowComponent);
  }

  loadContacts() {
    this.dialog.open(ContactsWindowComponent);
  }

  logout() {
    this.signOutService.logout();
    this.close()
  }

  close(){
    this.drawer.toggle()
  }
}
