import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpService} from "../../../services/http.service";
import {Observable} from "rxjs/internal/Observable";

@Injectable({providedIn: "root"})
export class UserReferalService extends HttpService {

  private _refStatistic = environment.api + 'api/user/ref/statistic';
  private _refChart = environment.api + 'api/user/ref/chart';

  getStatistic(): Observable<any> {
    return this.GET(this._refStatistic);
  }

  getChartData(): Observable<any> {
    return this.GET(this._refChart);
  }

}
