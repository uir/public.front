import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpService} from "../../../services/http.service";
import {TariffInfo} from "../model/tariff";
import {UserInfo} from "../model/info";
import {Observable} from "rxjs/internal/Observable";

@Injectable()
export class UserLkService {

  private _getTariffsApi: string = environment.api + "api/user/tariffs";
  private _getInfoApi: string = environment.api + "api/user/lk/info";

  constructor(private http: HttpService) {
  }

  getTariffs(): Observable<TariffInfo> {
    return this.http.GET<TariffInfo>(this._getTariffsApi);
  }

  getInfo(): Observable<UserInfo> {
    return this.http.GET<UserInfo>(this._getInfoApi);
  }

  edit(info): Observable<UserInfo> {
    return this.http.PUT<UserInfo>(this._getInfoApi,info);
  }
}
