import { Component, OnInit } from '@angular/core';
import {TariffInfo} from "../model/tariff";
import {UserLkService} from "../services/user-lk.service";
import {PaymentService} from "../../windows/pay-window/payment.service";
import {MatDialog} from "@angular/material";
import {PayWindowComponent} from "../../windows/pay-window/pay-window/pay-window.component";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-user-tariffs',
  templateUrl: './user-tariffs.component.html',
  styleUrls: ['./user-tariffs.component.css']
})
export class UserTariffsComponent implements OnInit {
  public tariffInfo: TariffInfo = null;
  public error: boolean = false;

  constructor(private _paymentService: PaymentService,
              private _userLkService: UserLkService,
              private dialog: MatDialog,public media: ObservableMedia) {
    _userLkService.getTariffs().subscribe(x => {
      this.tariffInfo = x
    })
  }

  clickTariff(tariff) {
    if (tariff.lines > 0 && !this.tariffInfo.infosFill)
      this.error = true
    else
      this._paymentService.createPayment({paymentPlanId: tariff.id}).subscribe(x => {
        this.dialog.open(PayWindowComponent, {
          panelClass: this.media.isActive('xs') ? "user-lk-window" : "",
            data: {
              tariff: tariff,
              paymentId: x
            },
          }
        )
      })
  }

  ngOnInit() {
  }

}
