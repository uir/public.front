import {Component, Input, OnInit} from '@angular/core';
import {Tariff} from "../../model/tariff";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-user-tariff',
  templateUrl: './user-tariffs-tariff.component.html',
  styleUrls: ['./user-tariffs-tariff.component.css']
})
export class UserTariffsTariffComponent{
  @Input() tariff: Tariff;
  @Input() clickTariff: (tariff) => {}


  constructor(public media: ObservableMedia) {
  }

  getColorTariff(){
    switch (this.tariff.price){
      case 0: {
        return '#00c2d6';
      }
      case 500: {
        return '#00b08c';
      }
      case 2000: {
        return '#ff695e';
      }
      case 3000: {
        return 'rgb(234, 63, 51)';
      }
      default: {
        return '#2d3743';
      }
    }
  }
}
