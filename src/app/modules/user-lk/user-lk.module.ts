import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatButtonModule,
  MatCardModule, MatChipsModule,
  MatIconModule,
  MatInputModule,
  MatListModule, MatMenuModule,
  MatRadioModule,
  MatTableModule,
  MatTabsModule
} from "@angular/material";
import {UserPersonalDataComponent} from './user-personal-data/user-personal-data.component';
import {UserReferalsComponent} from './user-referals/user-referals.component';
import {UserFaqComponent} from './user-faq/user-faq.component';
import {UserLkComponent} from './user-lk.component';
import {ChartsModule} from "ng2-charts";
import {UserReferalStatisticComponent} from './user-referals/user-referal-statistic/user-referal-statistic.component';
import {UserReferalChartComponent} from './user-referals/user-referal-chart/user-referal-chart.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {FlexLayoutModule} from "@angular/flex-layout";
import {UserReferalManyComponent} from "./user-referals/user-referal-many/user-referal-many.component";
import {UserReferalManyTabComponent} from "./user-referals/user-referal-many/user-referal-many-tab/user-referal-many-tab.component";
import {UserTariffsComponent} from "./user-tariffs/user-tariffs.component";
import {UserTariffsTariffComponent} from "./user-tariffs/user-tariffs-tariff/user-tariffs-tariff.component";
import {UserInfoForm} from "./user-personal-data/user-personal-data.component.form";
import {UserLkService} from "./services/user-lk.service";
import {UserReferalService} from "./services/user-referal.service";
import {LkMobileDialog} from "./mobile/lk-mobile-dialog/lk-mobile-dialog";
import { ClickOutsideModule } from 'ng-click-outside';
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatListModule,
    MatIconModule,
    MatTableModule,
    MatTabsModule,
    MatRadioModule,
    ChartsModule,
    ClickOutsideModule,
    RouterModule
  ],
  declarations: [
    UserPersonalDataComponent,
    UserReferalsComponent,
    UserFaqComponent,
    UserLkComponent,
    UserReferalStatisticComponent,
    UserReferalChartComponent,
    UserReferalManyComponent,
    UserReferalManyTabComponent,
    UserTariffsComponent,
    UserTariffsTariffComponent,
    LkMobileDialog
  ],
  providers: [
    UserLkService,
    UserReferalService,
    UserInfoForm
  ],
  exports: [UserLkComponent, UserReferalChartComponent,LkMobileDialog]
})
export class UserLkModule {
}
