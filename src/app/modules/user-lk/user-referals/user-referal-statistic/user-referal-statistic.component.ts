import {Component, OnInit} from '@angular/core';
import {UserReferalService} from "../../services/user-referal.service";

@Component({
  selector: 'app-user-referal-statistic',
  templateUrl: './user-referal-statistic.component.html',
  styleUrls: ['./user-referal-statistic.component.css']
})
export class UserReferalStatisticComponent implements OnInit {
  public dataSource = [];
  public allStatistic = {
    all: 0,
    pay: 0,
    profit: 0
  };

  constructor(private referalService: UserReferalService) {

  }

  ngOnInit() {
    this.referalService
      .getStatistic()
      .subscribe(items => {
        this.dataSource = [];
        this.allStatistic = {
          all: 0,
          pay: 0,
          profit: 0,
        };

        for (let i = 0; i < items.length; i++) {
          this.allStatistic.all += items[i].all;
          this.allStatistic.all += items[i].pay;
          this.allStatistic.all += items[i].profit;
          let lineData = {...items[i], line: i + 1};
          this.dataSource.push(lineData);
        }
      });
  }

}
