import {Component, Input, OnInit} from '@angular/core';
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-user-referals',
  templateUrl: './user-referals.component.html',
  styleUrls: ['./user-referals.component.css', '../../../../assets/styles/menu.css']
})
export class UserReferalsComponent implements OnInit {

  @Input() selectedIndex: any;

  constructor(public media: ObservableMedia) {
  }

  ngOnInit() {
  }

}
