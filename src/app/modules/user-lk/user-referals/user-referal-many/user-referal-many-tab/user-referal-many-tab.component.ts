import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-user-referal-many-tab',
  templateUrl: './user-referal-many-tab.component.html',
  styleUrls: ['./user-referal-many-tab.component.css']
})
export class UserReferalManyTabComponent implements OnInit {
  @Input() title: string;
  @Input() value: string;
  @Input() color: string;
  @Input() img: string;

  constructor() {
  }

  ngOnInit() {
  }
}
