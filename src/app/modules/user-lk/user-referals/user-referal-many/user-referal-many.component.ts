import {Component, Input, OnInit} from '@angular/core';
import {UserLkService} from "../../services/user-lk.service";
import {UserInfo} from "../../model/info";
import {UserReferalService} from "../../services/user-referal.service";
import {ObservableMedia} from "@angular/flex-layout";


@Component({
  selector: 'app-user-referal-many',
  templateUrl: './user-referal-many.component.html',
  styleUrls: ['./user-referal-many.component.css']
})
export class UserReferalManyComponent implements OnInit {
  public allStatistic = {
    allFrients: 0,
    allPay: 0,
    monthPay: 0
  };

  @Input() selectedIndex: any;

  public userInfo: UserInfo;

  constructor(private userLkService: UserLkService, private referalService: UserReferalService,public media: ObservableMedia) {

  }

  ngOnInit() {
    this.referalService
      .getStatistic()
      .subscribe(items => {
        this.allStatistic = {
          allFrients: 0,
          allPay: 0,
          monthPay: 0,
        };
        for (let i = 0; i < items.length; i++) {
          this.allStatistic.allFrients += items[i].all;
          this.allStatistic.allPay += items[i].pay;
          this.allStatistic.monthPay += items[i].profit;
        }
      });

    this.userLkService
      .getInfo()
      .subscribe(x => {
        this.userInfo = x
      })
  }
}
