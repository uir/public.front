import {Component, OnInit} from '@angular/core';
import {UserReferalService} from "../../services/user-referal.service";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-user-referal-chart',
  templateUrl: './user-referal-chart.component.html',
  styleUrls: ['./user-referal-chart.component.css']
})
export class UserReferalChartComponent implements OnInit {
  public detalization = 1;
  public lineChartOptions: any = {
    responsive: true
  };
  public months = [
    {
      number: 1,
      name: 'Январь'
    },
    {
      number: 2,
      name: 'Февраль'
    },
    {
      number: 3,
      name: 'Март'
    },
    {
      number: 4,
      name: 'Апрель'
    },
    {
      number: 5,
      name: 'Май'
    },
    {
      number: 6,
      name: 'Июнь'
    },
    {
      number: 7,
      name: 'Июль'
    },
    {
      number: 8,
      name: 'Август'
    },
    {
      number: 9,
      name: 'Сентябрь'
    },
    {
      number: 10,
      name: 'Октябрь'
    },
    {
      number: 11,
      name: 'Ноябрь'
    },
    {
      number: 12,
      name: 'Декабрь'
    }
  ];

  // lineChart
  public byMonthData = [];
  public byMonthLabels = [];
  public byWeekData = [];
  public byWeekLabels = [];

  constructor(private referalService: UserReferalService) {
  }

  initChart(item) {
    this.byMonthData = [];
    let mdata = item.byMonth.map(v => {
      let month = this.months.filter(value => value.number == v.key)[0].name;
      return {y: v.count, x: month};
    });
    let labels = item.byMonth.map(x => this.months.filter(value => value.number == x.key)[0].name);
    this.byMonthData.push({data: mdata, label: 'Отправленно приглашений'});
    this.byMonthLabels = labels;

    this.byWeekData = [];
    let wdata = item.byWeek.map(v => {
      return {y: v.count, x: v.key};
    });
    let wlabels = item.byWeek.map(v => v.key);
    this.byWeekData.push({data: wdata, label: 'Отправленно приглашений'});
    this.byWeekLabels = wlabels
  }

  change(radio) {

  }

  ngOnInit() {
    this.referalService
      .getChartData()
      .pipe(
        filter(x => x != null)
      )
      .subscribe(item => this.initChart(item));
  }

}
