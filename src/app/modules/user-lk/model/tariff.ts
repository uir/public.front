export class Tariff {
  public name:string;
  public price: number;
  public isActive: boolean;
  public lines: number;
  public color: string;
  public id: number;
}

export class TariffInfo {
  public tariffs: Tariff[]
  public infosFill: boolean
}
