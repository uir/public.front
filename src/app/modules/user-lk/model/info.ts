export class UserInfo {
  id: number;
  name: string;
  tariffId: number;
  endDay: number;
  tariffName: string;
  numberPassport: string;
  seriesPassport: string;
  birthday: string;
  residenceAddress: string;
  datePassport: string;
  passportDepartment: string;
  surname: string;
  middleName: string;
  phoneNumber: string;
  email: string;
  payPalEmail: string;
}
