import {Component, OnInit} from '@angular/core';
import {TokenForm} from "../../../forms/token.form";
import {RegForm} from "../../../forms/reg.form";
import {RegistrationService} from "../../../services/registration.service";
import {RegisterConfirm} from "../../../models/register-confirm.model";
import {Router} from "@angular/router";
import {WindowService} from "../../../../../services/window.service";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-register-stepper',
  templateUrl: './register-stepper.component.html',
  styleUrls: ['./register-stepper.component.css']
})
export class RegisterStepperComponent implements OnInit {

  constructor(public tokenForm: TokenForm,
              public regForm: RegForm,
              public media: ObservableMedia,
              private router: Router,
              private winService: WindowService,
              private regServise: RegistrationService) {
  }

  ngOnInit() {
    this.regForm.buildForm();
    this.tokenForm.buildForm();
  }

  reg() {
    if (this.regForm.isValid()) {
      this.regServise
        .register(this.regForm.getValue())
        .subscribe();
    }
  }

  confirm() {
    if (this.tokenForm.isValid()) {
      let data: RegisterConfirm = {...this.tokenForm.getValue(), ...this.regForm.form.value};
      this.regServise
        .confirm(data)
        .subscribe(value => {
          this.router.navigate(['/']);
          this.winService.closeAll();
          this.regForm.reset();
        });
    }
  }
}
