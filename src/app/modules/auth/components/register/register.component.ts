import {Component, Inject, OnInit, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {Router} from "@angular/router";
import {ObservableMedia} from "@angular/flex-layout";
import {WindowBaseComponent} from "../../../windows/window-base.component";
import {AuthWindowComponent} from "../auth-window/authwindow.component";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css','./register.component.mobile.css']
})
export class RegisterComponent extends WindowBaseComponent<RegisterComponent> implements OnInit {
  public isShowTokenForm: boolean = false;

  constructor(@Optional() @Inject(MAT_DIALOG_DATA) dialogData: any,
              router: Router,
              dr: MatDialogRef<RegisterComponent>,
              private dialog: MatDialog,
              public media: ObservableMedia) {
    super(dialogData, router, dr);
  }

  close() {
    this.dr.close();
    this.dialog.open(AuthWindowComponent, {
      panelClass: "user-lk-window"
    });
  }
}
