import {Component, Input, OnInit} from '@angular/core';
import {TokenForm} from "../../../forms/token.form";
import {RegistrationService} from "../../../services/registration.service";
import {RegisterConfirm} from "../../../models/register-confirm.model";
import {Router} from "@angular/router";
import {RegForm} from "../../../forms/reg.form";
import {WindowService} from "../../../../../services/window.service";

@Component({
  selector: 'app-register-confirm',
  templateUrl: './register-confirm.component.html',
  styleUrls: ['./register-confirm.component.css']
})
export class RegisterConfirmComponent implements OnInit {

  @Input() regForm;
  @Input() tokenForm;
  public phone: string;
  public password: string;

  constructor() {
  }

  ngOnInit() {
    if (this.regForm.isValid()) {
      let data = this.regForm.getValue();
      this.phone = data.phone;
      this.password = data.password;
    }
  }
}
