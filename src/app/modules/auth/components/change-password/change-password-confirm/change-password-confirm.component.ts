import {Component, Input, OnInit} from '@angular/core';
import {ChangePasswordConfirmForm} from "../../../forms/change-password";
import {RegistrationService} from "../../../services/registration.service";
import {WindowService} from "../../../../../services/window.service";
import {ChangePasswordConfirm} from "../../../models/change-password-confirm.model";

@Component({
  selector: 'app-change-password-confirm',
  templateUrl: './change-password-confirm.component.html',
  styleUrls: ['./change-password-confirm.component.css']
})
export class ChangePasswordConfirmComponent implements OnInit {

  @Input() phone: string;

  constructor(
    public form: ChangePasswordConfirmForm,
    private regService: RegistrationService,
    private windowService: WindowService) {
  }

  ngOnInit() {
    this.form.buildForm();
  }

  confirm() {
    if (this.form.isValid()) {
      let data: ChangePasswordConfirm = {...this.form.getValue(), ...{phone: this.phone}};
      this.regService
        .changepasswordConfirm(data)
        .subscribe(value => this.windowService.closeAll());
    }
  }
}
