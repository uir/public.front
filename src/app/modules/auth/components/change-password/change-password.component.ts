import {Component, OnInit} from '@angular/core';
import {ChangePasswordForm} from "../../forms/change-password";
import {RegistrationService} from "../../services/registration.service";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  public isShowConfirm: boolean = false;

  constructor(public form: ChangePasswordForm, private regService: RegistrationService) {
  }

  ngOnInit() {
    this.form.buildForm();
  }

  submit() {
    if (this.form.isValid())
      this.regService.changepassword(this.form.getValue())
        .subscribe(value => {
          this.isShowConfirm = true;
        });
  }
}
