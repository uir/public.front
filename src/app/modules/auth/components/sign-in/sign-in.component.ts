import {Component, Inject, OnInit, Optional} from '@angular/core';
import {LoginForm} from "../../forms/login.form";
import {SignInService} from "../../services/sign-in.service";
import {WindowService} from "../../../../services/window.service";
import {ObservableMedia} from "@angular/flex-layout";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {WindowBaseComponent} from "../../../windows/window-base.component";
import {Router} from "@angular/router";
import {AuthWindowComponent} from "../auth-window/authwindow.component";
import {RegisterComponent} from "../register/register.component";
import {RegForm} from "../../forms/reg.form";
import {RegistrationService} from "../../services/registration.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css','./sign-in.component.mobile.css']
})
export class SignInComponent extends WindowBaseComponent<SignInComponent> implements OnInit {
  public showChangePassword: boolean;

  constructor(@Optional() @Inject(MAT_DIALOG_DATA) dialogData: any,
              router: Router,
              dr: MatDialogRef<SignInComponent>,
              public media: ObservableMedia,
              private dialog: MatDialog,
             private regServise: RegistrationService,
              public loginForm: LoginForm,
              private authService: SignInService) {
    super(dialogData, router, dr);
    this.showChangePassword = false;
  }

  ngOnInit() {
    this.loginForm.buildForm();
  }


  close(){
    this.dr.close();
    this.dialog.open(AuthWindowComponent, {
      panelClass: "user-lk-window"
    });
  }

  login() {
    if (this.loginForm.form.valid)
      this.authService
        .login(this.loginForm.form.value)
        .subscribe(value =>  this.dr.close());
  }
}
