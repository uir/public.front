import {Component, Inject, Optional} from '@angular/core';
import {WindowBaseComponent} from "../../../windows/window-base.component";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {RegisterComponent} from "../register/register.component";
import {SignInComponent} from "../sign-in/sign-in.component";
import {Router} from "@angular/router";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'authwindow',
  templateUrl: 'authwindow.template.html',
  styleUrls: ['authwindow.styles.css']
})
export class AuthWindowComponent extends WindowBaseComponent<AuthWindowComponent>{
  public activeTab: string;

  protected get url(): string {
    return "auth";
  }

  protected dialogDataResolver(dialogData: any) {
    this.activeTab = dialogData == null ? '' : dialogData.activeTab;
  }

  getIndex(): number {
    let index = 0;
    if (this.activeTab != null && this.activeTab === 'reg')
      index = 1;
    return index;
  }

  //mobile
  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) dialogData: any,
    router: Router,
    dr: MatDialogRef<AuthWindowComponent>,
    private dialog: MatDialog,
    public media: ObservableMedia,) {
    super(dialogData, router, dr);
  }

  sigIn(){
    this.dr.close();
    this.dialog.open(SignInComponent, {
      panelClass: "user-lk-window"
    });
  }

  register(){
    this.dr.close();
    this.dialog.open(RegisterComponent, {
      panelClass: ["user-lk-window","window-register"]
    });
  }

}
