import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthWindowComponent} from "./components/auth-window/authwindow.component";
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule, MatDialogModule, MatInputModule, MatStepperModule, MatTabsModule} from "@angular/material";
import {RegisterComponent} from "./components/register/register.component";
import {ChangePasswordComponent} from "./components/change-password/change-password.component";
import {ChangePasswordConfirmComponent} from "./components/change-password/change-password-confirm/change-password-confirm.component";
import {RegisterConfirmComponent} from "./components/register/register-confirm/register-confirm.component";
import {ChangePasswordConfirmForm, ChangePasswordForm} from "./forms/change-password";
import {RegForm} from "./forms/reg.form";
import {TokenForm} from "./forms/token.form";
import {LoginForm} from "./forms/login.form";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {CookieService} from "angular2-cookie/core";
import {RegistrationService} from "./services/registration.service";
import {SignInComponent} from "./components/sign-in/sign-in.component";
import {AuthInterceptor} from "./services/auth.interceptor";
import {UserService} from "./services/user.service";
import {SignInService} from "./services/sign-in.service";
import {SignOutService} from "./services/sign-out.service";
import {DialogMobileModule} from "../common/dialog-mobile/dialog-mobile.module";
import {RegisterStepperComponent} from './components/register/register-stepper/register-stepper.component';
import {RegisterFormComponent} from './components/register/register-form/register-form.component';

export function cookieServiceFactory() {
  return new CookieService();
}

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    DialogMobileModule,
    MatStepperModule
  ],
  declarations: [
    AuthWindowComponent,
    SignInComponent,
    RegisterComponent,
    RegisterConfirmComponent,
    ChangePasswordComponent,
    ChangePasswordConfirmComponent,
    RegisterStepperComponent,
    RegisterFormComponent
  ],
  providers: [
    {provide: CookieService, useFactory: cookieServiceFactory},
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    LoginForm,
    ChangePasswordForm,
    ChangePasswordConfirmForm,
    RegForm,
    TokenForm,
    SignInService,
    SignOutService,
    RegistrationService,
    UserService
  ],
  bootstrap: [
    AuthWindowComponent,SignInComponent,RegisterComponent
  ]
})
export class AuthModule {
}
