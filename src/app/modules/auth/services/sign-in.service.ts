import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Token} from "../models/token.model";
import {Auth} from "../models/auth.model";
import {CookieService} from "angular2-cookie/core";
import {HttpService} from "../../../services/http.service";
import {environment} from "../../../../environments/environment";
import {UserService} from "./user.service";
import {BookmarkService} from "../../../services/bookmark.service";
import {AuthorizeService} from "./authorize.service";
import {Observable} from "rxjs";
import {switchMap, tap} from "rxjs/operators";

@Injectable()
export class SignInService extends HttpService {

  private _userLogin = environment.api + 'token';

  constructor(
    http: HttpClient,
    private cookieService: CookieService,
    private userService: UserService,
    private authService: AuthorizeService,
    private bookmarkService: BookmarkService) {
    super(http);
  }

  login(data: Auth): Observable<any> {
    const auth = 'grant_type=password&username=' + data.login.replace('+', '%2B') + '&password=' + data.password;
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
    return this.POST<Token>(this._userLogin, auth, headers)
      .pipe(
        tap(value => this.cookieService.putObject('token_user', value)),
        tap(() => this.authService.set(true)),
        switchMap(() => this.userService.fetchUser()),
        switchMap(() => this.bookmarkService.fetchBookmarks())
      );
  }
}
