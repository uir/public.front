import {Injectable} from '@angular/core';
import {HttpService} from "../../../services/http.service";
import {User} from "../../../models/user.model";
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {CookieService} from "angular2-cookie/core";
import {AuthorizeService} from "./authorize.service";
import {BehaviorSubject, Observable} from "rxjs";
import {filter, tap} from "rxjs/operators";


@Injectable()
export class UserService extends HttpService {

  private _userInfo = environment.api + 'api/user/info';
  private _user$: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  constructor(http: HttpClient, private cookieService: CookieService, private authService: AuthorizeService) {
    super(http);
    if (this.cookieService.getObject("token_user") != null)
      this.fetchUser().subscribe();
    this.authService
      .isAuthorize()
      .pipe(
        filter(value => !value)
      )
      .subscribe(value => this._user$.next(null))
  }

  getUser(): Observable<User> {
    return this._user$;
  }

  fetchUser(): Observable<User> {
    return this.GET<User>(this._userInfo)
      .pipe(tap(x => this._user$.next(x)));
  }

  setUser(user: User) {
    this._user$.next(user);
  }
}
