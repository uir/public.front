import {Injectable} from '@angular/core';
import {Register} from "../models/register.model";
import {HttpClient} from "@angular/common/http";
import {SignInService} from "./sign-in.service";
import {RegisterConfirm} from "../models/register-confirm.model";
import {HttpService} from "../../../services/http.service";
import {environment} from "../../../../environments/environment";
import {NotificationService} from "../../../services/notification.service";
import {ChangePasswordConfirm} from "../models/change-password-confirm.model";
import {Observable} from "rxjs/internal/Observable";
import {switchMap, tap} from "rxjs/operators";

@Injectable()
export class RegistrationService extends HttpService {

  private _userReg = environment.api + 'api/auth/reg';
  private _userRegConfirm = environment.api + 'api/auth/confirm';
  private _changePassword = environment.api + 'api/auth/changepassword';
  private _changePasswordConfirm = environment.api + 'api/auth/changepassword/confirm';


  constructor(
    http: HttpClient,
    private signInOutService: SignInService,
    private notifyService: NotificationService) {
    super(http);
  }

  confirm(data: RegisterConfirm): Observable<any> {
    return this.POST(this._userRegConfirm, data)
      .pipe(
        tap(_ => this.notifyService.notify('Регистрация успешно завершена')),
        switchMap(value => this.signInOutService.login({
          password: data.password,
          login: data.phone
        })));
  }

  register(data: Register): Observable<number> {
    return this.POST<number>(this._userReg, data);
  }

  changepassword(data): Observable<any> {
    return this.POST(this._changePassword, data);
  }

  changepasswordConfirm(data: ChangePasswordConfirm): Observable<any> {
    return this.POST(this._changePasswordConfirm, data)
      .pipe(
        tap(_ => this.notifyService.notify('Пароль изменен')),
        switchMap(value => this.signInOutService.login({
          password: data.password,
          login: data.phone
        })));
  }

}
