import {Injectable} from '@angular/core';
import {CookieService} from "angular2-cookie/core";
import {AuthorizeService} from "./authorize.service";

@Injectable()
export class SignOutService {

  constructor(
    private cookieService: CookieService,
    private authService: AuthorizeService) {
  }

  logout() {
    this.cookieService.remove('token_user');
    this.authService.set(false);
  }
}
