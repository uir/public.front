import {Injectable} from "@angular/core";
import {CookieService} from "angular2-cookie/core";
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";
import {Observable} from "rxjs/internal/Observable";

@Injectable({providedIn: "root"})
export class AuthorizeService {
  private _isAuthorize$: BehaviorSubject<boolean>;

  constructor(private cookieService: CookieService) {
    this._isAuthorize$ = new BehaviorSubject<boolean>(this.cookieService.getObject("token_user") != null)
  }

  isAuthorize(): Observable<boolean> {
    return this._isAuthorize$;
  }

  isAuthorizeSnapshot(): boolean {
    return this._isAuthorize$.getValue();
  }

  set(value: boolean){
    this._isAuthorize$.next(value);
  }
}
