import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpHeaders, HttpErrorResponse
} from '@angular/common/http';
import {CookieService} from "angular2-cookie/core";
import {Token} from "../models/token.model";
import {OperationError} from "../../../models/operation-error.model";
import {SignOutService} from "./sign-out.service";
import {Router} from "@angular/router";
import {NotificationService} from "../../../services/notification.service";
import {EMPTY, Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private notification: NotificationService,
    private router: Router,
    private cookieService: CookieService,
    private signOutService: SignOutService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      withCredentials: true,
      headers: this.getHeaders(request.headers)
    });
    return next
      .handle(request)
      .pipe(
        catchError(error => {
          if (error instanceof HttpErrorResponse) {
            if (error.status == 403) {
              this.signOutService.logout();
              let res = error.error as OperationError;
              this.notification.notify(res.message);
              return EMPTY;
            }
            if (error.status == 401) {
              this.signOutService.logout();
              this.router.navigate(['auth']);
              return EMPTY;
            }
          }
          return throwError(error);
        }));
  }

  private getHeaders(oldHeaders?: HttpHeaders): HttpHeaders {
    let headers = new HttpHeaders();

    if (oldHeaders)
      headers = oldHeaders;

    let token = this.cookieService.getObject("token_user") as Token;
    if (token != null) {
      headers = headers.set('Authorization', 'bearer ' + token.access_token)
    }

    if (!headers.has("content-Type"))
      headers = headers.set('content-Type', 'application/json');

    return headers;
  }
}
