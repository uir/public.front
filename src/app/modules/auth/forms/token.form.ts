import {FormBuilder, Validators} from '@angular/forms';
import {FormGroup} from '@angular/forms/forms';
import {Injectable} from '@angular/core';
import {BaseForm} from "../../../forms/base.form";

@Injectable()
export class TokenForm extends BaseForm<{code:string}> {
  protected getForm(): FormGroup {
    return this.fb.group({
      code: [
        this.data.code,
        [
          Validators.required,
          Validators.pattern(/\d{6}/)
        ]
      ]
    });
  }

  protected getValidationMessages() {
    return {
      code: {
        required: this.required,
        pattern: "Неправильный формат. (6 цифр)"
      },
    };
  }

  constructor(private fb: FormBuilder) {
    super({
      code: null,
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }
}
