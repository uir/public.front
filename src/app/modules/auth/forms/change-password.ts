import {FormBuilder, Validators} from '@angular/forms';
import {FormGroup} from '@angular/forms/forms';
import {Injectable} from '@angular/core';
import {BaseForm} from "../../../forms/base.form";

@Injectable()
export class ChangePasswordForm extends BaseForm<{phone:string}> {
  protected getForm(): FormGroup {
    return this.fb.group({
      phone: [
        this.data.phone,
        [
          Validators.required,
          Validators.pattern(/^\+[0-9]{11}$/)
        ]
      ]
    });
  }

  protected getValidationMessages() {
    return {
      phone: {
        required: this.required,
        pattern: "Неверный формат. Пример +7хххххххххх"
      },
    };
  }

  constructor(private fb: FormBuilder) {
    super({
      phone: null,
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }
}

@Injectable()
export class ChangePasswordConfirmForm extends BaseForm<{code:string, password:string}> {
  protected getForm(): FormGroup {
    return this.fb.group({
      code: [
        this.data.code,
        [
          Validators.required,
          Validators.pattern(/\d{6}/)
        ]
      ],
      password: [
        this.data.password,
        [
          Validators.required,
          Validators.pattern(/^\S{6,25}$/)
        ]
      ]
    });
  }

  protected getValidationMessages() {
    return {
      code: {
        required: this.required,
        pattern: "Неправильный формат. (6 цифр)"
      },
      password: {
        required: this.required,
        pattern: "Количество символов от 6 до 25"
      },
    };
  }

  constructor(private fb: FormBuilder) {
    super({
      code: null,
      password: null
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }
}
