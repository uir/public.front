import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Injectable} from '@angular/core';
import {Register} from "../models/register.model";
import {BaseForm} from "../../../forms/base.form";

@Injectable()
export class RegForm extends BaseForm<Register> {

  //private emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  protected getForm(): FormGroup {
    return this.fb.group({
      name: [
        this.data.name,
        [
          Validators.required,
          Validators.pattern(/^\S{2,25}$/)
        ]
      ],
      email: [
        this.data.email,
        [
          Validators.required,
          Validators.pattern(this.emailPattern)
        ]
      ],
      phone: [
        this.data.phone,
        [
          Validators.required,
          Validators.pattern(/^\+[0-9]{11}$/)
        ]
      ],
      password: [
        this.data.password,
        [
          Validators.required,
          Validators.pattern(/^\S{6,25}$/)
        ]
      ],
      parentId: [this.data.parentId]
    });
  }

  protected getValidationMessages() {
    return {
      name: {
        required: this.required,
        pattern: "Количество символов от 2 до 25"
      },
      email: {
        required: this.required,
        pattern: this.emailPatternError
      },
      phone: {
        required: this.required,
        pattern: "Неверный формат. Пример +7хххххххххх"
      },
      password: {
        required: this.required,
        pattern: "Количество символов от 6 до 25"
      }
    };
  }

  constructor(private fb: FormBuilder) {
    super({
      email: '',
      login: '',
      name: '',
      password: '',
      phone: '',
      parentId: 0
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }
}
