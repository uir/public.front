import {FormBuilder, Validators} from '@angular/forms';
import {FormGroup} from '@angular/forms/forms';
import {Injectable} from '@angular/core';
import {Auth} from "../models/auth.model";
import {BaseForm} from "../../../forms/base.form";

@Injectable()
export class LoginForm extends BaseForm<Auth> {
  protected getForm(): FormGroup {
    return this.fb.group({
      login: [
        this.data.login,
        [
          Validators.required,
          Validators.pattern(/^\+[0-9]{11}$/)
        ]
      ],
      password: [
        this.data.password,
        [
          Validators.required
        ]
      ]
    });
  }

  protected getValidationMessages() {
    return {
      password: {
        required: this.required
      },
      login: {
        required: this.required,
        pattern: "Неверный формат. Пример +7хххххххххх"
      }
    };
  }

  constructor(private fb: FormBuilder) {
    super({
      login: '',
      password: '',
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }
}
