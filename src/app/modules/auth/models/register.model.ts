import {Auth} from "./auth.model";

export class Register extends Auth {
  email: string;
  phone: string;
  name: string;
  parentId: number;
}
