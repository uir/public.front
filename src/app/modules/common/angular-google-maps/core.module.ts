import {ModuleWithProviders, NgModule} from '@angular/core';
import {AgmMap} from './directives/map';
import {
  LAZY_MAPS_API_CONFIG,
  LazyMapsAPILoader,
  LazyMapsAPILoaderConfigLiteral
} from './services/maps-api-loader/lazy-maps-api-loader';
import {MapsAPILoader} from './services/maps-api-loader/maps-api-loader';
import {BROWSER_GLOBALS_PROVIDERS} from './utils/browser-globals';
import {GoogleMarkerService} from "./services/google-marker.service";

@NgModule({declarations: [AgmMap], exports: [AgmMap]})
export class AgmCoreModule {
  /**
   * Please use this method when you register the module at the root level.
   */
  static forRoot(lazyMapsAPILoaderConfig?: LazyMapsAPILoaderConfigLiteral): ModuleWithProviders {
    return {
      ngModule: AgmCoreModule,
      providers: [
        GoogleMarkerService,
        ...BROWSER_GLOBALS_PROVIDERS, {provide: MapsAPILoader, useClass: LazyMapsAPILoader},
        {provide: LAZY_MAPS_API_CONFIG, useValue: lazyMapsAPILoaderConfig}
      ],
    };
  }
}
