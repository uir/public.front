import {LatLngLiteral, MarkerOptions} from "../services/google-maps-types";

export declare type MouseEventType = "click" | "mouseover" | "mouseout";

export class SelectMarkerParams {
  options?: MarkerOptions;
  zoom?: number = 14;
  changeCenter?: boolean;
}

export class MapMouseEvent {

  constructor(latLng: LatLngLiteral, x: number, y: number, type: MouseEventType) {
    this.latLng = latLng;
    this.x = x;
    this.type = type;
    this.y = y;
  }

  latLng: LatLngLiteral;
  x: number;
  y: number;
  type: MouseEventType;
}
