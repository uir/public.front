import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';
import {Observable, Subject, Subscription} from 'rxjs';
import {GoogleMapsAPIWrapper} from '../services/google-maps-api-wrapper';
import {
  FullscreenControlOptions,
  LatLngBounds,
  LatLngBoundsLiteral,
  LatLngLiteral,
  MapTypeControlOptions,
  MapTypeStyle,
  MarkerOptions,
  PanControlOptions,
  RotateControlOptions,
  ScaleControlOptions,
  StreetViewControlOptions,
  ZoomControlOptions
} from '../services/google-maps-types';
import {GoogleMarkerService} from "../services/google-marker.service";
import {MapMouseEvent, SelectMarkerParams} from "../models/select-marker.model";

/**
 * AgmMap renders a Google Map.
 * **Important note**: To be able see a map in the browser, you have to define a height for the
 * element `agm-map`.
 *
 * ### Example
 * ```typescript
 * import { Component } from '@angular/core';
 *
 * @Component({
 *  selector: 'my-map-cmp',
 *  styles: [`
 *    agm-map {
 *      height: 300px;
 *    }
 * `],
 *  template: `
 *    <agm-map [latitude]="lat" [longitude]="lng" [focusPlace]="focusPlace">
 *    </agm-map>
 *  `
 * })
 * ```
 */
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'agm-map',
  providers: [
    GoogleMapsAPIWrapper
  ],
  host: {
    // todo: deprecated - we will remove it with the next version
    '[class.sebm-google-map-container]': 'true'
  },
  styles: [`
    .agm-map-container-inner {
      width: inherit;
      height: inherit;
    }

    .agm-map-content {
      display: none;
    }
  `],
  template: `
    <div class='agm-map-container-inner sebm-google-map-container-inner'></div>
    <div class='agm-map-content'>
      <ng-content></ng-content>
    </div>
  `
})
export class AgmMap implements OnInit, OnDestroy {
  /**
   * The longitude that defines the center of the map.
   */
  @Input() longitude: number = 0;

  /**
   * The latitude that defines the center of the map.
   */
  @Input() latitude: number = 0;

  /**
   * The focusPlace level of the map. The default focusPlace level is 8.
   */
  @Input() zoom: number = 8;

  /**
   * The minimal focusPlace level of the map allowed. When not provided, no restrictions to the focusPlace level
   * are enforced.
   */
  @Input() minZoom: number;

  /**
   * The maximal focusPlace level of the map allowed. When not provided, no restrictions to the focusPlace level
   * are enforced.
   */
  @Input() maxZoom: number;

  /**
   * Enables/disables if map is draggable.
   */
    // tslint:disable-next-line:no-input-rename
  @Input('mapDraggable') draggable: boolean = true;

  /**
   * Enables/disables focusPlace and center on double click. Enabled by default.
   */
  @Input() disableDoubleClickZoom: boolean = false;

  /**
   * Enables/disables all default UI of the Google map. Please note: When the map is created, this
   * value cannot get updated.
   */
  @Input() disableDefaultUI: boolean = false;

  /**
   * If false, disables scrollwheel zooming on the map. The scrollwheel is enabled by default.
   */
  @Input() scrollwheel: boolean = true;

  /**
   * Color used for the background of the Map div. This color will be visible when tiles have not
   * yet loaded as the user pans. This option can only be set when the map is initialized.
   */
  @Input() backgroundColor: string;

  /**
   * The name or url of the cursor to display when mousing over a draggable map. This property uses
   * the css  * cursor attribute to change the icon. As with the css property, you must specify at
   * least one fallback cursor that is not a URL. For example:
   * [draggableCursor]="'url(http://www.example.com/icon.png), auto;'"
   */
  @Input() draggableCursor: string;

  /**
   * The name or url of the cursor to display when the map is being dragged. This property uses the
   * css cursor attribute to change the icon. As with the css property, you must specify at least
   * one fallback cursor that is not a URL. For example:
   * [draggingCursor]="'url(http://www.example.com/icon.png), auto;'"
   */
  @Input() draggingCursor: string;

  /**
   * If false, prevents the map from being controlled by the keyboard. Keyboard shortcuts are
   * enabled by default.
   */
  @Input() keyboardShortcuts: boolean = true;

  /**
   * The enabled/disabled state of the Zoom control.
   */
  @Input() zoomControl: boolean = true;

  /**
   * Options for the Zoom control.
   */
  @Input() zoomControlOptions: ZoomControlOptions;

  /**
   * Styles to apply to each of the default map types. Note that for Satellite/Hybrid and Terrain
   * modes, these styles will only apply to labels and geometry.
   */
  @Input() styles: MapTypeStyle[] = [];

  /**
   * When true and the latitude and/or longitude values changes, the Google Maps panTo method is
   * used to
   * center the map. See: https://developers.google.com/maps/documentation/javascript/reference#Map
   */
  @Input() usePanning: boolean = false;

  /**
   * The initial enabled/disabled state of the Street View Pegman control.
   * This control is part of the default UI, and should be set to false when displaying a map type
   * on which the Street View road overlay should not appear (e.g. a non-Earth map type).
   */
  @Input() streetViewControl: boolean = true;

  /**
   * Options for the Street View control.
   */
  @Input() streetViewControlOptions: StreetViewControlOptions;

  /**
   * Sets the viewport to contain the given bounds.
   */
  @Input() fitBounds: LatLngBoundsLiteral | LatLngBounds = null;

  /**
   * The initial enabled/disabled state of the Scale control. This is disabled by default.
   */
  @Input() scaleControl: boolean = false;

  /**
   * Options for the scale control.
   */
  @Input() scaleControlOptions: ScaleControlOptions;

  /**
   * The initial enabled/disabled state of the Map type control.
   */
  @Input() mapTypeControl: boolean = false;

  /**
   * Options for the Map type control.
   */
  @Input() mapTypeControlOptions: MapTypeControlOptions;

  /**
   * The initial enabled/disabled state of the Pan control.
   */
  @Input() panControl: boolean = false;

  /**
   * Options for the Pan control.
   */
  @Input() panControlOptions: PanControlOptions;

  /**
   * The initial enabled/disabled state of the Rotate control.
   */
  @Input() rotateControl: boolean = false;

  /**
   * Options for the Rotate control.
   */
  @Input() rotateControlOptions: RotateControlOptions;

  /**
   * The initial enabled/disabled state of the Fullscreen control.
   */
  @Input() fullscreenControl: boolean = false;

  /**
   * Options for the Fullscreen control.
   */
  @Input() fullscreenControlOptions: FullscreenControlOptions;

  /**
   * The map mapTypeId. Defaults to 'roadmap'.
   */
  @Input() mapTypeId: 'roadmap' | 'hybrid' | 'satellite' | 'terrain' | string = 'roadmap';

  /**
   * When false, map icons are not clickable. A map icon represents a point of interest,
   * also known as a POI. By default map icons are clickable.
   */
  @Input() clickableIcons: boolean = true;

  /**
   * This setting controls how gestures on the map are handled.
   * Allowed values:
   * - 'cooperative' (Two-finger touch gestures pan and focusPlace the map. One-finger touch gestures are not handled by the map.)
   * - 'greedy'      (All touch gestures pan or focusPlace the map.)
   * - 'none'        (The map cannot be panned or zoomed by user gestures.)
   * - 'auto'        [default] (Gesture handling is either cooperative or greedy, depending on whether the page is scrollable or not.
   */
  @Input() gestureHandling: 'cooperative' | 'greedy' | 'none' | 'auto' = 'auto';

  @Input() markers$: Observable<MarkerOptions[]>;

  @Input() selectedMarker$: Subject<SelectMarkerParams>;

  private _observableSubscriptions: Subscription[] = [];

  @Output() clickMarker: EventEmitter<MapMouseEvent> = new EventEmitter<MapMouseEvent>();
  @Output() mouseOverMarker: EventEmitter<MapMouseEvent> = new EventEmitter<MapMouseEvent>();
  @Output() mouseOutMarker: EventEmitter<MapMouseEvent> = new EventEmitter<MapMouseEvent>();

  private _markersCache: MarkerOptions[];

  constructor(
    private markerService: GoogleMarkerService,
    private cd: ChangeDetectorRef,
    private _elem: ElementRef,
    private _mapsWrapper: GoogleMapsAPIWrapper) {
    this.cd.detach();
  }

  ngOnInit() {
    this.initMap();
    this.initInputObservablesHandlers();
  }

  private initInputObservablesHandlers() {
    if (this.markers$)
      this.markers$.subscribe((value: MarkerOptions[]) => this.initMarkers(value));

    if (this.selectedMarker$)
      this.selectedMarker$.subscribe(value => this.selectedMarkerHandler(value));
  }

  private initMap() {
    const container = this._elem.nativeElement.querySelector('.agm-map-container-inner');
    this._initMapInstance(container);
  }

  private selectedMarkerHandler(value) {
    if (value)
      this.selectMarker(value);
    else
      this.resetMap();
  }

  private initMarkers(value: MarkerOptions[]) {
    this._markersCache = value;
    if (value === null || value.length === 0)
      this.markerService.clear();
    else
      this._mapsWrapper
        .createMarkers(this._markersCache)
        .then(x => {
          this.markerService.init(this._markersCache, x);
          this.initMarkerEvents();
        })
  }

  private initMarkerEvents() {
    this.markerService.addClickEvents()
      .subscribe(e => {
        this.clickMarker.emit(e);
      });
    this.markerService.addMouseOverOutEvents("mouseout")
      .subscribe(e => {
        this.mouseOutMarker.emit(e);
      });
    this.markerService.addMouseOverOutEvents("mouseover")
      .subscribe(e => {
        this.mouseOverMarker.emit(e);
      });
  }

  private selectMarker(value) {
    this.markerService.select(value.options);
    if (value.zoom)
      this._mapsWrapper.setZoom(value.zoom);
    if (value.changeCenter)
      this._mapsWrapper.setCenter(value.options.position as LatLngLiteral);
  }

  private resetMap() {
    this._mapsWrapper.setZoom(this.zoom);
    this._mapsWrapper.setCenter({lat: this.latitude || 0, lng: this.longitude || 0});
  }

  private _initMapInstance(el: HTMLElement) {
    this._mapsWrapper.createMap(el, {
      center: {lat: this.latitude || 0, lng: this.longitude || 0},
      zoom: this.zoom,
      minZoom: this.minZoom,
      maxZoom: this.maxZoom,
      disableDefaultUI: this.disableDefaultUI,
      disableDoubleClickZoom: this.disableDoubleClickZoom,
      scrollwheel: this.scrollwheel,
      backgroundColor: this.backgroundColor,
      draggable: this.draggable,
      draggableCursor: this.draggableCursor,
      draggingCursor: this.draggingCursor,
      keyboardShortcuts: this.keyboardShortcuts,
      styles: this.styles,
      zoomControl: this.zoomControl,
      zoomControlOptions: this.zoomControlOptions,
      streetViewControl: this.streetViewControl,
      streetViewControlOptions: this.streetViewControlOptions,
      scaleControl: this.scaleControl,
      scaleControlOptions: this.scaleControlOptions,
      mapTypeControl: this.mapTypeControl,
      mapTypeControlOptions: this.mapTypeControlOptions,
      panControl: this.panControl,
      panControlOptions: this.panControlOptions,
      rotateControl: this.rotateControl,
      rotateControlOptions: this.rotateControlOptions,
      fullscreenControl: this.fullscreenControl,
      fullscreenControlOptions: this.fullscreenControlOptions,
      mapTypeId: this.mapTypeId,
      clickableIcons: this.clickableIcons,
      gestureHandling: this.gestureHandling
    })
      .then(() => this._mapsWrapper.getNativeMap());

    this._handleMapZoomChange();
    this._handleIdleEvent();
  }

  ngOnDestroy() {
    this._observableSubscriptions.forEach((s) => s.unsubscribe());
    this._mapsWrapper.clearInstanceListeners();
  }

  private _handleMapZoomChange() {
    const s = this._mapsWrapper.subscribeToMapEvent('zoom_changed').subscribe(() => {
      this._mapsWrapper.getZoom().then((z: number) => {
        this.markerService.hide();
      });
    });
    this._observableSubscriptions.push(s);
  }

  private _handleIdleEvent() {
    const s = this._mapsWrapper
      .subscribeToMapEvent('idle')
      .subscribe(map => {
        this.markerService.show(map);
      });
    this._observableSubscriptions.push(s);
  }
}
