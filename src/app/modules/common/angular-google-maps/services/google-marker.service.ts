import {Injectable} from "@angular/core";
import {Marker, MarkerOptions} from "./google-maps-types";
import {fromEventPattern, merge, Observable} from "rxjs";
import {buffer, bufferCount, filter, map, tap} from "rxjs/operators";
import {MapMouseEvent, MouseEventType} from "../models/select-marker.model";

@Injectable()
export class GoogleMarkerService {

  private _isHide = false;
  private _markers: Marker[] = [];
  private _optionMarkerMap: Map<MarkerOptions, Marker> = new Map();
  private selectedMarkerOptions: MarkerOptions = null;

  public init(options: MarkerOptions[], markers: Marker[]) {
    options.forEach((value, i) => {
      this._optionMarkerMap.set(value, markers[i]);
      this._markers.push(markers[i]);
    });
  }

  public clear() {
    this.setMapForAll(null);
    this._markers = [];
  }

  public hide() {
    this._isHide = true;
    this.setMapForAll(null);
  }

  public show(map) {
    if (this._isHide) {
      this.setMapForAll(map);
      this._isHide = false;
    }
  }

  public select(option: MarkerOptions) {
    if (!option) {
      let marker = this._optionMarkerMap.get(this.selectedMarkerOptions);
      if (marker) {
        marker.setIcon(this.selectedMarkerOptions.icon);
        this.selectedMarkerOptions = null;
      }
      return;
    }
    if (option.icon && option.selectIcon) {
      let marker = this._optionMarkerMap.get(option);
      marker.setIcon(option.selectIcon);
      if (this.selectedMarkerOptions) {
        let selectedMarker = this._optionMarkerMap.get(this.selectedMarkerOptions);
        selectedMarker.setIcon(this.selectedMarkerOptions.icon);
      }
      this.selectedMarkerOptions = option;
    }
    else
      throw "Marker not have icon or selectedIcon"
  }

  private setMapForAll(map) {
    for (const m of this._markers) {
      m.setMap(map);
    }
  }

  public addClickEvents(): Observable<MapMouseEvent> {
    return this.addEvents("click");
  }

  public addMouseOverOutEvents(event: MouseEventType): Observable<MapMouseEvent> {
    let domEvent = fromEventPattern(handler =>
      document.addEventListener("mousemove", evt => handler(evt)));

    const mapEvent = merge(...this._markers.map(m => {
      return fromEventPattern(handler => m.addListener(event, handler));
    }));

    const bufferedDomEvent = domEvent.pipe(
      buffer(mapEvent),
      map(value => value[value.length - 1])
    );

    return merge(mapEvent, bufferedDomEvent).pipe(
      bufferCount(2),
      filter(value => value[1] != null),
      map(([m, e]: [any, any]) => {
        return new MapMouseEvent({lat: m.latLng.lat(), lng: m.latLng.lng()}, e.x, e.y, event);
      })
    );
  }

  private addEvents(event: MouseEventType): Observable<MapMouseEvent> {
    let events = [];
    for (let m of this._markers) {
      let clickEvent = fromEventPattern(handler => m.addListener(event, handler));
      events.push(clickEvent);
    }

    let mapClick = fromEventPattern(handler =>
      document.addEventListener(event, evt => handler(evt)));

    events.push(mapClick);

    return merge(...events)
      .pipe(
        bufferCount(2),
        filter(([mapClick, click]) => click["latLng"] || mapClick["latLng"]),
        map(([mapClick, click]: [any, any]) => {
          if (click["latLng"])
            return new MapMouseEvent({lat: click.latLng.lat(), lng: click.latLng.lng()}, mapClick.x, mapClick.y, event);
          if (mapClick["latLng"])
            return new MapMouseEvent({lat: mapClick.latLng.lat(), lng: mapClick.latLng.lng()}, click.x, click.y, event);
        })
      );
  }
}
