import {Component, Input} from '@angular/core';
import {DiscountType} from "../../../models/sale";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-sale-item',
  template: `
    <div block="sale-item">
      <app-sale-amount [amount]="amount" [type]="type" *ngIf="!media.isActive('xs')"></app-sale-amount>
      <div *ngIf="media.isActive('xs')">
        item
      </div>
      <div block="item-body">
        <div elem="sale-name" class="item-sales-overflow" style="-webkit-box-orient: vertical;">{{name}}</div>
        <div elem="owner-name">{{organizationName}}</div>
      </div>
    </div>
  `,
  styles: [`
    .sale-item {
      cursor: pointer;
      display: flex;
      height: 100%;
    }

    .item-body__owner-name {
      font-size: 12px;
      color: gray;
    }
  `]
})
export class SaleItemComponent {

  @Input() amount: number;
  @Input() type: DiscountType;
  @Input() organizationName: string;
  @Input() name: string;
  @Input() isExclusive: boolean;

  constructor(public media: ObservableMedia) {

    console.log('media', media.isActive('xs'));
  }

}
