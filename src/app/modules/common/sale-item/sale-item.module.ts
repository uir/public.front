import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SaleAmountComponent} from './sale-amount.component';
import {SaleItemComponent} from './sale-item.component';
import {BemModule} from "angular-bem";

@NgModule({
  imports: [
    CommonModule,
    BemModule
  ],
  declarations: [SaleAmountComponent, SaleItemComponent],
  exports: [SaleAmountComponent, SaleItemComponent]
})
export class SaleItemModule { }
