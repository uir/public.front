import {Component, Input} from '@angular/core';
import {DiscountType} from "../../../models/sale";

@Component({
  selector: 'app-sale-amount',
  template: `
    <div block="discount-type" [mod]="getClass()" [title]="type === 1 ? 'Акция' : 'Скидка ' + amount + '%'">
      <span *ngIf="type === 1">A</span>
      <span *ngIf="type === 0">{{amount}}%</span>
    </div>
  `,
  styles: [`
    :host {
      display: flex;
    }
  `]
})
export class SaleAmountComponent {
  @Input() amount;
  @Input() type: DiscountType;

  public getClass(): string {
    switch (this.type) {
      case DiscountType.Common:
        return 'common';
      case DiscountType.Stock:
        return 'stock';
    }
  }
}
