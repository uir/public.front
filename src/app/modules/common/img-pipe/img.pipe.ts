import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'img'
})
export class ImgPipe implements PipeTransform {

  transform(value: string, width: number, height: number, rmode: string = 'crop'): string {
    return value === null ? 'assets/icons/device-camera-icon.png' : value + `?width=${width}&height=${height}&rmode=${rmode}`;
  }

}
