import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GalleryComponent} from './gallery/gallery.component';
import {GalleryViewComponent} from './gallery-view/gallery-view.component';
import {GalleryFullComponent} from './gallery-full/gallery-full.component';
import {MatIconModule} from "@angular/material";
import {FlexLayoutModule} from "@angular/flex-layout";

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    FlexLayoutModule
  ],
  exports: [GalleryComponent, GalleryViewComponent, GalleryFullComponent],
  declarations: [GalleryComponent, GalleryViewComponent, GalleryFullComponent],
})
export class ImageGalleryModule {
}
