import {Injectable} from '@angular/core';
import {IImage} from "./model/image.model";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable()
export class ImageGalleryService {

  private selectedImage: BehaviorSubject<IImage>;
  private images: BehaviorSubject<IImage[]>;
  private _isShow: BehaviorSubject<boolean>;

  constructor() {
    this.images = new BehaviorSubject<IImage[]>([]);
    this.selectedImage = new BehaviorSubject<IImage>(null);
    this._isShow = new BehaviorSubject<boolean>(false);
  }

  show(selectedImage: IImage, images: IImage[]) {
    this.selectedImage.next(selectedImage);
    this.images.next(images);
    this._isShow.next(true);
  }

  hide() {
    this.images.next([]);
    this.selectedImage.next(null);
    this._isShow.next(false);
  }

  getImages(): Observable<IImage[]> {
    return this.images;
  }

  getSelectedImage(): Observable<IImage> {
    return this.selectedImage;
  }

  isShow(): Observable<boolean> {
    return this._isShow;
  }
}
