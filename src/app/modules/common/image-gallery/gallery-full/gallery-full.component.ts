import {Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {IImage} from "../model/image.model";
import {ImageGalleryService} from "../image-gallery.service";
import {fromEvent} from "rxjs";
import {debounceTime} from "rxjs/operators";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'gallery-full',
  templateUrl: './gallery-full.component.html',
  styleUrls: ['./gallery-full.component.css']
})
export class GalleryFullComponent implements OnInit, OnChanges {

  @Input() images: IImage[];
  @Input() class: string;
  @ViewChild('galleryContainer') galleryContainer: ElementRef;
  public width: number;
  public currentImage: IImage;
  public isGalleryViewShow: boolean;

  constructor(private imageGalleryService: ImageGalleryService,private media: ObservableMedia) {
    this.isGalleryViewShow = false;
    this.currentImage = null;
    fromEvent(window, 'resize')
      .pipe(debounceTime(500))
      .subscribe((event) => {
        this.width = this.galleryContainer.nativeElement.clientWidth;
      });
  }

  ngOnInit() {
  }

  onImageClick(img: IImage) {
    this.imageGalleryService.show(img, this.images);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.images) {
      this.width = this.media.isActive('xs') ? window.screen.width : this.galleryContainer.nativeElement.clientWidth;
    }
  }

}
