import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import {IImage} from "../model/image.model";

@Component({
  selector: 'image-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnChanges{
  @Input() imagesSrc: IImage[];
  @Input() width: number = 0;
  @Input('flexBorderSize') providedImageMargin: number = 3;
  @Input('flexImageSize') providedImageSize: number = 3;
  @Output() onImageClick = new EventEmitter<IImage>();
  public gallery: any[];

  private images: IImage[];

  constructor() {
    this.images = [];
    this.gallery = [];
  }

  ngOnChanges() {
    this.images = this.imagesSrc.slice();
    this.render();
  }

  public openImageViewer(img) {
    this.onImageClick.emit(img);
  }

  private render() {
    if (this.images == null || this.images.length == 0 || this.getGalleryWidth() == 0 || this.getGalleryWidth() == null)
      return;

    this.gallery = []

    let tempRow = [this.images[0]]
    let rowIndex = 0
    let i = 0

    for (i; i < this.images.length; i++) {
      while (this.images[i + 1] && this.shouldAddCandidate(tempRow, this.images[i + 1])) {
        i++
      }
      if (this.images[i + 1]) {
        tempRow.pop()
      }
      this.gallery[rowIndex++] = tempRow

      tempRow = [this.images[i + 1]]
    }
    this.scaleGallery()
  }

  private shouldAddCandidate(imgRow: any[], candidate: any): boolean {
    let oldDifference = this.calcIdealHeight() - this.calcRowHeight(imgRow)
    imgRow.push(candidate)
    let newDifference = this.calcIdealHeight() - this.calcRowHeight(imgRow)

    return Math.abs(oldDifference) > Math.abs(newDifference)
  }

  private calcRowHeight(imgRow: any[]) {
    let originalRowWidth = this.calcOriginalRowWidth(imgRow)

    let ratio = (this.getGalleryWidth() - (imgRow.length - 1) * this.calcImageMargin()) / originalRowWidth
    let rowHeight = imgRow[0]['height'] * ratio

    return rowHeight
  }

  private calcImageMargin() {
    let galleryWidth = this.getGalleryWidth()
    let ratio = galleryWidth / 1920
    return Math.round(Math.max(1, this.providedImageMargin * ratio))
  }

  private calcOriginalRowWidth(imgRow: any[]) {
    let originalRowWidth = 0
    imgRow.forEach((img) => {
      let individualRatio = this.calcIdealHeight() / img['height']
      img['width'] = img['width'] * individualRatio
      img['height'] = this.calcIdealHeight()
      originalRowWidth += img['width']
    })

    return originalRowWidth
  }

  private calcIdealHeight() {
    return this.getGalleryWidth() / (80 / this.providedImageSize) + 100
  }

  private getGalleryWidth() {
    return this.width;
  }

  private scaleGallery() {
    let maximumGalleryImageHeight = 0

    this.gallery.forEach((imgRow) => {
      let originalRowWidth = this.calcOriginalRowWidth(imgRow);

      if (imgRow !== this.gallery[this.gallery.length - 1]) {
        let galleryWidth = this.getGalleryWidth();
        let imageMargin = this.calcImageMargin();
        let ratio = (galleryWidth - (imgRow.length - 1) * imageMargin) / originalRowWidth;

        imgRow.forEach((img: any) => {
          let ratioWidth = Math.round(img.width * ratio);
          img.width = ratioWidth == 0 ? img.width : ratioWidth;
          let ratioHeigth = Math.round(img.height * ratio);
          img.height = ratioHeigth == 0 ? img.height : ratioHeigth;
          maximumGalleryImageHeight = Math.max(maximumGalleryImageHeight, img.height);
        })
      }
      else {
        imgRow.forEach((img: any) => {
          img.width = Math.round(img.width);
          img.height = Math.round(img.height);
          maximumGalleryImageHeight = Math.max(maximumGalleryImageHeight, img['height'])
        })
      }
    });
  }
}
