export interface IImage {
  file: string;
  height: number;
  width: number;
}
