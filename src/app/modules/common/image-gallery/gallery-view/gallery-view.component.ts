import {Component, EventEmitter, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {IImage} from "../model/image.model";
import {ImageGalleryService} from "../image-gallery.service";

@Component({
  selector: 'gallery-view',
  templateUrl: './gallery-view.component.html',
  styleUrls: ['./gallery-view.component.css'],
  animations: [
    trigger('showViewerTransition', [
      state('true', style({
        opacity: 1
      })),
      state('void', style({
        opacity: 0
      })),
      transition('void => *', [
        style({
          opacity: 0
        }),
        animate('500ms ease-in')]
      ),
      transition('* => void', [
        style({
          opacity: 1
        }),
        animate('500ms ease-out')]
      )
    ])
  ]
})
export class GalleryViewComponent {
  @Output() onClose = new EventEmitter();
  public showViewer: boolean = false;
  public img: IImage;
  public imgs: IImage[];

  constructor(private imagesService: ImageGalleryService) {
    imagesService.getSelectedImage().subscribe(x => this.img = x);
    imagesService.isShow().subscribe(x => this.showViewer = x);
    imagesService.getImages().subscribe(x => this.imgs = x);
  }

  next() {
    let index = this.imgs.indexOf(this.img);
    index = index + 1;
    if (index >= this.imgs.length)
      index = 0;
    this.img = this.imgs[index];
  }

  back() {
    let index = this.imgs.indexOf(this.img);
    index = index - 1;
    if (index < 0)
      index = this.imgs.length - 1;
    this.img = this.imgs[index];
  }

  select(img: IImage) {
    this.img = img;
  }

  closeViewer() {
    this.showViewer = false;
    this.img = null;
    this.onClose.emit();
    this.imagesService.hide();
  }
}
