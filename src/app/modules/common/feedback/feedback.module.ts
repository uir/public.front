import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FeedbackComponent} from './feedback/feedback.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MatButtonModule, MatIconModule, MatInputModule} from "@angular/material";
import {FeedbackForm} from "./feedback.form";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule
  ],
  declarations: [FeedbackComponent],
  providers: [
    FeedbackForm
  ],
  exports: [FeedbackComponent]
})
export class FeedbackModule {
}
