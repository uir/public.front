import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Injectable} from "@angular/core";
import {BaseForm} from "../../../forms/base.form";

export class Order {
  email: string;
  message: string;
}

@Injectable()
export class FeedbackForm extends BaseForm<Order> {

  protected getForm(): FormGroup {
    return this.fb.group({
      email: [
        this.data.email,
        [
          Validators.required,
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")
        ]
      ],
      message: [
        this.data.message,
        [
          Validators.required
        ]
      ]
    });
  }

  protected getValidationMessages() {
    return {
      email: {required: this.required, pattern: "Неверный формат. Пример : uir@yandex.ru"},
      message: {required: this.required}
    };
  }

  constructor(private fb: FormBuilder) {
    super({
      email: "",
      message: ""
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }
}
