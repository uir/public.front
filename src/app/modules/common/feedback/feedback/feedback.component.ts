import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FeedbackForm} from "../feedback.form";
import {FeedbackService} from "../feedback.service";

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  @Output("submit") _submit: EventEmitter<any> = new EventEmitter();

  constructor(public form: FeedbackForm, private feedbackService: FeedbackService) {
  }

  ngOnInit() {
    this.form.buildForm();
  }

  submit() {
    if (this.form.isValid()) {
      let value = this.form.getValue();
      this.feedbackService.feedback(value).subscribe();
      this._submit.emit(value);
    }
  }

}
