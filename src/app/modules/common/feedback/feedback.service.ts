import {Injectable} from '@angular/core';
import {HttpService} from "../../../services/http.service";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {Feedback} from "../mat-feedback/entity/feedback";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Injectable()
export class FeedbackService extends HttpService {


  constructor(http: HttpClient, private router: Router) {
    super(http);
  }

  private _feedback = environment.api + 'api/user/callback';
  private _feedbackReport = environment.api + 'api/user/feedback';

  public feedback(data): Observable<any> {
    return this.POST<any>(this._feedback, data, null);
  }

  public feedbackReport(data: Feedback) {
    this.POST<any>(this._feedbackReport, {
      text: data.description,
      url: this.router.url,
      platform: navigator.platform,
      screenWidth: screen.width,
      screenHeight: screen.height,
      userAgent: navigator.userAgent
    }, null).subscribe();
  }

}
