import { Component, Input, OnInit } from '@angular/core';

import { Tag } from '../../../models/tag';

@Component({
  selector: 'app-tags-list',
  templateUrl: './tags-list.component.html',
  styleUrls: ['./tags-list.component.css']
})
export class TagsListComponent implements OnInit {

  @Input() tags: Tag[]
  constructor() { }

  ngOnInit() {
  }

}
