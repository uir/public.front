import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TagsListComponent} from "./tags-list.component";
import {MatChipsModule} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    MatChipsModule
  ],
  declarations: [TagsListComponent],
  exports: [TagsListComponent]
})
export class TagListModule {
}
