import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatIconModule} from "@angular/material";
import {DialogMobileComponent} from "./dialog-mobile";

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule
  ],
  declarations: [
    DialogMobileComponent
  ],
  entryComponents: [
    DialogMobileComponent
  ],
  exports: [DialogMobileComponent]
})
export class DialogMobileModule {
}
