import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'dialog-mobile',
  templateUrl: 'dialog-mobile.html',
  styleUrls: ['dialog-mobile.css']
})
export class DialogMobileComponent{

  @Input() title: string = "";
  @Input() matButton: boolean = false;
  @Input() icon: string = "";
  @Input() buttonClose: string = "Отмена";
  @Input() buttonSave: string = "Далее";
  @Input() disabledSave: boolean = false;
  @Input() hiddenButton: boolean = false;
  @Input() hiddenSave: boolean = false;
  @Input() hiddenClose: boolean = false;
  @Input() colorHeader: string = '#00838f';
  @Input() bodyClass = 'dialog-mobile__body';
  @Input() headerClose: boolean = false;

  @Output() close  = new EventEmitter<any>();
  @Output() save  = new EventEmitter<any>();

  closeEmit(){
    this.close.emit()
  }

  saveEmit(){
    this.save.emit()
  }
}




