import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class FeedbackService {
  private feedbackSource = new Subject();
  public feedback$ = this.feedbackSource.asObservable();

  public setFeedback(feedback) {
    this.feedbackSource.next(feedback);
  }

}
