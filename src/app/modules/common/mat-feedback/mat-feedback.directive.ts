import {Directive, HostListener, EventEmitter, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {FeedbackDialogComponent} from './feedback-dialog/feedback-dialog.component';
import {FeedbackService} from './feedback.service';

@Directive({selector: '[mat-feedback]'})
export class MatFeedbackDirective {
  @Output()
  public onSend = new EventEmitter<object>();

  public constructor(private dialogRef: MatDialog, private feedbackService: FeedbackService) {
    this.feedbackService.feedback$.subscribe(x => {
      this.onSend.emit(x);
      this.dialogRef.closeAll();
    });
  }

  @HostListener('click')
  public onClick() {
    this.openFeedbackDialog();
  }

  public openFeedbackDialog() {
    let dialogRef = this.dialogRef.open(FeedbackDialogComponent, {
      panelClass: 'feedback-window',
      disableClose: true,
      height: 'auto',
      width: 'auto'
    });
  }
}
