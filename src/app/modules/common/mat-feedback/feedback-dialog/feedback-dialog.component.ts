import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {Feedback} from '../entity/feedback';
import {FeedbackService} from '../feedback.service';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'feedback-dialog',
  templateUrl: './feedback-dialog.component.html',
  styleUrls: ['./feedback-dialog.component.css']
})

export class FeedbackDialogComponent {
  public title: string;
  public descriptionTips: string;
  public feedback = new Feedback();

  constructor(public dialogRef: MatDialogRef<FeedbackDialogComponent>,
              private feedbackService: FeedbackService) {
    this.title = 'Сообщить об ошибке';
    this.descriptionTips = 'Напишите подробные шаги вызвавшие ошибку, чтобы наши специалисты могли устранить ее в кратчайшие сроки';
    this.feedback = new Feedback();
    this.feedback.description = '';
  }

  public send() {
    this.feedbackService.setFeedback(this.feedback);
  }
}
