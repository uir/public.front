import {Injectable, ModuleWithProviders, NgModule} from '@angular/core';
import {ActivationStart, Router} from "@angular/router";
import {filter, tap} from "rxjs/operators";
import {HttpClient, HttpClientModule} from "@angular/common/http";

export class ClickLogConf {
  api: string;
}

@Injectable()
export class ClickLogService {

  constructor(router: Router, private http: HttpClient, private conf: ClickLogConf) {
    router.events.pipe(
      filter(x => x instanceof ActivationStart && x.snapshot.data.useClickLog === true),
      tap((x: ActivationStart) => this.request(x))
    )
      .subscribe(x => {
        console.log(x)
      })
  }

  private request(x: ActivationStart) {
    this.http
      .post<any>(this.conf.api, {
        url: location.pathname,
        handler: x.snapshot.data.handler,
        data: this.getParamsData(x)
      })
      .subscribe()
  }

  private getParamsData(x: ActivationStart): any {
    return x.snapshot.pathFromRoot
      .map(x => x.params)
      .reduce((p, c) => {
        return {...p, ...c}
      }, {})
  }
}

@NgModule({
  imports: [
    HttpClientModule
  ]
})
export class ClickLogModule {
  public static forRoot(conf: ClickLogConf): ModuleWithProviders[] {
    return [
      {
        ngModule: ClickLogModule,
        providers: [
          ClickLogService,
          {provide: ClickLogConf, useValue: conf}
        ]
      },
    ];
  }
}
