import {Component, Input, OnInit} from '@angular/core';

import {LoaderService} from "../loader.service";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  @Input() allowAlways = false;
  @Input() diameter: number = 100;
  @Input() keyWords: string[];
  public isActive: Observable<boolean>;

  constructor(private loaderService: LoaderService) {
    this.isActive = loaderService.isActive().pipe(map(value => value.status));
  }

  ngOnInit(): void {
    this.loaderService.keyWords = this.keyWords;
  }
}
