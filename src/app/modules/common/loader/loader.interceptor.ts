import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {LoaderService} from "./loader.service";
import {Observable} from "rxjs";
import {finalize} from "rxjs/operators";

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private loaderService: LoaderService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.validateByKeyWords(request))
      return this.addRequest(request, next);
    else
      return next.handle(request);
  }

  validateByKeyWords(request: HttpRequest<any>): boolean {
    if (this.loaderService.keyWords == null || this.loaderService.keyWords.length == 0)
      return true;
    let result = true;
    for (let w of this.loaderService.keyWords) {
      result = result && request.url.includes(w);
    }
    return result;
  }

  addRequest(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.loaderService.addRequestStatus({
      url: request.url,
      status: true
    });
    return next
      .handle(request)
      .pipe(
        finalize(() => {
          this.loaderService.addRequestStatus({
            url: request.url,
            status: false
          })
        })
      );
  }
}
