import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BookmarkControlComponent} from './bookmark-control/bookmark-control.component';
import {MatButtonModule, MatIconModule} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule
  ],
  declarations: [BookmarkControlComponent],
  exports: [BookmarkControlComponent]
})
export class BookmarkControlModule {
}
