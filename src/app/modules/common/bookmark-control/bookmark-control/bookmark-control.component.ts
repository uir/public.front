import {ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {AuthorizeService} from "../../../auth/services/authorize.service";
import {WindowService} from "../../../../services/window.service";
import {BookmarkService} from "../../../../services/bookmark.service";

@Component({
  selector: 'app-bookmark-control',
  templateUrl: './bookmark-control.component.html',
  styleUrls: ['./bookmark-control.component.css']
})
export class BookmarkControlComponent implements OnInit, OnDestroy {

  @Input() placeId: number;
  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();

  public isBookmark: boolean;
  private sub;

  constructor(
    public cd: ChangeDetectorRef,
    private authService: AuthorizeService,
    private bookmarkService: BookmarkService,
    private windowService: WindowService) {
  }

  ngOnInit() {
    this.sub = this.bookmarkService
      .isBookmark(this.placeId)
      .subscribe(x => {
        this.isBookmark = x;
        this.change.emit(x);
      });
  }

  controlBookmark(e) {
    e.stopPropagation();
    e.preventDefault();
    if (!this.isBookmark) {
      if (!this.authService.isAuthorizeSnapshot())
        this.windowService.showAuth();
      else
        this.bookmarkService
          .addBookmark(this.placeId)
          .subscribe(() => {
            this.isBookmark = true;
            this.change.emit(this.isBookmark);
          });
    } else
      this.bookmarkService
        .deleteBookmark(this.placeId)
        .subscribe(() => {
          this.isBookmark = false;
          this.change.emit(this.isBookmark);
        });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
