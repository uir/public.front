import {Component, Input} from '@angular/core';
import {MatDialog} from "@angular/material";

@Component({
  selector: 'app-close-button',
  templateUrl: './close-button.component.html',
  styleUrls: ['./close-button.component.css']
})
export class CloseButtonComponent {

  @Input() isAbsolute: boolean = false;
  @Input() color: string = "black";

  constructor(private dialog: MatDialog) {
  }

  close() {
    this.dialog.closeAll();
  }
}
