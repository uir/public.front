import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CloseButtonComponent} from "./close-button.component";
import {MatButtonModule, MatIconModule} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule
  ],
  declarations: [
    CloseButtonComponent
  ],
  exports: [CloseButtonComponent]
})
export class CloseButtonModule {
}
