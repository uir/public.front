import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from "./header.comp";
import {ContactsComponent} from "./components/contacts/contacts.component";
import {DocumentsComponent} from "./components/documents/documents.component";
import {LoginComponent} from "./components/login/auth.component";
import {NotificationComponent} from "./components/notification/notification.component";
import {SearchComponent} from "./components/search/search.component";
import {TagInputModule} from "ngx-chips";
import {
  MatButtonModule,
  MatDialogModule,
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatTabsModule
} from "@angular/material";
import {FlexLayoutModule} from "@angular/flex-layout";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {AppRoutes} from "../../app.routing";
import {SearchTagInputDropdownComponent} from "./components/search/components/search-tag-input-dropdown/search-tag-input-dropdown.component";
import {Ng2DropdownModule} from "ng2-material-dropdown";
import {BemModule} from "angular-bem";
import {SaleItemModule} from "../common/sale-item/sale-item.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(AppRoutes),
    ReactiveFormsModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    TagInputModule,
    Ng2DropdownModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatListModule,
    MatDialogModule,
    MatTabsModule,
    BemModule,
    SaleItemModule
  ],
  declarations: [
    HeaderComponent,
    ContactsComponent,
    DocumentsComponent,
    LoginComponent,
    NotificationComponent,
    SearchComponent,
    SearchTagInputDropdownComponent
  ],
  exports: [
    HeaderComponent
  ]
})
export class HeaderModule {
}
