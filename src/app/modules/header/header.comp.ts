import {Component, Input} from '@angular/core';
import {UserService} from "../auth/services/user.service";
import {User} from "../../models/user.model";
import {MapService} from "../map/map.service";
import {Router} from "@angular/router";
import {ActiveTemplateService} from "../side-nav/services/active-template.service";
import {ObservableMedia} from "@angular/flex-layout";
import {SearchService} from "./services/search.service";

@Component({
  selector: 'header-app',
  templateUrl: 'header.component.html',
  styleUrls: ['header.style.css', 'header.style-mobile.css']
})
export class HeaderComponent{
  public user: User;
  public isLogin: boolean = false;
  @Input() drawer: any;

  constructor(
    public media: ObservableMedia,
    private activeTemplateService: ActiveTemplateService,
    private userService: UserService,
    private mapService: MapService,
    private searchService: SearchService,
    private router: Router) {
    userService
      .getUser()
      .subscribe(value => {
        this.user = value;
        this.isLogin = true;
      })


  }
  navigate() {
    this.router
      .navigate(['/'])
      .then(() => {
        this.activeTemplateService.setActive(0);
        this.mapService.clear();
      });
  }
}
