import {Injectable} from '@angular/core';
import {HttpService} from "../../../services/http.service";
import {environment} from "../../../../environments/environment";
import {Tag} from "../../../models/tag";
import {HttpParams} from "@angular/common/http";
import {Search} from "../../../models/search.model";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class SearchService extends HttpService {
  private _search = environment.api + 'api/search';
  private _tagsByIdsApi = environment.api + 'api/tags';

  private _searchResult: BehaviorSubject<Search> = new BehaviorSubject<Search>(null);
  private _tagsInputResult: BehaviorSubject<Tag[]> = new BehaviorSubject<Tag[]>([]);
  private _isShow: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  getSearchResult() {
    return this._searchResult;
  }

  getTagsInputResult() {
    return this._tagsInputResult;
  }

  isShow(): Observable<boolean> {
    return this._isShow;
  }

  hide() {
    this._isShow.next(false);
  }

  show() {
    this._isShow.next(true);
  }

  searchTagsByIds(ids: number[]) {
    if (!ids)
      return;
    if (ids.length == 0)
      return;
    let params: HttpParams = new HttpParams();
    let idsStr = ids
      .map(n => "" + n)
      .reduce((total, current) => total + ',' + current);
    params = params.set('ids', idsStr);
    return this.GET<Tag[]>(this._tagsByIdsApi, params)
      .subscribe(value => this._tagsInputResult.next(value));
  }

  searchTags(name: string) {
    let params: HttpParams = new HttpParams();
    params = params.set('text', name);
    this.GET<Search>(this._search, params)
      .subscribe(value => this._searchResult.next(value));
  }

  clearInputTags() {
    this._tagsInputResult.next([]);
  }
}
