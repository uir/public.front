import {Component, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material";
import {ObservableMedia} from "@angular/flex-layout";
import {UserLkWindowComponent} from "../../../windows/user-lk-window/user-lk-window.component";
import {WindowService} from "../../../../services/window.service";
import {SignOutService} from "../../../auth/services/sign-out.service";
import {AuthorizeService} from "../../../auth/services/authorize.service";
import {Observable} from "rxjs";

@Component({
  selector: 'login',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.styles.css']
})
export class LoginComponent implements OnInit {

  public isLogin$: Observable<boolean>;

  constructor(
    private dialog: MatDialog,
    private windows: WindowService,
    private media: ObservableMedia,
    private authService: AuthorizeService,
    private signOutService: SignOutService) {
    this.isLogin$ = authService.isAuthorize();
  }

  openAuth() {
    this.windows.showAuth();
  }

  logout() {
    this.signOutService.logout();
  }

  loadRecent() {
  }

  referals() {
    this.dialog.open(UserLkWindowComponent, {
      panelClass: !this.media.isActive("xs") ? "user-lk-window" : ["user-lk-window", "window_mobile"]
    });
  }

  ngOnInit() {
  }

}
