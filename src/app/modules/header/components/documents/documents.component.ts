import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material";
import {DocumentWindowComponent} from "../../../windows/document-window/document-window.component";

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  loadDocuments() {
    this.dialog.open(DocumentWindowComponent);
  }

}
