import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Tag} from '../../../../models/tag';
import {Router} from "@angular/router";
import {SnackBarService} from "../../../../services/snack-bar.service";
import {SaleSearchResult, Search, SearchResult, SearchResultType} from "../../../../models/search.model";
import {Observable, of, timer} from "rxjs";
import {filter, map, tap} from "rxjs/operators";
import {SearchService} from "../../services/search.service";
import {AuthorizeService} from "../../../auth/services/authorize.service";
import {AuthWindowComponent} from "../../../auth/components/auth-window/authwindow.component";
import {MatDialog} from "@angular/material";
import {TagModel} from "ngx-chips/core/accessor";
import {TagInputComponent, TagInputDropdown} from "ngx-chips";
import {ActiveTemplateService} from "../../../side-nav/services/active-template.service";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css', 'search.style-mobile.css']
})
export class SearchComponent implements OnInit {

  public searchResult;
  public inputTags: any[];
  public isFocus = false;
  public isLogin: boolean;

  @Input() drawer: any;
  @ViewChild('dropdown') public dropdown: TagInputDropdown;
  @ViewChild('input') public input: TagInputComponent;

  constructor(
    public media: ObservableMedia,
    private tagService: SearchService,
    private router: Router,
    private dialog: MatDialog,
    private snackService: SnackBarService,
    private activeTemplateService: ActiveTemplateService,
    private authService: AuthorizeService) {

    authService.isAuthorize()
      .subscribe(isLogin => {
        this.isLogin = isLogin;
      });

    this.inputTags = [];
    this.searchResult = this.tagService
      .getSearchResult()
      .pipe(
        filter(value => value != null),
        map((x: Search) => {
          let items = [];
          if (x.categories.length > 0) {
            x.categories.forEach(i => i.type = SearchResultType.Category);
            items.push(...x.categories);
          }
          if (x.places.length > 0) {
            x.places.forEach(i => i.type = SearchResultType.Place);
            items.push(...x.places);
          }
          if (x.tags.length > 0) {
            x.tags.forEach(i => i.type = SearchResultType.Tag);
            items.push(...x.tags);
          }
          if (x.sales.length > 0) {
            x.sales.forEach(i => i.type = SearchResultType.Sale);
            items.push(...x.sales);
          }
          return items;
        })
      );

    this.tagService
      .getTagsInputResult()
      .subscribe(value => this.inputTags = value.map(this.tagMap));
  }

  private tagMap(tag: Tag): any {
    return {
      display: tag.name,
      value: tag.id,
      readonly: false
    };
  }


  openLk() {
    if (!this.isLogin)
      this.dialog.open(AuthWindowComponent, {
        data: {
          activeTab: 'login'
        },
        panelClass: "user-lk-window"
      });
    else
      this.drawer.toggle()
  }

  ngOnInit(): void {
    // this.input.dropdown = this.dropdown;
  }

  onTextChange(text) {
    this.tagService.searchTags(text);
  }

  itemClick(item: SearchResult) {
    switch (item.type) {
      case SearchResultType.Tag: {
        this.inputTags.push({
          display: item.name,
          value: item.id
        });
        break;
      }
      case SearchResultType.Category: {
        const tempId = this.activeTemplateService.getTemplate(item.id).id;
        this.router
          .navigate(['temps', tempId, 'categories', item.id])
          .then(this.clearFunc);
        break;
      }
      case SearchResultType.Place: {
        this.router
          .navigate(['result', 'places', item.id])
          .then(this.clearFunc);
        break;
      }
      case SearchResultType.Sale: {
        this.router
          .navigate(['result', 'places', (item as SaleSearchResult).placeId, 'sales'])
          .then(this.clearFunc);
        break;
      }
    }
  }

  onInputFocus(dropdown?: TagInputDropdown) {
    if (dropdown)
      dropdown.show();
    this.isFocus = true;
  }

  onAdd() {
    let t = timer(101);
    t.subscribe(x => {
      this.isFocus = true;
    });
  }

  onRemove() {
    let t = timer(101);
    t.subscribe(x => {
      this.isFocus = true;
    });
  }

  onInputBlur() {
    let t = timer(100);
    t.subscribe(x => {
      this.isFocus = false;
    });
  }

  searchPartners() {
    let tags = this.inputTags.map(value => value.value);
    if (tags.length == 0) {
      this.snackService.show('Выберите тэги.');
      return;
    }
    this.router.navigate(['/search'], {queryParams: {tags: tags}})
  }

  clearFunc = () => {
    this.dropdown.hide();
    this.input.inputText = '';
  };

  matchingFn = (value: string, target: any): boolean => {
    return true;
  };

  onAdding = (tag: any): Observable<TagModel> => {
    return of(tag).pipe(filter(t => t.isTag));
  }
}
