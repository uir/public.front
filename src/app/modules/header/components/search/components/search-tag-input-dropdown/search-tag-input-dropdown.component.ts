import {Component, ElementRef, EventEmitter, Injector, Input, OnInit, Output} from '@angular/core';
import {TagInputComponent, TagInputDropdown} from "ngx-chips";
import {
  PlaceSearchResult,
  SaleSearchResult,
  SearchResult,
  SearchResultType
} from "../../../../../../models/search.model";
import {Observable} from "rxjs";
import {switchMap} from "rxjs/operators";
import {TagModel} from "ngx-chips/core/accessor";

@Component({
  selector: 'app-search-tag-input-dropdown',
  templateUrl: './search-tag-input-dropdown.component.html',
  styleUrls: ['./search-tag-input-dropdown.component.css']
})
// @ts-ignore
export class SearchTagInputDropdownComponent extends TagInputDropdown {

  @Input() tagInput: TagInputComponent;
  @Input() items$: Observable<any>;

  @Output() itemClick: EventEmitter<SearchResult> = new EventEmitter<SearchResult>();

  constructor(private injector: Injector) {
    super(injector);
  }

  public get categoriesItems() {
    return this.items.filter((x: SearchResult) => x.type == SearchResultType.Category);
  }

  public get placesItems() {
    return this.items.filter((x: PlaceSearchResult) => x.type == SearchResultType.Place);
  }

  public get tagsItems() {
    return this.items.filter((x: SearchResult) => x.type == SearchResultType.Tag);
  }

  public get salesItems() {
    return this.items.filter((x: SaleSearchResult) => x.type == SearchResultType.Sale);
  }


  public click(item: SearchResult) {
    this.itemClick.emit(item);
  }

  private getMatchingItems(value: string): TagModel[] {
    return this.items;
  }

  ngOnInit(): void {
    this.tagInput.dropdown = this.injector.get(SearchTagInputDropdownComponent) as any;
    this.tagInput
      .onTextChange
      .asObservable()
      .pipe(
        switchMap(() => this.items$),
      )
      .subscribe(x => {
        this.items = x;
        return this.show();
      });
  }
}
