import { Component, OnInit } from '@angular/core';
import {MatDialog} from "@angular/material";
import {ContactsWindowComponent} from "../../../windows/contacts-window/contacts-window.component";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  loadContacts() {
    this.dialog.open(ContactsWindowComponent);
  }

}
