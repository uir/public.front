import {Component, OnInit} from '@angular/core';
import {ObservableMedia} from "@angular/flex-layout";
import {NavigationService} from "../../../../services/navigation.service";

@Component({
  selector: 'app-side-nav-places',
  styleUrls: ['side-nav-places.component.css'],
  template: `
    <div fxLayout="row" [ngStyle]="media.isActive('xs') && {'height':'100%'}">
      <div [ngClass]="{'side-nav-desktop' : !media.isActive('xs'), 'side-nav--mobile' : media.isActive('xs')}" class="side-nav with-shadow" elem="place-list">
        <app-loader [keyWords]="['category']"></app-loader>
        <app-places></app-places>
      </div>
      <router-outlet></router-outlet>
    </div>
  `,
})
export class SideNavPlacesComponent{

  constructor(public media: ObservableMedia,
              private navigationService: NavigationService) {
  }
}
