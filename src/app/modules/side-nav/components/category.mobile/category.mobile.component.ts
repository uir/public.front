import {Component, Input, OnInit} from '@angular/core';
import {MatChipCategory} from "../../services/category.map.mobile.service";
import {MapService} from "../../../map/map.service";
import {Router} from "@angular/router";

@Component({
  selector: 'category-mobile',
  templateUrl: './category.mobile.component.html',
  styleUrls: ['./category.mobile.component.css']
})
export class CategoryMobileComponent implements OnInit {

  @Input()public category: MatChipCategory;

  constructor(private mapService: MapService,public router: Router){
  }

  openMenu(e){
    this.router.navigate([this.category.url])
  }

  close(){
    this.category = null;
    this.mapService.clear()
  }

  ngOnInit(): void {
  }
}
