import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";
import {switchMap, tap} from "rxjs/operators";
import {ActiveTemplateService} from "../../services/active-template.service";
import {CategoriesList} from "../../../../models/categories-list.model";

@Component({
  selector: 'app-categories',
  styleUrls: ['categories.component.css'],
  templateUrl: './categories.component.html'
})
export class CategoriesComponent implements OnInit {

  public data: CategoriesList;
  public title: string;

  constructor(private categoriesService: CategoryService,
              private activeTemplate: ActiveTemplateService,
              private route: ActivatedRoute,
              protected router: Router) {
  }

  ngOnInit() {
    this.route.params
      .pipe(
        tap(x => this.activeTemplate.setActive(+x.tempId)),
        switchMap(x => this.categoriesService.getCategoriesList(x.tempId))
      )
      .subscribe(value => {
        this.data = value;
      })
  }
}

