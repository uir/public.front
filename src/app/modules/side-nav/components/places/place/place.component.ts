import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';
import {Place} from "../../../../../models/place.model";
import {MapService} from "../../../../map/map.service";
import {ObservableMedia} from "@angular/flex-layout";
import {Router} from "@angular/router";

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.css']
})
export class PlaceComponent {

  @Input() place: Place;
  @Output() select: EventEmitter<Place> = new EventEmitter();
  @Output() bookmarkClick: EventEmitter<Place> = new EventEmitter();

  constructor(
    public media: ObservableMedia,
    private mapService: MapService,
    public cd: ChangeDetectorRef,
    private router: Router) {
  }

  click(p: Place) {
    this.select.emit(p)
  }

  zoom(p: Place) {
    this.mapService.focusPlace(p.id);
  }

  navigatePlace(place){
    this.router.navigate([`result/places/${place.id}`])
  }

}
