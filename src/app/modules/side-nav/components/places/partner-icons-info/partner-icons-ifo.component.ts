import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Place} from "../../../../../models/place.model";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'partner-icons-info',
  templateUrl: './partner-icons-ifo.component.html',
  styleUrls: ['./partner-icons-ifo.component.css']
})
export class PartnerIconsIfoComponent implements OnInit {

  @Input() place: Place;

  public isHint: boolean;
  public nameHint: string;


  constructor(public media: ObservableMedia,
              private route: ActivatedRoute,
              protected router: Router) {
  }

  ngOnInit() {
  }

  show(icon){
    this.isHint = true;
    this.nameHint = icon
  }

  close(){
    this.isHint = false;
    this.nameHint = null
  }

  mobileNavigate(e,tab){
    e.stopPropagation()
    e.preventDefault()
    this.router.navigate(['result/places/' + this.place.id], { queryParams: { tab: tab } });
  }

}
