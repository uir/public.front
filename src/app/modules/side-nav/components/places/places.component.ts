import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef, Inject,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import {ObservableMedia} from "@angular/flex-layout";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {CategoryService} from "../../services/category.service";
import {Place} from "../../../../models/place.model";
import {BookmarkService} from "../../../../services/bookmark.service";
import {PlacesComponentData} from "../../models/place-component-data.model";
import {SideNavService} from "../../services/side-nav.service";
import {WindowService} from "../../../../services/window.service";
import {AuthorizeService} from "../../../auth/services/authorize.service";
import {MapService} from "../../../map/map.service";
import {SearchService} from "../../../header/services/search.service";
import {PageEvent} from "@angular/material";
import {ActiveTemplateService} from "../../services/active-template.service";
import {CategoryMapMobileService} from "../../services/category.map.mobile.service";
import {BottomBarService, BottomBarType} from "../../../../services/bottom.bar.service";
import {ScrollStoreProvider} from "../../../../services/scroll.store.provider";
import {DOCUMENT} from "@angular/common";
import {pairwise} from "rxjs/operators";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-places',
  templateUrl: 'places.component.html',
  styleUrls: ['places.component.css']
})
export class PlacesComponent implements OnInit, AfterViewInit{
  public data: PlacesComponentData;
  public selectPartnerId: number;
  public sortings = [
    {value: 'name', viewValue: 'Наименование', asc: true},
    {value: 'salesCount', viewValue: 'Скидки', asc: true},
    {value: 'overviewsCount', viewValue: 'Обзоры', asc: true},
    {value: 'videosCount', viewValue: 'Видео', asc: true},
    {value: 'imagesCount', viewValue: 'Изображения', asc: true}
  ];
  public selectSorting = null;
  public selectPage = 1;
  public placesCount = 0;
  scrollStoreProvider: ScrollStoreProvider;
  @ViewChild('placesContainer') placesContainer: ElementRef;

  constructor(public media: ObservableMedia,
              private categoryService: CategoryService,
              private mapService: MapService,
              private tagService: SearchService,
              public route: ActivatedRoute,
              private sideNavService: SideNavService,
              private bookmarkService: BookmarkService,
              private windowService: WindowService,
              private authService: AuthorizeService,
              public cd: ChangeDetectorRef,
              private activeTemplateService: ActiveTemplateService,
              private router: Router,
              private categoryMapMobileService: CategoryMapMobileService,
              public bottomBarService: BottomBarService) {
  }

  public back(){
    this.router.navigate(['/'])
  }

  ngOnInit() {
    this.bottomBarService.setType(BottomBarType.category);
    this.sideNavService
      .getPlaces(this.route)
      .subscribe(x => {
        this.selectSorting = null;
        this.selectPage = 1;
        this.placesCount = x.placesCount;
        this.data = x;
        this.cd.detectChanges();
        // if (this.media.isActive('xs'))
        //   this.setCategoryMobile()
      });

    this.bookmarkService
      .getBookmarks()
      .subscribe(() => {
        this.cd.markForCheck();
      });
  }

  setCategoryMobile(){
    if(this.route.routeConfig.path == 'bookmarks')
    {
      this.categoryMapMobileService.set({
        color: 'yellow',
        icon: 'star',
        name: 'Закладки',
        url: this.router.url
      })
    }
    else if(this.route.routeConfig.path == 'search')
    {
      this.categoryMapMobileService.set({
        color: '#00838f',
        icon: 'search',
        name: 'Поиск',
        url: this.router.url
      })
    }
    else {
      this.route.params.subscribe(x => {
        let a = this.activeTemplateService.getTemplate(x['id']);
        this.categoryMapMobileService.set({
          color: a.color,
          icon: a.icon,
          name: this.getTitle(),
          url: this.router.url
        })
      })
    }
  }

  close() {
    this.mapService.clear();
    this.tagService.clearInputTags();
    this.activeTemplateService.setActive(0);
    this.router.navigate([""]);
  }

  goMap() {
    this.router.navigate(['/']);
  }

  click(p: Place) {
    this.selectPartnerId = p.id;
  }

  getTitle(){
    if(this.route.routeConfig.path == 'bookmarks')
      return "Закладки";
    if(this.route.routeConfig.path == 'search')
      return "Поиск";
    return this.data ? this.data.categoryName : ""
  }

  getBackLink(){
    return this.route.routeConfig.path == 'bookmarks' || this.route.routeConfig.path == 'search' ?
       '../' : '../..'
  }

  clickSort(e) {
    this.updatePlaces(e, 1);
  }

  onPaged(page: PageEvent) {
    (this.placesContainer.nativeElement as HTMLElement).scrollTo(0,0)
    this.updatePlaces(null, page.pageIndex + 1)
  }

  private updatePlaces(sort = null, page) {
    this.selectPage = page;
    let sub = this.sideNavService
      .getPlaces(this.route, {order: this.getSorting(sort), page: page})
      .subscribe(value => {
        this.data = value;
        sub.unsubscribe()
      });
  }

  private getSorting(sort = null): string {
    if (sort)
      this.selectSorting = sort;
    else
      sort = this.selectSorting;

    return sortBuilder(sort);

    function sortBuilder(s) {
      if (!s)
        return s;
      return s.value + (s.value == 'name' ? '.Asc' : s.asc ? '.Asc' : '.Desc');
    }
  }

  ngAfterViewInit(): void {
    this.scrollStoreProvider = new ScrollStoreProvider({
      compContext: this,
      router: this.router,
    });
    this.scrollStoreProvider
      .handleScroll("key",this.placesContainer.nativeElement);
  }
}
