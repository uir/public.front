import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'iconColor'
})
export class IconColorPipe implements PipeTransform {

  private colorPrefix = "color";
  private colorHue = "800";

  transform(value: string, prefix: string): string {
    return `${prefix ? prefix : this.colorPrefix}-${value}-${this.colorHue}`;
  }

}
