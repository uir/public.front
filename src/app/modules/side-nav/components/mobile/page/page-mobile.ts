import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {CategoryService} from "../../../services/category.service";
import {ActiveTemplateService} from "../../../services/active-template.service";
import {CategoriesList} from "../../../../../models/categories-list.model";

@Component({
  selector: 'page-mobile',
  styleUrls: ['page-mobile.css'],
  templateUrl: './page-mobile.html'
})
export class PageMobile{

  public data: CategoriesList;
  public isLogin: boolean;
  @Input() title: string;
  @Input() redirect: string = '/';

  constructor(private categoriesService: CategoryService,
              private activeTemplate: ActiveTemplateService,
              private route: ActivatedRoute,
              protected router: Router) {
  }


  close() {
    this.router.navigate([this.redirect]);
  }

}
