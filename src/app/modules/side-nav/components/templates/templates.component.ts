import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import {CategoryService} from "../../services/category.service";
import {Router} from "@angular/router";
import {ActiveTemplateService} from "../../services/active-template.service";
import {delay, tap} from "rxjs/operators";
import {Template} from "../../../../models/template.model";
import {MatMenuTrigger} from "@angular/material";
import {ObservableMedia} from "@angular/flex-layout";

declare var $: any;

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-templates',
  styleUrls: ['templates.component.css'],
  templateUrl: './templates.component.html'
})
export class TemplatesComponent implements OnInit {
  public templates: Template[];
  public activeId: number;
  @Input() drawer: any;

  @ViewChildren(MatMenuTrigger) menus: QueryList<MatMenuTrigger>;

  constructor(
    private cd: ChangeDetectorRef,
    private categoryService: CategoryService,
    private router: Router,
    private activeTemplate: ActiveTemplateService,
    public media: ObservableMedia) {
    this.categoryService
      .getTemplates()
      .pipe(
        tap(x => {
          this.activeTemplate.setTemplates(x);
          this.templates = x;
          this.cd.detectChanges();
        }),
        delay(1000)
      )
      .subscribe(() => this.initOwl());

    this.activeTemplate
      .getActive()
      .subscribe(x => {
        this.activeId = x;
        this.cd.detectChanges();
      });
  }

  private initOwl() {
    const owl = $('.owl-carousel');
    owl.owlCarousel({
      items: 4,
      loop: false,
      dots: false,
      nav: true,
      rewind: false,
      autoWidth: true,
      checkVisible: false,
      mouseDrag: false,
      touchDrag: false,
      navText: ["<i class=\"material-icons\">\n" +
      "keyboard_arrow_left\n" +
      "</i>", "<i class=\"material-icons\">\n" +
      "keyboard_arrow_right\n" +
      "</i>"]
    });
    $('.owl-prev').click(() => {
      this.menus.forEach(tr => tr.closeMenu());
    });
    $('.owl-next').click(() => {
      this.menus.forEach(tr => tr.closeMenu());
    });
  }

  select(tempId: number, categoryId: number) {
    this.categoryService.templateId = tempId;
    this.router.navigate(["temps", tempId, "categories", categoryId])
      .then(

      );
  }

  openMenu(menu) {
    this.menus.filter(tr => tr.menu !== menu).forEach(tr => tr.closeMenu());
  }

  ngOnInit() {
  }
}
