import { Injectable } from '@angular/core';

export class MatChipCategory {
  name: string;
  icon: string;
  color: string;
  url: string
}

@Injectable()
export class CategoryMapMobileService {

  public category: MatChipCategory;

  set(category: MatChipCategory){
    this.category = category;
  }

  get(){
    return this.category;
  }
}
