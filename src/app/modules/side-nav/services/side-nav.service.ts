import {ActivatedRoute} from "@angular/router";
import {Injectable} from "@angular/core";
import {PlacesComponentData} from "../models/place-component-data.model";
import {CategoryService} from "./category.service";
import {Observable} from "rxjs";
import {filter, map, switchMap, tap} from "rxjs/operators";
import {SearchService} from "../../header/services/search.service";
import {PlacesList} from "../../../models/place.model";
import {PlacesService} from "./places.service";
import {ActiveTemplateService} from "./active-template.service";
import {PlacesQueryOptions} from "../models/place-provider";

@Injectable()
export class SideNavService {

  constructor(
    private placesService: PlacesService,
    private categoriesService: CategoryService,
    private activeTemplate: ActiveTemplateService,
    private tagService: SearchService) {
  }

  getPlaces(route: ActivatedRoute, opts: PlacesQueryOptions = {page: 1}): Observable<PlacesComponentData> {
    let path = route.routeConfig.path;
    switch (path) {
      case "search": {
        return this.getSearchData(route, opts);
      }
      case "bookmarks": {
        return this.getBookmarksData(opts);
      }
      default: {
        return this.getPlacesData(route, opts);
      }
    }
  }

  private getBookmarksData(opts: PlacesQueryOptions): Observable<PlacesComponentData> {
    return this.placesService
      .loadPlaces(this.categoriesService.fetchPlacesBookmarks.bind(this.categoriesService), null, opts)
      .pipe(
        map(x => {
          return {
            places: x.places,
            header: "Закладки",
            placesCount: x.placesCount
          }
        })
      );
  }

  private getSearchData(route: ActivatedRoute, opts: PlacesQueryOptions): Observable<PlacesComponentData> {
    return route.queryParams
      .pipe(
        filter(value => value.tags != null),
        map(value => {
          if (!(value.tags instanceof Array))
            return [value.tags];
          else
            return value.tags;
        }),
        tap(x => this.tagService.searchTagsByIds(x)),
        switchMap(value =>
          this.placesService.loadPlaces(this.categoriesService.searchPlaces.bind(this.categoriesService), value, opts)
        ),
        map(x => {
          return {
            places: x.places,
            header: "Результат поиска",
            placesCount: x.placesCount
          }
        })
      )
  }

  private getPlacesData(route: ActivatedRoute, opts: PlacesQueryOptions): Observable<PlacesComponentData> {
    return route.params
      .pipe(
        tap(x => this.activeTemplate.setActive(+x.tempId)),
        map(value => value.id),
        switchMap(value =>
          this.placesService.loadPlaces(this.categoriesService.getPlacesList.bind(this.categoriesService), value, opts)
        ),
        map((x: PlacesList) => {
          return {
            places: x.places,
            header: "Организации",
            categoryName: x.categoryName,
            placesCount: x.placesCount
          }
        })
      )
  }
}
