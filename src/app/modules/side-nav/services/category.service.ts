import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {PlacesAndMarkers, PlacesList} from "../../../models/place.model";
import {HttpParams} from "@angular/common/http";
import {HttpService} from "../../../services/http.service";
import {environment} from "../../../../environments/environment";
import {Category} from "../../../models/category.model";
import {Template} from "../../../models/template.model";
import {PlacesQueryOptions} from "../models/place-provider";
import {CategoriesList} from "../../../models/categories-list.model";

@Injectable()
export class CategoryService extends HttpService {
  private _templatesUrl = environment.api + 'api/templates/categories';
  private _placesListApi = environment.api + 'api/category/$categoryId/places';
  private _placesBookmarkApi: string = environment.api + 'api/user/places/bookmarks';
  private _placesByTagsApi = environment.api + 'api/tag/places';
  public templateId: number = null;
  public category: Category = null;
  private _categoriesListApi = environment.api + 'api/template/$templateId/categories';

  getCategoriesList(templateId): Observable<CategoriesList> {
    let url = this._categoriesListApi.replace('$templateId', templateId);
    return this.GET<CategoriesList>(url);
  }

  getPlacesList(categoryId, opts: PlacesQueryOptions): Observable<PlacesList> {
    let url = this._placesListApi.replace('$categoryId', categoryId);
    let params = this.getPagingParams(opts);
    return this.GET<PlacesList>(url, params);
  }

  getTemplates(): Observable<Template[]> {
    return this.GET<Template[]>(this._templatesUrl);
  }

  searchPlaces(tagsIds: number[], opts: PlacesQueryOptions): Observable<PlacesAndMarkers> {
    if (tagsIds == null)
      return of(null);
    let params: HttpParams = new HttpParams();
    params = params.set('ids', tagsIds.map(n => "" + n).reduce((total, current) => total + ',' + current));
    params = this.getPagingParams(opts, params);
    return this.GET<PlacesAndMarkers>(this._placesByTagsApi, params)
  }

  fetchPlacesBookmarks(v, opts: PlacesQueryOptions): Observable<PlacesAndMarkers> {
    let params = this.getPagingParams(opts);
    return this.GET<PlacesAndMarkers>(this._placesBookmarkApi, params)
  }

  private getPagingParams(opts: PlacesQueryOptions, params: HttpParams = null): HttpParams {
    if (!params)
      params = new HttpParams();
    if (opts.order)
      params = params.set('orderBy', opts.order);

    params = params.set('withMarkers', (opts.page === 1) + "");
    params = params.set('page', opts.page + "");
    return params;
  }
}
