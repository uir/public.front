import {Injectable} from '@angular/core';
import {CategoryService} from "./category.service";
import {MapService} from "../../map/map.service";
import {BookmarkService} from "../../../services/bookmark.service";
import {BehaviorSubject, Observable} from "rxjs";
import {Place, PlacesAndMarkers, PlacesList} from "../../../models/place.model";
import {finalize, tap} from "rxjs/operators";
import {PlacesAndMarkersProvider, PlacesQueryOptions} from "../models/place-provider";

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  public places: BehaviorSubject<Place[]> = new BehaviorSubject<Place[]>([]);

  constructor(
    private categoriesService: CategoryService,
    private mapService: MapService,
    private bookmarkService: BookmarkService
  ) {
  }

  loadPlaces(placesProvider: PlacesAndMarkersProvider, v = null, opts: PlacesQueryOptions): Observable<PlacesAndMarkers | PlacesList> {
    return placesProvider(v, opts)
      .pipe(
        tap(x => {
          this.bookmarkService.setBookmarks(x.places.filter(p => p.isBookmark).map(p => p.id));
          if (opts.page === 1)
            this.mapService.setMarkers(x.markers);
          this.places.next(x.places);
        }),
        finalize(() => this.mapService.resetFocusPlace())
      )
  }
}
