import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {filter} from "rxjs/operators";
import {Template} from "../../../models/template.model";

@Injectable()
export class ActiveTemplateService {

  private _active: BehaviorSubject<number> = new BehaviorSubject<number>(null);
  private _templatses: Template[] = [];

  constructor() {
  }

  getActive(): Observable<number> {
    return this._active
      .pipe(
        filter(x => x != null)
      )
  }

  setActive(id: number) {
    this._active.next(id);
  }

  setTemplates(templates: Template[]) {
    this._templatses = templates;
  }

  getTemplate(categoryId: number): Template {
    return this._templatses.find(t => t.categories.some(c => c.id == categoryId));
  }
}
