import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {TemplatesComponent} from "./components/templates/templates.component";
import {PlacesComponent} from "./components/places/places.component";
import {
  MatButtonModule,
  MatChipsModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule
} from "@angular/material";
import {CategoryService} from "./services/category.service";
import {FlexLayoutModule} from "@angular/flex-layout";
import {RouterModule} from "@angular/router";
import {AppRoutes} from "../../app.routing";
import {TagListModule} from "../common/tags-list/tag-list.module";
import {LoaderModule} from "../common/loader/loader.module";
import {HttpClientModule} from "@angular/common/http";
import {PartnerIconsIfoComponent} from "./components/places/partner-icons-info/partner-icons-ifo.component";
import {PlaceComponent} from "./components/places/place/place.component";
import {SideNavPlacesComponent} from "./components/side-nav/side-nav-places.component";
import {SideNavService} from "./services/side-nav.service";
import {IconColorPipe} from './components/icon-color.pipe';
import {ActiveTemplateService} from "./services/active-template.service";
import {BemModule} from "angular-bem";
import {SideNavTemplatesMobile} from "./components/mobile/main-bar/main-bar-mobile.component";
import {HeaderModule} from "../header/header.module";
import {CategoriesComponent} from "./components/categories/categories.component";
import {MatSidenavModule} from "@angular/material/sidenav";
import {UserLkModule} from "../user-lk/user-lk.module";
import {PageMobile} from "./components/mobile/page/page-mobile";
import {BookmarkControlModule} from "../common/bookmark-control/bookmark-control.module";
import {CategoryMobileComponent} from "./components/category.mobile/category.mobile.component";

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    RouterModule.forRoot(AppRoutes),
    FlexLayoutModule,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatProgressSpinnerModule,
    TagListModule,
    HttpClientModule,
    MatChipsModule,
    MatMenuModule,
    LoaderModule,
    MatPaginatorModule,
    BemModule,
    HeaderModule,
    MatSidenavModule,
    UserLkModule,
    BookmarkControlModule
  ],
  declarations: [
    TemplatesComponent,
    PlacesComponent,
    PlaceComponent,
    PartnerIconsIfoComponent,
    SideNavPlacesComponent,
    IconColorPipe,
    SideNavTemplatesMobile,
    CategoriesComponent,
    PageMobile,
    CategoryMobileComponent
  ],
  exports: [
    TemplatesComponent,SideNavTemplatesMobile,CategoryMobileComponent
  ],
  providers: [
    SideNavService,
    CategoryService,
    ActiveTemplateService
  ],
  entryComponents: [SideNavPlacesComponent,PageMobile]
})
export class CategoriesMenuModule {
}
