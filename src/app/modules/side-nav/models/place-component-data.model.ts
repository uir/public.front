import {Place} from "../../../models/place.model";

export class PlacesComponentData {
  placesCount: number;
  places: Place[];
  header: string;
  categoryName?: string;
}
