import {Observable} from "rxjs";
import {PlacesAndMarkers} from "../../../models/place.model";

export declare type PlacesAndMarkersProvider = (v, o: PlacesQueryOptions) => Observable<PlacesAndMarkers>

export interface PlacesQueryOptions {
  order?: string;
  page: number;
  withMarkers?: boolean;
  take?: number;
}
