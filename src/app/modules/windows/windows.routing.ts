import {Routes} from "@angular/router";
import {WindowRoutingComponent} from "./window-routing.component";

export const AppWindowsRoutes: Routes = [
  {
    path: 'docs',
    component: WindowRoutingComponent
  },
  {
    path: 'reg/:ref',
    component: WindowRoutingComponent
  },
  {
    path: 'contacts',
    component: WindowRoutingComponent
  },
  {
    path: 'auth',
    component: WindowRoutingComponent
  },
  {
    path: 'pay',
    component: WindowRoutingComponent
  },
  {
    path: 'payment/success',
    component: WindowRoutingComponent
  },
  {
    path: 'payment/failure',
    component: WindowRoutingComponent
  },
];
