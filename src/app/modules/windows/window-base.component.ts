import {Inject, OnInit, Optional} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Router} from "@angular/router";

export abstract class WindowBaseComponent<T> implements OnInit {
  protected useAllUrl = false;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) private dialogData: any,
    public router: Router,
    public dr: MatDialogRef<T>) {
    this.dialogDataResolver(this.dialogData);
    if (this.url !== null)
      this.dr
        .afterClosed()
        .subscribe(x => {
          this.afterClosedResolver();
        });
  }

  protected get url(): string {
    return null
  };

  protected get nav(): string[] {
    return [".."]
  };

  protected dialogDataResolver(dialogData: any) {
  }

  protected afterClosedResolver() {
    const segments = this.router.url.split('/');
    if (segments[segments.length - 1].includes(this.url) && !this.useAllUrl)
      this.router.navigate(this.nav);
    if (this.router.url.includes(this.url) && this.useAllUrl)
      this.router.navigate(this.nav);
  }

  ngOnInit(): void {
  }
}
