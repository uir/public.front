import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MatDialogModule, MatIconModule, MatListModule} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";
import {SaleWindowComponent} from "./sales-window.component";

@NgModule({
  declarations: [
    SaleWindowComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatDialogModule,
    MatListModule
  ],
  bootstrap: [
    SaleWindowComponent
  ],
})
export class SalesWindowModule {
}
