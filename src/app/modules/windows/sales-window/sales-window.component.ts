import {Component, OnInit} from '@angular/core';
import {RecentSale} from "../../../models/sale";

@Component({
  selector: 'sale-window',
  templateUrl: './sale-window.component.html',
  styleUrls: ['./style.css']
})
export class SaleWindowComponent implements OnInit {
  public sales: RecentSale[];

  constructor() {
    this.sales = [];
    // this.store.select(fromRoot.getUser).subscribe(user => {
    //   if (user != null && user.sales != null)
    //     this.sales = user.sales;
    // })
  }

  ngOnInit() {
  }
}
