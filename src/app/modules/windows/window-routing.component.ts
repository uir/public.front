import {ActivatedRoute} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {DocumentWindowComponent} from "./document-window/document-window.component";
import {ContactsWindowComponent} from "./contacts-window/contacts-window.component";
import {WindowService} from "../../services/window.service";
import {PayWindowComponent} from "./pay-window/pay-window/pay-window.component";
import {AuthWindowComponent} from "../auth/components/auth-window/authwindow.component";
import {RegForm} from "../auth/forms/reg.form";
import {PaymentWindowComponent} from "./payment-window/pay-window/payment-window.component";
import {MatDialog} from "@angular/material";
import {ReferralService} from "../referral/services/referral.service";
import {filter, map, switchMap, tap} from "rxjs/operators";

@Component({
  selector: 'route',
  template: ``
})
export class WindowRoutingComponent implements OnInit {
  constructor(private window: WindowService,
              private regForm: RegForm,
              private refService: ReferralService,
              private dialog: MatDialog,
              private route: ActivatedRoute) {
    let path = route.snapshot.routeConfig.path;
    switch (path) {
      case "docs": {
        this.window.show(DocumentWindowComponent);
        break;
      }
      case "reg/:ref" : {
        this.route.paramMap
          .pipe(
          map(value => value.get("ref")),
          filter(value => value != null),
          tap(x => this.regForm.buildForm()),
          switchMap(value => this.refService.loadRef(value)),
          )
          .subscribe(value => {
            this.regForm.setFormData({
              name: value.name,
              parentId: value.userId
            });
            this.window.showReg();
          });
        break;
      }
      case "contacts": {
        this.window.show(ContactsWindowComponent);
        break;
      }
      case "auth": {
        this.window.showAuth();
        break;
      }
      case "pay": {
        this.window.show(PayWindowComponent);
        break;
      }
      case "payment/success": {
        this.dialog.open(PaymentWindowComponent, {
          panelClass: "payment-window",
          data: {
            isSuccess: true
          }
        });
        break;
      }
      case "payment/failure": {
        this.dialog.open(PaymentWindowComponent, {
          panelClass: "payment-window",
          data: {
            isSuccess: false
          }
        });
        break;
      }
    }
  }

  ngOnInit() {
  }
}
