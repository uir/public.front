import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material";

@Component({
  selector: 'app-user-lk-window',
  templateUrl: './user-lk-window.component.html',
  styleUrls: ['./user-lk-window.component.css']
})
export class UserLkWindowComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<UserLkWindowComponent>) {
    this.dialogRef.updateSize("800px", "500px")
  }

  ngOnInit() {
  }

}
