import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {UserLkWindowComponent} from "./user-lk-window.component";
import {UserLkModule} from "../../user-lk/user-lk.module";
import {CloseButtonModule} from "../../common/close-buttom/close-button.module";

@NgModule({
  declarations: [
    UserLkWindowComponent
  ],
  imports: [
    CommonModule,
    CloseButtonModule,
    UserLkModule
  ],
  bootstrap: [
    UserLkWindowComponent
  ],
})
export class UserLkWindowModule {
}
