import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ContactsWindowModule} from "./contacts-window/contacts-window.module";
import {DocumentsWindowModule} from "./document-window/document-window.module";
import {PaymentWindowModule} from "./payment-window/payment-window.module";
import {SalesWindowModule} from "./sales-window/sales-window.module";
import {UserLkWindowModule} from "./user-lk-window/user-lk-window.module";
import {PayWindowModule} from "./pay-window/pay-window.module";
import {RouterModule} from "@angular/router";
import {AppWindowsRoutes} from "./windows.routing";
import {WindowRoutingComponent} from "./window-routing.component";
import {MatButtonModule} from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    ContactsWindowModule,
    DocumentsWindowModule,
    PaymentWindowModule,
    SalesWindowModule,
    UserLkWindowModule,
    PayWindowModule,
    MatButtonModule,
    RouterModule.forChild(AppWindowsRoutes)
  ],
  declarations:[
    WindowRoutingComponent,
  ],
})
export class WindowsModule {
}
