import { Injectable } from '@angular/core';
import {CreatePayment} from "./models/create-payment";
import {environment} from "../../../../environments/environment";
import {HttpService} from "../../../services/http.service";
import {Observable} from "rxjs/internal/Observable";

@Injectable()
export class PaymentService {

  private _createPaymentApi: string = environment.api + "api/payment";

  constructor(private http: HttpService) { }

  createPayment(model: CreatePayment): Observable<number>{
    return this.http.POST<number>(this._createPaymentApi, model);
  }
}
