import {Component, Inject, Optional} from '@angular/core';
import {PaymentService} from "../payment.service";
import {WindowService} from "../../../../services/window.service";
import {AuthorizeService} from "../../../auth/services/authorize.service";
import {Tariff} from "../../../user-lk/model/tariff";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {switchMap} from "rxjs/operators";
import {WindowBaseComponent} from "../../window-base.component";
import {Router} from "@angular/router";
import {ObservableMedia} from "@angular/flex-layout";


export class DataPay {
  tariff: Tariff;
  paymentId: number
}

@Component({
  selector: 'app-pay-window',
  templateUrl: './pay-window.component.html',
  styleUrls: ['./pay-window.component.css']
})
export class PayWindowComponent extends WindowBaseComponent<PayWindowComponent> {

  public paymentId: number;
  public data;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) dialogData: any,
    router: Router,
    dr: MatDialogRef<PayWindowComponent>,
    private paymentService: PaymentService,
    private windowService: WindowService,
    private authService: AuthorizeService,
    public media: ObservableMedia) {
    super(dialogData, router, dr);
  }

  protected dialogDataResolver(dialogData: any) {
    this.data = dialogData;
  }

  protected get url(): string {
    return "pay";
  }

  pay(form, planId: number) {
    this.authService
      .isAuthorize()
      .pipe(
        switchMap(value => this.paymentService
          .createPayment({
            paymentPlanId: planId
          }))
      )
      .subscribe(value => {
        this.paymentId = value;
        setTimeout(() => {
          form.submit()
        }, 200);
      });
  }
}
