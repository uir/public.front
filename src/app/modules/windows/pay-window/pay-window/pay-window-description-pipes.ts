import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'payDescription'})

export class PayWindowDescriptionPipe implements PipeTransform {
  transform(price: number): string {
    if(price==500) return "";
    else return `и доступ к ${price == 2000 ? '3' : '5'} линиям партнерской программы`
  }
}
