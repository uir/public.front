import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PayWindowComponent} from './pay-window/pay-window.component';
import {MatButtonModule, MatCardModule, MatDialogModule, MatIconModule} from "@angular/material";
import {PaymentService} from "./payment.service";
import {PayWindowDescriptionPipe} from "./pay-window/pay-window-description-pipes";
import {DialogMobileModule} from "../../common/dialog-mobile/dialog-mobile.module";

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    DialogMobileModule
  ],
  declarations: [
    PayWindowComponent,PayWindowDescriptionPipe
  ],
  providers: [
    PaymentService,

  ],
  bootstrap: [
    PayWindowComponent
  ],
})
export class PayWindowModule {
}
