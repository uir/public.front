import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ContactsWindowComponent} from "./contacts-window.component";
import {MatDialogModule, MatIconModule} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";
import {FeedbackModule} from "../../common/feedback/feedback.module";

@NgModule({
  declarations: [
    ContactsWindowComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatIconModule,
    MatDialogModule,
    FeedbackModule
  ],
  bootstrap: [
    ContactsWindowComponent
  ]
})
export class ContactsWindowModule {
}
