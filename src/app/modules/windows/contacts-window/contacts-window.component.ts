import {Component, Inject, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {WindowBaseComponent} from "../window-base.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-contacts-window',
  templateUrl: './contacts-window.component.html',
  styleUrls: ['./contacts-window.component.css']
})
export class ContactsWindowComponent extends WindowBaseComponent<ContactsWindowComponent> {

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) dialogData: any,
    router: Router,
    dr: MatDialogRef<ContactsWindowComponent>,
    private dialog: MatDialog) {
    super(dialogData, router, dr);
  }

  onSubmit() {
    this.dialog.closeAll();
  }

  protected get url(): string {
    return "contacts";
  }
}
