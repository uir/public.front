import { Component} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {WindowBaseComponent} from "../window-base.component";

@Component({
  selector: 'app-document-window',
  templateUrl: './document-window.component.html',
  styleUrls: ['./document-window.component.css']
})
export class DocumentWindowComponent  extends WindowBaseComponent<DocumentWindowComponent> {

  public serverUrl = environment.cloud;

  protected get url(): string {
    return "docs";
  }
}
