import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MatDialogModule} from "@angular/material";
import {DocumentWindowComponent} from "./document-window.component";

@NgModule({
  declarations: [
    DocumentWindowComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule
  ],
  bootstrap: [
    DocumentWindowComponent
  ]
})
export class DocumentsWindowModule {
}
