import {Component, Inject, Optional} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {WindowBaseComponent} from "../../window-base.component";
import {Router} from "@angular/router";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-pay-window',
  templateUrl: './payment-window.component.html',
  styleUrls: ['./payment-window.component.css', '../../../../../assets/styles/color.css']
})
export class PaymentWindowComponent extends WindowBaseComponent<PaymentWindowComponent> {

  public isSuccess: boolean;

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) dialogData: any,
    router: Router,
    dr: MatDialogRef<PaymentWindowComponent>,public media: ObservableMedia) {
    super(dialogData, router, dr);
    this.useAllUrl = true;
  }

  protected get url(): string {
    return "payment";
  }

  protected dialogDataResolver(dialogData: any) {
    this.isSuccess = dialogData.isSuccess;
  }
}
