import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule, MatCardModule, MatDialogModule, MatIconModule} from "@angular/material";
import {FeedbackModule} from "../../common/feedback/feedback.module";
import {PaymentWindowComponent} from "./pay-window/payment-window.component";
import {CloseButtonModule} from "../../common/close-buttom/close-button.module";
import {DialogMobileModule} from "../../common/dialog-mobile/dialog-mobile.module";

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    FeedbackModule,
    CloseButtonModule,
    DialogMobileModule
  ],
  declarations: [
    PaymentWindowComponent
  ],
  bootstrap: [
    PaymentWindowComponent
  ]
})
export class PaymentWindowModule {
}
