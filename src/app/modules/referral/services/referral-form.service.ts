import {FormBuilder, Validators} from '@angular/forms';
import {FormGroup} from '@angular/forms/forms';
import {Injectable} from '@angular/core';
import {BaseForm} from "../../../forms/base.form";
import {Referal} from "../../../models/referal.model";

@Injectable()
export class ReferralForm extends BaseForm<Referal> {
  protected getForm(): FormGroup {
    return this.fb.group({
      name: [
        this.data.name,
        [
          Validators.required
        ]
      ]
    });
  }

  protected getValidationMessages() {
    return {
      name: {
        required: this.required
      }
    };
  }

  constructor(private fb: FormBuilder) {
    super({
      name: '',
      type: null,
      userId: null
    });
    this.validationMessages = this.getValidationMessages();
    this.form = this.getForm();
  }
}
