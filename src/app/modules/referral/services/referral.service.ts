import {Injectable} from '@angular/core';
import {HttpParams} from "@angular/common/http";
import {HttpService} from "../../../services/http.service";
import {environment} from "../../../../environments/environment";
import {Referal} from "../../../models/referal.model";
import {Observable} from "rxjs/internal/Observable";
import {map} from "rxjs/operators";

@Injectable()
export class ReferralService extends HttpService {
  private _createRef = environment.api + 'api/user/ref';
  private _loadRef = environment.api + 'api/user/ref';

  createRef(data: Referal): Observable<string> {
    return this.POST<string>(this._createRef, data, null)
      .pipe(map(value => environment.domain + 'reg/' + value));
  }

  loadRef(ref: string): Observable<Referal> {
    let params: HttpParams = new HttpParams();
    params = params.set('referal', ref.toLocaleString());

    return this.GET<Referal>(this._loadRef, params);
  }
}
