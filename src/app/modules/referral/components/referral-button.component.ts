import {Component, Input, OnInit} from "@angular/core";
import {MatDialog} from "@angular/material";
import {AuthWindowComponent} from "../../auth/components/auth-window/authwindow.component";
import {AuthorizeService} from "../../auth/services/authorize.service";
import {ReferralService} from "../services/referral.service";
import {ObservableMedia} from "@angular/flex-layout";
import {GeneralReferralComponent} from "./general-referral/general-referral.component";
import {PersonalReferralMobileComponent} from "./personal-referral/personal-referral.component-mobile";
import {GeneralReferralMobileComponent} from "./general-referral/general-referral.component-mobile";

@Component({
  selector: 'app-referral-button',
  templateUrl: 'referral-button.component.html',
  styleUrls: ['referral-button.component.css', 'referral-button.component_mobile.css']
})
export class ReferralButtonComponent implements OnInit {
  public isLogin: boolean;
  public showSubButtons: boolean;
  public activeBlock: string;
  public refBlock = {
    general: "general",
    personal: "personal"
  };

  constructor(
    public media: ObservableMedia,
    private dialog: MatDialog,
    private refService: ReferralService,
    private authService: AuthorizeService) {
    this.showSubButtons = false;
    authService.isAuthorize()
      .subscribe(isLogin => {
        this.isLogin = isLogin;
        this.showSubButtons = isLogin && this.showSubButtons;
      });
  }

  ngOnInit() {
  }

  open() {
    if (!this.isLogin){
      this.dialog.open(AuthWindowComponent, {
        data: {
          activeTab: 'login'
        },
        panelClass: this.media.isActive('xs') ? "user-lk-window" : ""
      });
    }
    this.showSubButtons = !this.showSubButtons && this.isLogin;
  }

  openPersonalReferral() {
    if(!this.media.isActive('xs'))
      this.activeBlock = this.refBlock.personal;
    else {
      this.dialog.open(PersonalReferralMobileComponent, {
        panelClass: this.media.isActive('xs') ? "user-lk-window" : ""
      });
    }
  }

  createGeneralReferral() {
    if(!this.media.isActive('xs'))
      this.activeBlock = this.refBlock.general;
    else {
      this.dialog.open(GeneralReferralMobileComponent, {
        panelClass: this.media.isActive('xs') ? "user-lk-window" : ""
      });
    }
  }
}
