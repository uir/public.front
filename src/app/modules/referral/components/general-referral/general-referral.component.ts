import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ReferralService} from "../../services/referral.service";
import {ReferalType} from "../../../../models/referal.model";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-general-referral',
  templateUrl: 'general-referral.component.html',
  styleUrls: ['general-referral.component.css']
})
export class GeneralReferralComponent implements OnInit {

  public generalRef$;

  @Output() close: EventEmitter<{}> = new EventEmitter<{}>();

  constructor(private refService: ReferralService,public media: ObservableMedia) {
  }

  onClose() {
    this.close.emit();
  }

  ngOnInit() {
    this.generalRef$ = this.refService.createRef({type: ReferalType.General});
  }
}
