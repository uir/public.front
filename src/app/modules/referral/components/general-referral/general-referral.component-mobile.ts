import {Component, Inject, Optional} from "@angular/core";
import {GeneralReferralComponent} from "./general-referral.component";
import {ReferralService} from "../../services/referral.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {LoginForm} from "../../../auth/forms/login.form";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  templateUrl: 'general-referral.component.html',
  styleUrls: ['general-referral.component.css']
})
export class GeneralReferralMobileComponent extends GeneralReferralComponent {

  constructor(
    refService: ReferralService,
    @Optional() @Inject(MAT_DIALOG_DATA) dialogData: any,
    public dr: MatDialogRef<GeneralReferralComponent>,
    public loginForm: LoginForm,
    media: ObservableMedia) {
    super(refService,media);
  }

}
