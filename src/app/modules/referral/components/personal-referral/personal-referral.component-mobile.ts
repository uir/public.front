import {Component, EventEmitter, Inject, OnInit, Optional, Output} from '@angular/core';
import {ReferralForm} from "../../services/referral-form.service";
import {ReferralService} from "../../services/referral.service";
import {UserService} from "../../../auth/services/user.service";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material";
import {ObservableMedia} from "@angular/flex-layout";
import {PersonalReferralComponent} from "./personal-referral.component";

@Component({
  templateUrl: 'personal-referral.component.html',
  styleUrls: ['personal-referral.component.css']
})
export class PersonalReferralMobileComponent extends PersonalReferralComponent {

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) dialogData: any,
    public dr: MatDialogRef<PersonalReferralMobileComponent>,
    media: ObservableMedia,
    referralForm: ReferralForm,
    userService: UserService,
    refService: ReferralService) {
    super(referralForm, userService,media, refService);
  }


  onClose() {
    super.onClose();
    this.dr.close();
  }
}
