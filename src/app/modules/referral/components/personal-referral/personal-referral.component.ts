import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ReferralForm} from "../../services/referral-form.service";
import {ReferralService} from "../../services/referral.service";
import {UserService} from "../../../auth/services/user.service";
import {ReferalType} from "../../../../models/referal.model";
import {ObservableMedia} from "@angular/flex-layout";

@Component({
  selector: 'app-personal-referral',
  templateUrl: 'personal-referral.component.html',
  styleUrls: ['personal-referral.component.css']
})
export class PersonalReferralComponent implements OnInit {

  @Output() close: EventEmitter<{}> = new EventEmitter<{}>();

  public personalRef: any;

  constructor(
    public referralForm: ReferralForm,
    public userService: UserService,
    public media: ObservableMedia,
    public refService: ReferralService) {
  }

  onUpdate() {
    this.personalRef = null;
    this.referralForm.form.reset();
  }

  onClose() {
    this.close.emit();
  }

  ngOnInit() {
    this.userService.getUser().subscribe(value => {
      this.referralForm.buildForm();
      this.referralForm.setFormData({
        name: '',
        userId: value,
        type: ReferalType.Personal
      });
    });

  }

  create() {
    if (this.referralForm.form.valid) {
      this.refService
        .createRef(this.referralForm.form.value)
        .subscribe(value => this.personalRef = {
          name: this.referralForm.form.value.name,
          ref: value
        });
    }
  }
}
