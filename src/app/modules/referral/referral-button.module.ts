import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule, MatCardModule, MatIconModule, MatInputModule} from "@angular/material";

import {CommonModule} from '@angular/common';
import {DialogMobileModule} from "../common/dialog-mobile/dialog-mobile.module";
import {GeneralReferralComponent} from "./components/general-referral/general-referral.component";
import {GeneralReferralMobileComponent} from "./components/general-referral/general-referral.component-mobile";
import {HttpClientModule} from "@angular/common/http";
import {LoaderModule} from "../common/loader/loader.module";
import {NgModule} from '@angular/core';
import {PersonalReferralComponent} from "./components/personal-referral/personal-referral.component";
import {PersonalReferralMobileComponent} from "./components/personal-referral/personal-referral.component-mobile";
import {ReferralButtonComponent} from "./components/referral-button.component";
import {ReferralForm} from "./services/referral-form.service";
import {ReferralService} from "./services/referral.service";
import {ShareButtonsModule} from "@ngx-share/buttons";
import {WindowsModule} from "../windows/windows.module";

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    HttpClientModule,
    ShareButtonsModule.withConfig(),
    LoaderModule,
    DialogMobileModule
  ],
  declarations: [
    ReferralButtonComponent,
    GeneralReferralComponent,
    PersonalReferralComponent,
    PersonalReferralMobileComponent,
    GeneralReferralMobileComponent
  ],
  exports: [
    ReferralButtonComponent
  ],
  providers: [
    ReferralForm,
    ReferralService
  ],
  entryComponents: [
    PersonalReferralComponent,
    GeneralReferralComponent,
    PersonalReferralMobileComponent,
    GeneralReferralMobileComponent
  ]
})
export class ReferralButtonModule {
}
