import {FormGroup} from '@angular/forms/forms';

export abstract class BaseForm<T> {

  protected required = 'Обязательно для заполнения';
  protected emailPatternError = 'Неверный формат. Пример info@uir.one'
  public data: T;
  public formErrors;
  public placeholders;
  public validationMessages;
  public form: FormGroup;
  public lables;
  public icons;
  public nonVisibleKeys: string[];

  protected emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  protected onValueChanged(data?: any) {
    if (!this.form) {
      return;
    }
    const form = this.form;

    for (const field in this.data) {
      Object.defineProperty(this.formErrors, field, {
        value: '',
        writable: true,
        enumerable: true,
        configurable: true
      });
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  public clear() {
    this.setFormData(this.data)
  }

  public getValueData(f) {
    return this.form.value[f]
  }

  public reset(): void {
    this.form.reset()
  }

  public buildForm(): void {
    this.onValueChanged(this.data);
    this.form.valueChanges
      .subscribe(data => this.onValueChanged(data));
  }

  public isValid = (): boolean => this.form.valid;

  public getValue = (): T => this.form.value;

  public getKeys = (): string[] => {
    let keys = Object.keys(this.data);
    if (this.nonVisibleKeys != null) {
      return keys.filter(key => !this.nonVisibleKeys.includes(key))
    }
    return keys;
  };

  public getlabel(field: any): string {
    return this.lables[field];
  }

  public getIcon(field: any): string {
    return this.icons[field];
  }

  public isError(field: any): boolean {
    if (!this.formErrors[field]) return
    return this.formErrors[field].length > 0;
  }

  public getError(field): string {
    return this.formErrors[field];
  }

  public getPlaceholder(field): string {
    if (this.isError(field)) {
      return this.getError(field);
    } else {
      return this.placeholders[field];
    }
  }

  public setFormData(data) {
    if (data != null)
      Object.keys(data).forEach(name => {
        if (this.form.controls[name]) {
          this.form.controls[name].patchValue(data[name]);
        }
      });
  }

  protected abstract getForm(): FormGroup;

  protected abstract getValidationMessages();

  protected getPlaceholders() {
    let obj = {} as T;
    let plhldr = {};
    Object.keys(obj).forEach(name => {
      Object.defineProperty(plhldr, name, {
        value: '',
        writable: true,
        enumerable: true,
        configurable: true
      });
    });
    return plhldr;
  }

  protected getIcons() {
  };

  protected isRequired(field): boolean {
    return this.validationMessages[field] && this.validationMessages[field].required;
  }

  constructor(data: T) {
    this.data = data;
    this.formErrors = {};
    this.placeholders = this.getPlaceholders();
  }

}
