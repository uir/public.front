import {Routes} from "@angular/router";
import {SideNavPlacesComponent} from "./modules/side-nav/components/side-nav/side-nav-places.component";
import {DashboardComponent} from "./modules/dashboard/components/dashboard/dashboard.component";
import {UserLkComponent} from "./modules/user-lk/user-lk.component";
import {UserTariffsComponent} from "./modules/user-lk/user-tariffs/user-tariffs.component";
import {UserPersonalDataComponent} from "./modules/user-lk/user-personal-data/user-personal-data.component";
import {UserReferalsComponent} from "./modules/user-lk/user-referals/user-referals.component";
import {CategoriesComponent} from "./modules/side-nav/components/categories/categories.component";

const placeInfoModule = "app/modules/place-info/partner-info.module#PlaceInfoModule";
const windowsModule = "app/modules/windows/windows.module#WindowsModule";

export const AppRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'result',
        redirectTo: '/',
        pathMatch: 'full'
      },
      {
        path: '',
        component: DashboardComponent,
        loadChildren: windowsModule
      },
      {
        path: 'result',
        loadChildren: placeInfoModule,
      }
    ]
  },
  {
    path: 'search',
    component: SideNavPlacesComponent,
    loadChildren: placeInfoModule
  },
  {
    path: 'bookmarks',
    component: SideNavPlacesComponent,
    loadChildren: placeInfoModule
  },
  {
    path: 'temps/:tempId',
    component: CategoriesComponent,
  },
  {
    path: 'temps/:tempId/categories/:id',
    data: {key: 'company'},
    component: SideNavPlacesComponent,
    loadChildren: placeInfoModule
  },

  {
    path: 'lk',
    component: UserLkComponent,
    children: [
      {
        path: '',
        redirectTo: 'partner',
        pathMatch: 'full'
      },
      {
        path: 'partner',
        component: UserReferalsComponent
      },
      {
        path: 'tariffs',
        component: UserTariffsComponent,
      },
      {
        path: 'info',
        component: UserPersonalDataComponent,
      },
    ]
  },
];
