import {Injectable} from '@angular/core';
import {MatDialog} from "@angular/material";
import {ComponentType} from "@angular/cdk/portal";
import {AuthWindowComponent} from "../modules/auth/components/auth-window/authwindow.component";
import {BehaviorSubject} from "rxjs";
import {ObservableMedia} from "@angular/flex-layout";

@Injectable()
export class WindowService {

  private _windows$: BehaviorSubject<ComponentType<any>> = new BehaviorSubject<ComponentType<any>>(null);

  constructor(private dialog: MatDialog,public media: ObservableMedia) {
    this._windows$.subscribe(x => {
      if (x) {
        this.dialog.closeAll();
        this.dialog.open(x)
      }
    });
  }

  public show<T>(comp: ComponentType<T>) {
    this._windows$.next(comp);
  }

  public showAuth() {
    this.dialog.open(AuthWindowComponent, {
      data: {
        activeTab: "auth"
      },
      panelClass: this.media.isActive('xs') ? "user-lk-window" : ""
    })
  }

  public showReg() {
    this.dialog.open(AuthWindowComponent, {
      data: {
        activeTab: "reg"
      },
      panelClass: this.media.isActive('xs') ? "user-lk-window" : ""
    })
  }

  public closeAll() {
    this.dialog.closeAll();
  }
}
