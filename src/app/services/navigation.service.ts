import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {filter, pairwise} from "rxjs/internal/operators";


@Injectable()
export class NavigationService {
  constructor(protected router: Router) {
    this.router.events
      .pipe(pairwise())
      .subscribe(x => {
        if(x[0] instanceof NavigationEnd)
         this.navigateUrl = x[0]['urlAfterRedirects']
      });

  }


  private defaultUrl: string = '';
  private navigateUrl: string;

  getUrl() {
    let url = this.navigateUrl ? this.navigateUrl : this.defaultUrl;
    this.navigateUrl = null;
    return url;
  }
}
