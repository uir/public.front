import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {pairwise} from "rxjs/operators";

declare var $: any;

class ElementScroll {
  el: HTMLElement;
  scroll: number;
}

class ScrollStoreConfig {
  componentContext: any;
  router: Router;
  storage: {
    [key: string]: ElementScroll
  };
}

export class ScrollStoreProvider {
  public config: ScrollStoreConfig = <ScrollStoreConfig>{};
  subs;

  constructor(options: {
    compContext: any,
    router: Router,
  }) {
    this.config.componentContext = options.compContext;
    this.config.router = options.router;
    this.config.storage = {};
    this.restoreAll()
  }

  private restoreAll() {
    this.subs = this.config.router.events
      .pipe(pairwise())
      .subscribe(x => {
        if(x[0]['url'] == '/')
          this.config.storage['key'].scroll = 0;
        if (x[0] instanceof NavigationEnd && this.config.storage['key']) {
          let url = x[0]['urlAfterRedirects'];
          if (url.indexOf('info') > -1 || url.indexOf('partner') ) {
            let id = setTimeout(()=> {
              if(this.config.storage['key'].el)
                this.config.storage['key'].el.scrollTo(0,this.config.storage['key'].scroll);
              if(this.config.storage['key'].el && this.config.storage['key'].el.scrollTop != 0)
                clearInterval(id)
            },50)
          }
        }
      });
  }

  private storeScroll(key: string, el: HTMLElement) {
    this.config.storage[key] = <ElementScroll>{
      el: el,
      scroll: $(el).scrollTop()
    };
  }

  public handleScroll(key: string, el: HTMLElement) {
    let providerRef = this;
    $(el).scroll(() => {
      providerRef.storeScroll(key, el);
    })
  }

}
