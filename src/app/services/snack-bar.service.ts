import { Injectable } from '@angular/core';
import {MatSnackBar} from "@angular/material";

@Injectable()
export class SnackBarService {

  constructor(private snackBar:MatSnackBar) { }

  show(text: string){
    this.snackBar.open(text, '',{duration: 4000})
  }
}
