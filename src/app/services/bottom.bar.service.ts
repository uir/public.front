import {Injectable, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from "rxjs";


export enum BottomBarType {
  main,category
}

@Injectable()
export class BottomBarService{

  public bottomBarType: BehaviorSubject<BottomBarType> = new BehaviorSubject(BottomBarType.main) ;
  public open: BehaviorSubject<boolean> = new BehaviorSubject(true);

  public toggle(open: boolean){
    this.open.next(open)
  }

  public setType(type: BottomBarType){
    this.bottomBarType.next(type)
  }

  public getType(): BehaviorSubject<BottomBarType>{
    return this.bottomBarType
  }
}


