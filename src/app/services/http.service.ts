import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable()
export class HttpService {

  POST<T>(url, data?, headers?: HttpHeaders): Observable<T> {
    return this
      .http
      .post<T>(url, data, {
        headers: headers
      });
  }

  DELETE<T>(url, params?: HttpParams): Observable<T> {
    return this
      .http
      .delete<T>(url, {
        params: params
      });
  }

  PUT<T>(url, data?, headers?: HttpHeaders): Observable<T> {
    return this
      .http
      .put<T>(url, data, {
      });
  }


  GET<T>(url, params?: HttpParams): Observable<T> {
    return this
      .http
      .get<T>(url, {
        params: params
      });
  }

  constructor(protected http: HttpClient) {
  }
}
