import {Injectable} from '@angular/core';
import {HttpService} from "./http.service";
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AuthorizeService} from "../modules/auth/services/authorize.service";
import {BehaviorSubject, Observable} from "rxjs";
import {filter, map, switchMap, tap} from "rxjs/operators";

@Injectable()
export class BookmarkService extends HttpService {

  private bookmarksApi: string = environment.api + 'api/user/bookmark/all';
  private bookmarkApi: string = environment.api + 'api/user/bookmark';
  private _bookmarks$: BehaviorSubject<number[]> = new BehaviorSubject<number[]>([]);


  constructor(http: HttpClient, private authService: AuthorizeService) {
    super(http);
    this.authService
      .isAuthorize()
      .pipe(filter(value => !value))
      .subscribe(value => {
        this._bookmarks$.next([]);
      });

    this.authService
      .isAuthorize()
      .pipe(
        filter(value => value),
        switchMap(() => this.fetchBookmarks())
      )
      .subscribe(value => {
        this._bookmarks$.next(value);
      });
  }

  public getBookmarks(): Observable<number[]> {
    return this._bookmarks$;
  }

  public setBookmarks(ids: number[]) {
    this._bookmarks$.next(ids);
  }

  public fetchBookmarks(): Observable<number[]> {
    return this.GET<number[]>(this.bookmarksApi).pipe(
      tap(value => this._bookmarks$.next(value))
    );
  }

  public addBookmark(placeId: number): Observable<any> {
    return this
      .POST<any>(this.bookmarkApi, {
        partnerId: placeId
      })
      .pipe(
        tap(() => {
          let bookmarks = this._bookmarks$.getValue();
          bookmarks.push(placeId);
          this._bookmarks$.next(bookmarks)
        })
      );
  }

  public deleteBookmark(placeId: number): Observable<any> {
    let params = new HttpParams();
    params = params.set("placeId", placeId + "");
    return this.DELETE<any>(this.bookmarkApi, params)
      .pipe(
        tap(() => {
          let bookmarks = this._bookmarks$.getValue();
          bookmarks = bookmarks.filter(x => x != placeId);
          this._bookmarks$.next(bookmarks)
        })
      );
  }

  public isBookmark(placeId: number): Observable<boolean> {
    return this._bookmarks$.pipe(
      map(value => value.includes(placeId))
    );
  }
}
