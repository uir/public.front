import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {OperationError} from "../models/operation-error.model";
import {NotificationService} from "./notification.service";
import {Observable} from "rxjs/internal/Observable";
import {catchError} from "rxjs/operators";
import {EMPTY} from "rxjs/internal/observable/empty";
import {throwError} from "rxjs/internal/observable/throwError";

@Injectable()
export class ErrorsInterceptor implements HttpInterceptor {

  constructor(private notification: NotificationService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      withCredentials: true
    });

    return next.handle(request)
      .pipe(
        catchError((error: any) => {
        if (error instanceof HttpErrorResponse) {
          if (error.status == 400) {
            let res = error.error as OperationError;
            this.notification.notify(res.message);
            return EMPTY;
          }
        }
        return throwError(error);
      }));
  }
}
