import {RouteReuseStrategy, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {AppRoutes} from "./app.routing";
import {HeaderModule} from "./modules/header/header.module";
import {HttpService} from "./services/http.service";
import {MatButtonModule, MatDialogModule, MatSnackBarModule} from "@angular/material";
import {SnackBarService} from "./services/snack-bar.service";
import {WindowService} from "./services/window.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {ErrorsInterceptor} from "./services/errors.interceptor";
import {AuthModule} from "./modules/auth/auth.module";
import {NotificationService} from "./services/notification.service";
import {CategoriesMenuModule} from "./modules/side-nav/side-nav.module";
import {ReferralButtonModule} from "./modules/referral/referral-button.module";
import {AppMapModule} from "./modules/map/app-map.module";
import {BemModule} from "angular-bem";
import {DashboardModule} from "./modules/dashboard/dashboard.module";
import {WindowsModule} from "./modules/windows/windows.module";
import {MatSidenavModule} from '@angular/material/sidenav';
import {UserLkModule} from "./modules/user-lk/user-lk.module";
import {ClickOutsideModule} from "ng-click-outside";
import {BookmarkService} from "./services/bookmark.service";
import {MatFeedbackModule} from "./modules/common/mat-feedback/mat-feedback.module";
import {FeedbackService} from "./modules/common/feedback/feedback.service";
import {NavigationService} from "./services/navigation.service";
import {CategoryMapMobileService} from "./modules/side-nav/services/category.map.mobile.service";
import {ImageGalleryModule} from "./modules/common/image-gallery/image-gallery.module";
import {ImageGalleryService} from "./modules/common/image-gallery/image-gallery.service";
import {BottomBarModule} from "./modules/bottom-bar/bottom-bar.module";
import {BottomBarService} from "./services/bottom.bar.service";
import {CustomReuseStrategy} from "./services/custom-reuse-strategy";
import {ClickLogModule} from "./modules/common/click-log/click-log.module";
import {environment} from "../environments/environment";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule.forRoot(AppRoutes),
    HeaderModule,
    AppMapModule,
    UserLkModule,
    CategoriesMenuModule,
    MatDialogModule,
    MatButtonModule,
    MatSnackBarModule,
    AuthModule,
    ReferralButtonModule,
    BemModule,
    DashboardModule,
    WindowsModule,
    MatSidenavModule,
    ClickOutsideModule,
    MatFeedbackModule,
    ImageGalleryModule,
    BottomBarModule,
    ClickLogModule.forRoot({api: environment.clickHost + "api/log"})
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: ErrorsInterceptor, multi: true},
    {provide: RouteReuseStrategy, useClass: CustomReuseStrategy},
    NotificationService,
    HttpService,
    SnackBarService,
    WindowService,
    NavigationService,
    BookmarkService,
    FeedbackService,
    CategoryMapMobileService,
    ImageGalleryService,
    BottomBarService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
