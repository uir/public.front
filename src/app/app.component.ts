import {Component, Inject, OnInit} from '@angular/core';
import {ObservableMedia} from "@angular/flex-layout";
import {NavigationEnd, Router} from "@angular/router";
import {FeedbackService} from "./modules/common/feedback/feedback.service";
import {DOCUMENT} from "@angular/common";
import {CategoryMapMobileService} from "./modules/side-nav/services/category.map.mobile.service";
import {BottomBarService, BottomBarType} from "./services/bottom.bar.service";
import {pairwise} from "rxjs/operators";
import {ClickLogService} from "./modules/common/click-log/click-log.module";

@Component({
  selector: 'app-root',
  templateUrl: `./app.component.html`,
  styleUrls: ['app.component.css']
})
export class AppComponent implements OnInit {
  hiddenScroll: boolean;
  public type: BottomBarType;

  constructor(public media: ObservableMedia,
              protected router: Router,
              public feedback: FeedbackService,
              public categoryMobileService: CategoryMapMobileService,
              public bottomBarService: BottomBarService,
              private log: ClickLogService,
              @Inject(DOCUMENT) private document: Document) {

    this.router.events.subscribe(x => {
      if (x instanceof NavigationEnd) {
        this.hiddenScroll = x.url != '/'
      }
    });
    this.bottomBarService.getType().subscribe(x => {
      this.type = x
    });
  }

  ngOnInit() {
    if (this.media.isActive('xs')) {
      this.document.body.classList.add('mobile_body');
      this.router.events.pipe(pairwise())
        .subscribe(x => {
          if (x[0] instanceof NavigationEnd){
            let url = x[0]['url'];
            let flagMain = url == '/' || url.indexOf('reg') > -1;
            let flagTemplate = /\/temps\/[0-9]+\/categories\/[0-9]+$/i.test(url);
            if(flagMain || flagTemplate){
              this.bottomBarService.toggle(true);
              if(flagMain)
                this.bottomBarService.setType(BottomBarType.main);
              else if(flagTemplate)
                this.bottomBarService.setType(BottomBarType.category)
            }
            else this.bottomBarService.toggle(false)
          }
        });
    }

  }

}
