export class UserParent {
    id: number;
    name?: string;
}

export class User {
  id: number;
  token?: any;
  name?: string;
  email?: string;
  phone?: string;
  roles?: string[];
}

