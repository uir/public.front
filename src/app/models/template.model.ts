import {Category} from "./category.model";

export class Template {
  public id: number;
  public name: string;
  public icon: string;
  public color: string;
  categories: Category[];
}
