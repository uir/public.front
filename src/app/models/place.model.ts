import {Tag} from "./tag";
import {Marker} from "./marker.model";

export class PlacesAndMarkers {
  placesCount: number;
  places: Place[];
  markers: Marker[];
}

export class PlacesList extends PlacesAndMarkers {
  categoryName: string;
}

export class Place {
  id: number;
  name?: string;
  address: string;
  lat?: number;
  lng?: number;
  logo?: string;
  isBookmark: boolean = false;
  tags?: Tag[];
  imagesCount: number;
  videosCount: number;
  salesCount: number;
  overviewsCount: number;
  phone: number;
  website?: string;
  planValue?: PlanValue;
}

export enum PlanValue {
  base = 0, standart = 4000, premium = 7000
}
