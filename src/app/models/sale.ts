export class Sale {
  id: number;
  name: string;
  description: string;
  amount: number;
  isExclusive: boolean;
  discountType: DiscountType;
  discountEndDate: Date;
}

export class RecentSale extends Sale {
  partnerId: number;
  partnerName: string;
}

export enum DiscountType {
  Common = 0, //скидка
  Stock = 1 //акция
}
