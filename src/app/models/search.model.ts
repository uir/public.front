import {DiscountType} from "./sale";

export class Search {
  tags: SearchResult[];
  places: PlaceSearchResult[];
  categories: SearchResult[];
  sales: SaleSearchResult[];
}

export enum SearchResultType {
  Sale, Place, Category, Tag
}

export class SearchResult {
  id: number;
  name: string;
  type?: SearchResultType;
}

export class SaleSearchResult extends SearchResult {
  amount: number;
  organizationName: string;
  placeId: number;
  discountType: DiscountType;
}

export class PlaceSearchResult extends SearchResult {
  logo: string;
  address: string;
}
