export class Image {
    file: string;
    height: number;
    width: number;
}
