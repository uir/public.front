export class Marker {
  id: number;
  latitude: number;
  longitude: number;
}
