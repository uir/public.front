export class Referal {
    name?: string;
    type?: ReferalType;
    userId?: number;
}
export enum ReferalType {
    Personal = 0, 
    General = 1
}
