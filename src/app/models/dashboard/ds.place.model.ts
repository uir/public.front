export class DsPlace {
  id: number;
  name: string;
  logo: string;
  website: string;
  phone: string;
  address: string;
  categories: string[];
  iHaveImages:boolean;
  iHaveVideos:boolean;
  iHaveOverviews:boolean;
}
