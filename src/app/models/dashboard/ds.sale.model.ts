import {DiscountType} from "../sale";

export class SaleItem {
  name: string;
  organizationName: string;
  type: DiscountType;
  amount: number;
  isExclusive: boolean;
  placeId: number;
}
