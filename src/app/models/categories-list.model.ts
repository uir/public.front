import {Category} from "./category.model";

export class CategoriesList {
  templateIcon: string;
  templateColor: string;
  templateName: string;
  categories: Category[];
}
