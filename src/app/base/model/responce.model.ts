export class AppResponce {
    message: string;
    isSuccess: boolean;
    isLoading: boolean;
}

export const responceInitial = {
    isSuccess: false,
    isLoading: false,
    message: ''
}