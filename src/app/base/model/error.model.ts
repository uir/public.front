import {HttpErrorResponse} from "@angular/common/http";

export class Error {
  message: string;
  status: number;
  error?: string;

  public static parse(err: any): Error {
    if (err instanceof HttpErrorResponse) {
      let ex = err as HttpErrorResponse;
      return {
        message: Error.getErrorMessage(ex),
        status: ex.status
      };
    }
    let body = JSON.parse(err._body);
    if (body["error"])
      return {
        status: err.status,
        error: body.error,
        message: Error.getMessageRus(body.error)
      };
    return {
      message: JSON.parse(err._body).message,
      status: err.status
    };
  }

  private static getMessageRus(err: string){
    if (err == "invalid_grant")
      return "Неверный логин или пароль";
  }

  private static getErrorMessage(err: HttpErrorResponse){
    if (err.error.error == "invalid_grant")
      return "Неверный логин или пароль";
    return err.error.error;
  }
}
